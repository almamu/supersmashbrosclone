#include "common.h"
#include "server.h"
#include "input.h"
#include "network.h"
#include "events.h"
#include "battle.h"

#include <time.h>

// externals
extern bool cl_inbattle;

// locals
client_t* sv_clients = NULL;
uint8_t sv_maxplayers = 2;
uint8_t sv_clientsCount = 0;
uint8_t dedicated = 0;
time_t sv_lastSync = 0;

void Server_Init(uint8_t maxplayers, uint8_t mode)
{
   sv_maxplayers = maxplayers;
   
   // free if its already allocated (allows full restart)
   if(sv_clients) Memory_Free(sv_clients);
   
   // alloc space for client_t
   sv_clients = (client_t*)Memory_Alloc(sizeof(client_t) * sv_maxplayers);
   
   // make sure everything stays as 0
   Com_Memset(&sv_clients[0], 0, sizeof(client_t) * sv_maxplayers);
   
   // TODO: MAKE THIS STANDARD AND REMOVE MANUAL DATA SETUP FOR THE MEMORYs SHAKE
   // fill in some data for ourselves
   for(uint8_t i = 0; i < sv_maxplayers; i ++)
   {
	   sv_clients[i].handshake = 0;
	   sv_clients[i].state = STATE_WAITING;
   }
   /*
   sv_clients[1].handshake = 0;
   sv_clients[1].state = STATE_WAITING;

   // THIS SHOULD BE LOADED FROM SETTINGS
   sv_clients[0].addr = (adr_t*)Memory_Alloc(sizeof(adr_t));
   
   sv_clients[0].addr->ip = ADDR_BOT;
   sv_clients[0].addr->port = 0;
   
   sv_clients[0].handshake = 0;
   sv_clients[0].state = STATE_READY;
   sv_clients[0].handshake = 0;
   sv_clients[0].state = STATE_WAITING;*/

   // update dedicated mode
	if(mode == 2)
	{
		dedicated = true;
	}

   sv_clientsCount = 0;
   sv_lastSync = time(NULL);

   // init network if needed
   if(dedicated > 0)
   {
	   Net_UdpInit();
	   Net_UdpListen();
   }
}

void Server_Frame()
{
	if(!dedicated) return;

	time_t diff = 0;

   // fire events for every player
   for(uint8_t i = 0; i < sv_maxplayers; i ++)
   {
      // ignore not authed users
      if(sv_clients[i].state != STATE_READY) continue;
      
	  // check player timeout
	  if(sv_clients[i].network.last_time + 10 < time(NULL) && sv_clients[i].addr->ip != ADDR_BOT)
	  {
		  // TODO: REMOVE BOT SKIP ON NETWORK TIME
		  // out of time, the user disconnected
		  sv_clients[i].state = STATE_WAITING;
		  
		  Com_Printf(Com_LogLevel_Info, "Player from %s (%d) disconected\n", Net_AdrToString(sv_clients[i].addr), i);
		  Memory_Free(sv_clients[i].addr);

		  // update server players count
		  sv_clientsCount --;
		  continue;
	  }

		// ignore users not pressing anything to let the game take a break
		if(sv_clients[i].keys.held == 0 && sv_clients[i].keys.down == 0)
		{
			continue;
		}   
      
      // build keys_t data
      keys_t* keys = (keys_t*)Memory_Alloc(sizeof(keys_t));
      keys->from = (adr_t*)Memory_Alloc(sizeof(adr_t));
      
      keys->from->ip = sv_clients[i].addr->ip;
      keys->from->port = sv_clients[i].addr->port;
      
      // setup keys
      keys->held = sv_clients[i].keys.held;
      keys->down = sv_clients[i].keys.down;
      
      // direct event queueing
      if(cl_inbattle)
	  {
	     Sys_QueueEvent(EVENT_BATTLE, keys, EVENT_KEYPRESS, 0);
	  }
	  else
	  {
	     Memory_Free(keys->from);
	     Memory_Free(keys);
	  }
   }

   diff = difftime(time(NULL), sv_lastSync);

   if(diff > 10 && sv_clientsCount > 0)
   {
	   sv_lastSync = time(NULL);

	   Server_FullSync();
   }
}   

void Server_KeyInput(keys_t* keys)
{
   uint8_t index = 0;
   
   for(index = 0; index < sv_maxplayers; index ++)
   {
      if(
		  sv_clients[index].addr != NULL &&
		  sv_clients[index].addr->ip == keys->from->ip &&
		  sv_clients[index].addr->port == keys->from->port
		)
      {
         break;
      }   
   }
	
	Com_Assert(index == sv_maxplayers, "Server_KeyInput: players overflow\n");
	
	sv_clients[index].keys.held = keys->held;
	sv_clients[index].keys.down = keys->down;
	sv_clients[index].network.last_time = time(NULL);
	
	// only do this if we are a network server
	if(dedicated > 0)
	{
		// notify other clients if needed
		for(uint8_t i = 0; i < sv_maxplayers; i ++)
		{
			if(sv_clients[i].state == STATE_READY)
			{
				// build msg_t data
				msg_t* msg = (msg_t*) Memory_Alloc(sizeof(msg_t));
				MSG_Init(msg, NULL, 1);
				
				// write adr_t
				MSG_WriteData(msg, sv_clients[i].addr, sizeof(adr_t));
				
				// write type
				MSG_WriteByte(msg, EVENT_KEYPRESSRELAY);
				
				// write key data
				MSG_WriteInteger(msg, keys->held);
				MSG_WriteInteger(msg, keys->down);

				// write user's data
				MSG_WriteData(msg, sv_clients[index].addr, sizeof(adr_t));
				
				// queue network event
				Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);
			}
		}
	}
}

void Server_Event(event_t* ev)
{
   switch(ev->type)
   {
	  case EVENT_HANDSHAKEREQ:
		  {
			  // get handshake data
			  handshakeReq_t* hand = (handshakeReq_t*)ev->data;

			  // check protocol version first
			  if(hand->protocol != PROTOCOL)
			  {
				  Com_Error("Wrong protocol version received\n");

				  // return handshake failed message
				  msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));

				  MSG_Init(msg, NULL, 1);

				  // write address
				  MSG_WriteData(msg, &hand->addr, sizeof(adr_t));

				  // write event
				  MSG_WriteByte(msg, EVENT_HANDSHAKEFAIL);

				  // write reason
				  MSG_WriteByte(msg, 0);

				  // queue packet
				  Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);

				  Memory_Free(hand);
				  return;
			  }

			  // find any empty user slot
			  uint8_t index = 0;

			  for(index = 0; index < sv_maxplayers; index ++)
			  {
				  if(sv_clients[index].state == STATE_WAITING)
				  {
					  break;
				  }
			  }

			  // check if we found any empty slot
			  if(index == sv_maxplayers)
			  {
				  Com_Error("Cannot find any left user slot\n");

				  // return handshake failed message
				  msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));

				  MSG_Init(msg, NULL, 1);

				  // write address
				  MSG_WriteData(msg, &hand->addr, sizeof(adr_t));

				  // write event
				  MSG_WriteByte(msg, EVENT_HANDSHAKEFAIL);

				  // write reason
				  MSG_WriteByte(msg, 1);

				  // queue packet
				  Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);

				  // free data
				  Memory_Free(hand);

				  return;
			  }

			  // generate handshake data from seed
			  srand(hand->seed);

			  // set data to correct user
			  sv_clients[index].handshake = rand() % 0xFFFFFFFF;

			  // update slot status (so we reserve it for this user)
			  sv_clients[index].state = STATE_HANDSHAKE;

			  // update address data
			  sv_clients[index].addr = (adr_t*)Memory_Alloc(sizeof(adr_t));

			  Com_Memcpy(sv_clients[index].addr, &hand->addr, sizeof(adr_t));

			  // update last packet time
			  sv_clients[index].network.last_time = time(NULL);

			  if(hand->addr.ip == ADDR_SELF)
			  {
				  Com_Printf(Com_LogLevel_Info, "Local player connected on slot %d\n", index);
			  }
			  else
			  {
				  // log connection
				  Com_Printf(Com_LogLevel_Info, "Incoming connection from %s (%d)\n", Net_AdrToString(&hand->addr), index);
			  }

			  // free useles data
			  Memory_Free(hand);
		  }
		  break;

      case EVENT_HANDSHAKE:
         {
			 serverHandshake_t* hand = (serverHandshake_t*)ev->data;
	         
	         bool correct = true;
	         
			 uint8_t index = 0;

			 for(index = 0; index < sv_maxplayers; index ++)
			 {
			    if(sv_clients[index].state != STATE_HANDSHAKE) continue;
			    
				 // check handshake and address
				 if(
					 sv_clients[index].handshake == hand->handshake &&
					 sv_clients[index].addr != NULL &&
					 sv_clients[index].addr->ip == hand->addr->ip &&
					 sv_clients[index].addr->port == hand->addr->port
					)
				 {
					 break;
				 }
			 }

			 if(index == sv_maxplayers)
			 {
				 Com_Error("User tried to login without being authed\n");

				  // return handshake failed message
				  msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));

				  MSG_Init(msg, NULL, 1);

				  // write address
				  MSG_WriteData(msg, hand->addr, sizeof(adr_t));

				  // write event
				  MSG_WriteByte(msg, EVENT_HANDSHAKEFAIL);

				  // write reason
				  MSG_WriteByte(msg, 2);

				  // queue packet
				  Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);

				 // free useles data
				 Memory_Free(hand->addr);
				 Memory_Free(hand);

				 return;
			 }

			 // increase players count
			 sv_clientsCount ++;

			 msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));
			 
			 // build msg_t
			 MSG_Init(msg, NULL, 1);

			 MSG_WriteData(msg, hand->addr, sizeof(adr_t));
			 MSG_WriteByte(msg, EVENT_HANDSHAKERES); // event type
			 MSG_WriteByte(msg, correct); // result flag
			 MSG_WriteByte(msg, index); // tell the game which player we are
			 MSG_WriteByte(msg, sv_maxplayers); // max players in server
			 MSG_WriteByte(msg, sv_clientsCount); // current players count

			 // queue packet event
			 Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);

			 // free useless data
			 Memory_Free(hand->addr);
			 Memory_Free(hand);

			 // update user status
			 sv_clients[index].state = STATE_READY;

			 // force a full sync
			 Server_FullSync();

			 // update player time
			 sv_clients[index].network.last_time = time(NULL);
		 }
         break;

		case EVENT_SERVERPING:
			{
				// send the ping back to client
				msg_t* msg = (msg_t*)ev->data;
				
				MSG_Reset(msg);
				
				adr_t adr;
				
				MSG_ReadData(msg, &adr, sizeof(adr_t));
				MSG_ReadByte(msg); // jump type byte
				
				int64_t pingtime = MSG_ReadLong(msg);
				
				// find users matching address
				for(uint8_t index = 0; index < sv_maxplayers; index++)
				{
					if(
						sv_clients[index].addr != NULL &&
						sv_clients[index].addr->ip == adr.ip &&
						sv_clients[index].addr->port == adr.port
						)
					{
						sv_clients[index].network.last_time = pingtime;
					}
				}
				
				Memory_Free(msg->data);
				Memory_Free(msg);

				// ping back
				msg = (msg_t*)Memory_Alloc(sizeof(msg_t));
				
				MSG_Init(msg, NULL, 15);
				
				MSG_WriteData(msg, &adr, sizeof(adr_t));
				MSG_WriteByte(msg, EVENT_CLIENTPING);
				MSG_WriteLong(msg, time(NULL));
				
				Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);
			}
		  break;

	  default:
		  break;
   }   
}

void Server_FullSync()
{
	 // full sync for everyone
	 msg_t* data = NULL;

	 Com_Assert(cl_inbattle == false, "Server_FullSync: Sync out of battle not supported\n");

	 data = Battle_SyncData(sv_clients);

	 for(uint8_t i = 0; i < sv_maxplayers; i ++)
	 {
		 if(sv_clients[i].state != STATE_READY) continue;

		 msg_t* tmp = (msg_t*)Memory_Alloc(sizeof(msg_t));

		 // initialize message and create buffer
		 MSG_Init(tmp, NULL, data->cursize + sizeof(adr_t));

		 // write address
		 MSG_WriteData(tmp, sv_clients[i].addr, sizeof(adr_t));

		 // write data
		 MSG_WriteData(tmp, &data->data[0], data->cursize);

		 Sys_QueueEvent(EVENT_NETWORK, tmp, NETWORK_OUTPUT, 0);
	 }
	 
	 Memory_Free(data->data);
	 Memory_Free(data);
}
