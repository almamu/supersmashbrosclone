#include "common.h"
#include "mvars.h"

mvar_t basic_mvars[] = {
  {NULL, MVAR_TYPE_NONE, NULL}, 
};

mvarListEntry_t* mvars_first = NULL;
mvarListEntry_t* mvars_last = NULL;

float mvar_version = 0.1f;

void Mvars_Init()
{
   // mvars_first mvar is mvar system version
   mvars_first = (mvarListEntry_t*)Memory_Alloc(sizeof(mvarListEntry_t));
   Com_Memset(mvars_first, 0, sizeof(mvarListEntry_t));
   
   mvars_first->value.name = "mvar_version";
   mvars_first->value.type = MVAR_TYPE_FLOAT;
   mvars_first->value.pointer = &mvar_version;
   mvars_first->next = NULL;
   mvars_first->prev = NULL;
   
   mvars_last = mvars_first;
   
   // register every mvar in the basic_mvars list (this will add them to the mvars list)
   for(mvar_t* mvar = basic_mvars; mvar->name && mvar->pointer; mvar++)
   {
      Mvars_Register(mvar->name, mvar->type, mvar->pointer);
   }
}

mvar_t* Mvars_Find(const char* name)
{
	// find correct mvar
	mvarListEntry_t* cur = mvars_first;

	while (cur != NULL)
	{
		if (strcmp(cur->value.name, name) == 0) return &cur->value;

		cur = cur->next;
	}

	return NULL;
}

mvar_t* Mvars_Register(const char* name, mvarType_t type, void* pointer)
{
	// make sure we dont have duplicates
	mvar_t* mvar = Mvars_Find(name);

	// we are not going to update the mvar value, return the actual mvar
	if (mvar)
	{
		return mvar;
	}

   uint32_t length = 0;
   // set next value
   mvars_last->next = (mvarListEntry_t*)Memory_Alloc(sizeof(mvarListEntry_t));
   Com_Memset(mvars_last->next, 0, sizeof(mvarListEntry_t));
   
   switch(type)
   {
      case MVAR_TYPE_STRING:	length = strlen((char*)pointer) + 1;	break;
	  case MVAR_TYPE_BYTE:		length = sizeof(uint8_t);				break;
	  case MVAR_TYPE_SHORT:		length = sizeof(uint16_t);				break;
	  case MVAR_TYPE_INT:		length = sizeof(uint32_t);				break;
	  case MVAR_TYPE_LONG:		length = sizeof(uint64_t);				break;
	  case MVAR_TYPE_FLOAT:		length = sizeof(float);					break;
	  case MVAR_TYPE_DOUBLE:	length = sizeof(double);				break;
	  default:					length = 0;								break;
   }

   Com_Assert(length == 0, "Mvars_Register: cannot determine data size for mvar \"%s\"\n", name);

   // assign data
   mvars_last->next->value.name = name;
   mvars_last->next->value.type = type;
   mvars_last->next->value.pointer = Memory_Clone(pointer, length);
   mvars_last->next->next = NULL;
   mvars_last->next->prev = mvars_last;

   // update list
   mvars_last = mvars_last->next;

   Com_Printf(Com_LogLevel_Debug, "Mvars_Register: registered mvar \"%s\" with type %d\n", name, type);

   return &mvars_last->value;
}

mvar_t* Mvars_RegisterString(const char* name, const char* value)
{
	char* pointer = String_Create(strlen(value));
	strcpy(pointer, value);

	return Mvars_Register(name, MVAR_TYPE_STRING, pointer);
}

void Mvars_SetString(const char* name, const char* value)
{
	// find mvar
	mvar_t* mvar = Mvars_Find(name);

	Com_Assert(mvar == NULL, "Mvars_SetString: cannot find mvar \"%s\"\n", name);
	Com_Assert(mvar->type != MVAR_TYPE_STRING, "Mvars_SetString: called on a %d type mvar\n", mvar->type);

	uint32_t length = strlen(value) + 1;
	mvar->pointer = Memory_Realloc(mvar->pointer, length);
	Com_Memset(mvar->pointer, 0, length);
	
	// copy string
	strcpy((char*)mvar->pointer, value);
}

void Mvars_SetInteger(const char* name, uint32_t value)
{
	mvar_t* mvar = Mvars_Find(name);

	Com_Assert(mvar == NULL, "Mvars_SetInteger: cannot find mvar \"%s\"\n", name);
	Com_Assert(mvar->type != MVAR_TYPE_INT, "Mvars_SetInteger: called on a %d type mvar\n", mvar->type);

	mvar->pointer = Memory_Realloc(mvar->pointer, sizeof(uint32_t));
	Com_Memcpy(mvar->pointer, &value, sizeof(uint32_t));
}

void Mvars_SetShort(const char* name, uint16_t value)
{
	mvar_t* mvar = Mvars_Find(name);

	Com_Assert(mvar == NULL, "Mvars_SetShort: cannot find mvar \"%s\"\n", name);
	Com_Assert(mvar->type != MVAR_TYPE_SHORT, "Mvars_SetShort: called on a %d type mvar\n", mvar->type);

	mvar->pointer = Memory_Realloc(mvar->pointer, sizeof(uint16_t));
	Com_Memcpy(mvar->pointer, &value, sizeof(uint16_t));
}

void Mvars_SetByte(const char* name, uint8_t value)
{
	mvar_t* mvar = Mvars_Find(name);

	Com_Assert(mvar == NULL, "Mvars_SetByte: cannot find mvar \"%s\"\n", name);
	Com_Assert(mvar->type != MVAR_TYPE_BYTE, "Mvars_SetByte: called on a %d type mvar\n", mvar->type);

	mvar->pointer = Memory_Realloc(mvar->pointer, sizeof(uint8_t));
	Com_Memcpy(mvar->pointer, &value, sizeof(uint8_t));
}

void Mvars_Unregister(const char* name)
{
	// find mvar in the list
	mvarListEntry_t* cur = mvars_first;

	while(cur != NULL)
	{
		if(strcmp(cur->value.name, name) == 0) break;

		cur = cur->next;
	}

	Com_Assert(cur == NULL, "Mvars_Unregister: cannot find mvar \"%s\"\n", name);

	// update mvars_last mvar entry just in case
	if(cur->next == NULL) mvars_last = cur->prev;
	if(cur->prev != NULL) cur->prev->next = cur->next;

	// free memory
	Memory_Free(cur->value.pointer);
	Memory_Free(cur);

	Com_Printf(Com_LogLevel_Debug, "Mvars_Uregister: unregistered mvar \"%s\"\n", name);
}
