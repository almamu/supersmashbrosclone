#include "common.h"
#include "battle.h"
#include "events.h"
#include "input.h"
#include "assets.h"
#include "network.h"
#include <graphics.h>
#include "client.h"
#include "server.h"
#include "cmap.h"

bool cl_inbattle = false;
extern uint8_t sv_maxplayers;
extern uint8_t cl_clientnum;
extern uint8_t cl_maxplayers;

// battle config
int g_shield = G_SHIELD;
int g_spawn = G_SPAWN;
int g_speed = G_SPEED;

// battle data
uint8_t b_maxplayers = 0;

background_t* b_background = NULL;
character_t* characters = NULL;
cpuTarget_t cpuTarget[6];
coord_t* playerSpawns = NULL;
extern camera_t camera;
map_t* map = NULL;

void Battle_Setup(const char** fastfiles, uint32_t count)
{
	// Load resources needed from fastfiles
	if(count > 0)
	{
		for(uint32_t i = 0; i < count; i ++)
		{
		   // load fastfile
			FastFile_Load(fastfiles[i]);
		}
	}
	
	b_maxplayers = max(sv_maxplayers, cl_maxplayers);

	if(b_maxplayers < 1)
	{
		Com_Error("Battle_Setup: b_maxplayers underflow\n");
	}
	
	if(characters)
	{		
		Memory_Free(characters);
	}
	
	if(playerSpawns)
	{
		Memory_Free(playerSpawns);
	}
	
	characters = (character_t*)Memory_Alloc(sizeof(character_t) * b_maxplayers);
	playerSpawns = (coord_t*)Memory_Alloc(sizeof(coord_t) * 4);
	
	Com_Memset(playerSpawns, 0, sizeof(coord_t) * 4);
	Com_Memset(characters, 0, sizeof(character_t) * b_maxplayers);
	
	// get map asset
	assetHeader_t* asset = Assets_Get("map", ASSET_MAP);
	map = (map_t*)asset->data;
	
	// setup spawns info
	playerSpawns[0].x = map->spawns[0].x;
	playerSpawns[0].y = map->spawns[0].y;
	
	playerSpawns[1].x = map->spawns[0].x;
	playerSpawns[1].y = map->spawns[0].y;

	for(uint32_t i = 0; i < b_maxplayers; i ++)
	{
		// setup player info
		characters[i].absolute_position.x = playerSpawns[i].x << 8;
		characters[i].absolute_position.y = playerSpawns[i].y << 8;

		characters[i].screen_position.x = 0;
		characters[i].screen_position.y = 0;

		characters[i].player_speed.x = 0;
		characters[i].player_speed.y = 0;

		characters[i].direction = DIRECTION_RIGHT;
		
		characters[i].frame = 0;
		characters[i].frameInfo = NULL;
		characters[i].frameStatus = 0;
		characters[i].frameCycles = 0;
		characters[i].frames = NULL;
		characters[i].fighter = NULL;
		characters[i].sprite = NULL;

		characters[i].jump = JUMP_GROUND;
		characters[i].drop = 0;
		characters[i].damage = 0;
		characters[i].kills = 0;
		characters[i].falls = 0;
		characters[i].inmortal = true;
		characters[i].spawn = g_spawn;
		characters[i].canplay = true;
		characters[i].action = ACTION_READY;
		characters[i].lastAction = ACTION_READY;
		characters[i].shield = g_shield;
		characters[i].hit_by = -1;
		
		// set networking data, just in case
		characters[i].addr.ip = 0;
		characters[i].addr.port = 0;
	}

	// setup cpu targets
	cpuTarget[0].position.x = 120;
	cpuTarget[0].position.y = 385;

	cpuTarget[1].position.x = 355;
	cpuTarget[1].position.y = 380;

	cpuTarget[2].position.x = 215;
	cpuTarget[2].position.y = 385;

	cpuTarget[3].position.x = 210;
	cpuTarget[3].position.y = 270;

	cpuTarget[4].position.x = 280;
	cpuTarget[4].position.y = 270;

	cpuTarget[5].position.x = 315;
	cpuTarget[5].position.y = 380;

	// TODO: Figure out what the hell this does
	cpuTarget[0].leftlimit = 0;
	cpuTarget[0].rightlimit = 0;

	// set camera focus
	camera.focus = cl_clientnum;

	// setup camera size
	camera.size.x = R_SCREEN_WIDTH;
	camera.size.y = R_SCREEN_HEIGHT;

	// setup camera position
	camera.position.x = (characters[camera.focus].absolute_position.x >> 8) - (camera.size.x / 2);
	camera.position.y = (characters[camera.focus].absolute_position.y >> 8) - (camera.size.y / 2);

	b_background = R_PrecacheBackground(asset, 0, 3, false);
	R_DisplayBackground(b_background);
	R_ScrollBackground(b_background, 0, 0);

	cl_inbattle = true;

	Com_Printf(Com_LogLevel_Info, "Battle_Setup: battle engine started\n");
}

bool Battle_CharacterNextToPoint(uint32_t sx, uint32_t sy, uint32_t player)
{
	int32_t x = characters[player].absolute_position.x + ((characters[player].fighter->image->size.x / 2) << 8);
	int32_t y = characters[player].absolute_position.y + ((characters[player].fighter->image->size.y / 2) << 8);
	
	int32_t pMinx = sx - G_ATKWEIGHT;
	int32_t pMiny = sy - G_ATKHEIGHT;
	
	int32_t pMaxx = sx + G_ATKWEIGHT;
	int32_t pMaxy = sy + G_ATKHEIGHT;
	
	return (pMinx <= x) && (pMiny <= y) && (pMaxx >= x) && (pMaxy >= y);
}

bool Battle_TouchingGround(uint8_t player)
{
   int32_t x = (characters[player].absolute_position.x >> 8) + (characters[player].fighter->image->size.x / 2);
   int32_t y = (characters[player].absolute_position.y >> 8) + characters[player].fighter->image->size.y;
   
   uint8_t color = Cmap_GetPoint(x, y);

   return color == 4 || (characters[player].drop < 120 && color == 3);
}

bool Battle_TouchingCeiling(uint8_t player)
{
   int32_t x = (characters[player].absolute_position.x >> 8) + (characters[player].fighter->image->size.x / 2);
   int32_t y = characters[player].absolute_position.y >> 8;
   
   uint8_t color = Cmap_GetPoint(x, y);
   
   return y < 0 || color == 4;
}   

bool Battle_FallenOut(uint8_t player)
{
	int32_t x1 = (characters[player].absolute_position.x >> 8) + characters[player].fighter->image->size.x;
	int32_t x2 = characters[player].absolute_position.x >> 8;
	int32_t y1 = (characters[player].absolute_position.y >> 8) + characters[player].fighter->image->size.y;
	
	return (x1 < 0) || (x2 > map->size.x) || (y1 > map->size.y);
}

void Battle_LoadCharacterFrames(uint32_t player, frames_t* from)
{
	// this has been already loaded
	if(characters[player].frameInfo == from || from == NULL || from->count == 0) return;

	if(characters[player].frames != NULL)
	{
		// unload every frame already loaded
		for(int i = 0; i < characters[player].frameInfo->count; i ++)
		{
			R_UnloadMaterial(characters[player].frames[i]); // unload materials
		}

		Memory_Free(characters[player].frames);
	}

	// we should be able to have those in memory without problems
	characters[player].frames = (material_t**)Memory_Alloc(sizeof(material_t*) * from->count);
	
	// load every material and assing it to the array
	for(int i = 0; i < from->count; i ++)
	{
		characters[player].frames[i] = R_LoadMaterial(from->frames[i], (image_t*) from->frames[i]->data, true);
	}

	// update frame info
	characters[player].frameInfo = from;
	characters[player].frame = 0;
	characters[player].frameCycles = 0;
	characters[player].frameStatus = 0;
}

void Battle_Frame()
{
	// update it everytime just in case it gets modified with new data
	camera.focus = cl_clientnum;

	// update camera position with character sprite
	camera.position.x = (characters[camera.focus].absolute_position.x >> 8) - (camera.size.x / 2) + (characters[camera.focus].fighter->image->size.x / 2);
	camera.position.y = (characters[camera.focus].absolute_position.y >> 8) - (camera.size.y / 2) + (characters[camera.focus].fighter->image->size.y / 2);

	// make sure camera doesnt get out of the screen
	if(camera.position.x < 0) camera.position.x = 0;
	if(camera.position.y < 0) camera.position.y = 0;
	if(map->size.x < camera.position.x + camera.size.x) camera.position.x = map->size.x - camera.size.x;
	if(map->size.y < camera.position.y + camera.size.y) camera.position.y = map->size.y - camera.size.y;

	// scroll the bg
	R_ScrollBackground(b_background, camera.position.x, camera.position.y);
	
	// update positions for characters
	for(uint8_t i = 0; i < b_maxplayers; i ++)
	{
		// handle actions
		switch(characters[i].action)
		{
			case ACTION_SHIELD:
				{
					if(characters[i].shield > 0)
					{
						characters[i].shield -= 2;
					}
					else
					{
						characters[i].action = ACTION_STAND;
					}
				}
				break;
				
			case ACTION_READY:
				{
					if(characters[i].spawn > 0)
					{
						characters[i].spawn -= 2;
					}
					else
					{
						characters[i].inmortal = false;
						characters[i].action = ACTION_STAND;
					}
				}
				break;
			
			case ACTION_KICK:
				{				   
					// check proximity
					for(uint32_t n = 0; n < b_maxplayers; n ++)
					{
					   if(
								(n != i) &&
								characters[n].inmortal == false &&
								Battle_CharacterNextToPoint(
																characters[i].absolute_position.x + ((characters[i].fighter->image->size.x / 2) << 8),
																characters[i].absolute_position.y + ((characters[i].fighter->image->size.y / 2) << 8),
																n
															  )
						  )
						  {
						     // increase rival damage
						     // uint8_t damage = rand() % 20;
							  // WARNING: DO NOT USE RANDS!!!
							  // TODO: Properly generate this data!
							  uint8_t damage = 8;
						     
						     characters[n].damage += damage; // randomize this a bit
						     characters[n].hit_by = i;
						     
						     // make the rival move a bit
						     if(characters[n].absolute_position.x > characters[i].absolute_position.x)
									characters[n].player_speed.x += characters[n].damage << 4;
							  else
							  		characters[n].player_speed.x -= characters[n].damage << 4;
						  }
					}
					
					// clear action
					characters[i].action = ACTION_STAND;
				}
				break;
				
			case ACTION_LOWKICK:
			   {			      
			      // check proximity
			      for(uint32_t n = 0; n < b_maxplayers; n ++)
			      {
			         if(
			         		(n != i) &&
			         		characters[n].inmortal == false &&
			         		Battle_CharacterNextToPoint(
			         										characters[i].absolute_position.x + ((characters[i].fighter->image->size.x / 2) << 8),
			         										characters[i].absolute_position.y + ((characters[i].fighter->image->size.y / 2) << 8),
			         										n
			         									)
			         	)
			         	{
			         	   // increase rival damage
						     // uint8_t damage = rand() % 20;
							  // WARNING: DO NOT USE RANDS!!!
							// TODO: Properly generate this data!
							  uint8_t damage = 8;
			         	   
			         	   characters[n].damage += damage; // randomize this a bit
			         	   characters[n].hit_by = i;
			         	   
			         	   // make the rival move a bit
			         	   int32_t difference = (characters[n].screen_position.x - characters[i].screen_position.x) << 4;
			         	   
			         	   characters[n].player_speed.x += difference;
			         	   
			         	   characters[n].player_speed.y -= characters[n].damage << 5;
			         	}   
			      }   
			   }   
			   break;
				
			case ACTION_PUNCH:
				{
					// TODO: check proximity
					
					// clear action
					characters[i].action = ACTION_STAND;
				}
				break;
				
			case ACTION_SMASH:
				{
					// smash attack
				}
				break;
				
			default:
				break;
		}
		
		if(characters[i].action != ACTION_READY)
		{
			if(characters[i].player_speed.y < 0)
			{
				characters[i].action = ACTION_JUMP;
			}
			else if(characters[i].player_speed.y > 0)
			{
				characters[i].action = ACTION_FALL;
			}
			/*else if(characters[i].player_speed.x != 0)
			{
				characters[i].action = ACTION_WALK;
			}*/
			else
			{
				if(characters[i].action != ACTION_SHIELD &&
					characters[i].action != ACTION_FALL &&
					characters[i].action != ACTION_CROUCH)
				{
					characters[i].action = ACTION_STAND;
				}

				// load frames and set data
				if(characters[i].lastAction != characters[i].action && characters[i].action == ACTION_STAND)
				{
					Battle_LoadCharacterFrames(i, characters[i].fighter->frames.stand);
				}
			}
			
			characters[i].lastAction = characters[i].action;
			characters[i].player_speed.y += G_GRAVITY;
			   
			characters[i].absolute_position.x += characters[i].player_speed.x;
			characters[i].absolute_position.y += characters[i].player_speed.y;
			
			if(characters[i].player_speed.x >= G_GRAVITY && Battle_TouchingGround(i) == true)
			{
				characters[i].player_speed.x -= G_GRAVITY;
			}
			else if(characters[i].player_speed.x <= -G_GRAVITY && Battle_TouchingGround(i) == true)
			{
				characters[i].player_speed.x += G_GRAVITY;
			}
			
			if(Battle_TouchingGround(i) == true && characters[i].player_speed.y >= 0)
			{
			   characters[i].absolute_position.y -= 256;
			   characters[i].player_speed.y = 0;
			   characters[i].jump = JUMP_GROUND;
			   characters[i].action = ACTION_STAND;
			}
			else if (Battle_TouchingCeiling(i) == true)
			{
			   if(characters[i].player_speed.y < 0)
			   {
			      characters[i].player_speed.y = G_GRAVITY;
			   }
			}
			
			if(characters[i].player_speed.y == 0 && characters[i].player_speed.x < G_GRAVITY && characters[i].player_speed.x > -G_GRAVITY)
			{
			   characters[i].player_speed.x = 0;
			}
			
			if(Battle_FallenOut(i) == true)
			{
			   // restart player position and vars
			   characters[i].player_speed.x = characters[i].player_speed.y = 0; // speed
			   characters[i].absolute_position.x = playerSpawns[i].x << 8; // x
			   characters[i].absolute_position.y = playerSpawns[i].y << 8; // y
			   
			   // character data
			   characters[i].falls = 0;
			   characters[i].damage = 0;
			   characters[i].spawn = g_spawn;
			   characters[i].action = ACTION_READY;
			   characters[i].shield = g_shield;
			   characters[i].inmortal = true;
			   characters[i].falls ++;
			   
			   if(characters[i].hit_by >= 0 && characters[i].hit_by < b_maxplayers)
			   {
			      characters[characters[i].hit_by].kills ++;
			      characters[i].hit_by = 0;
			   }   
			   
			   // skip the rest of checks
			   continue;
			}
		}
		
		// update screen position for players
		characters[i].screen_position.x = (characters[i].absolute_position.x >> 8) - camera.position.x;
		characters[i].screen_position.y = (characters[i].absolute_position.y >> 8) - camera.position.y;

		// we only need to take care of animated sprites
		if(characters[i].frameInfo != NULL && characters[i].frameInfo->count > 0)
		{
			// update frame status just in case
			characters[i].frameStatus += characters[i].frameInfo->speed << G_FRAMESHIFT;

			// take care of frame status here
			if(characters[i].frameStatus >= G_FRAMELIMIT)
			{
				characters[i].frame ++;

				// make sure we stay on limits
				if(characters[i].frame == characters[i].frameInfo->count)
				{
					characters[i].frame = 0;
					characters[i].frameCycles ++;
				}

				// make sure we start off the begining with the next frame
				characters[i].frameStatus = 0;
			}
		}

		if(
			characters[i].screen_position.x < -characters[i].fighter->image->size.x ||
			 characters[i].screen_position.y < -characters[i].fighter->image->size.y ||
			 characters[i].screen_position.x > camera.size.x ||
			 characters[i].screen_position.y > camera.size.y
		  )
		{
			if(characters[i].sprite != NULL) // NULL when not shown, or at least it should
			{
				R_HideMaterial(characters[i].sprite);
				characters[i].sprite = NULL;
				characters[i].direction = DIRECTION_NONE;
			}
		}
		else
		{
			if(characters[i].frameInfo != NULL)
			{
				// this actually switched image based on frames
				if(characters[i].lastFrame != characters[i].frame)
				{
					// hide current frame
					if(characters[i].sprite != NULL)
					{
						R_HideMaterial(characters[i].sprite);
						characters[i].sprite = NULL;
					}

					// show next frame
					characters[i].sprite = R_DisplayMaterial(
						characters[i].frames[characters[i].frame],
						characters[i].screen_position.x,
						characters[i].screen_position.y);

					characters[i].lastFrame = characters[i].frame; // make sure we do not reload it
				}
				else
				{
					// make sure our current frame is displayed
					if(characters[i].sprite == NULL)
						characters[i].sprite = R_DisplayMaterial(
						characters[i].frames[characters[i].frame],
						characters[i].screen_position.x,
						characters[i].screen_position.y);
				}
			}

			uint32_t x = characters[i].screen_position.x;

			// we need to flip them no matter what...
			R_FlipMaterial(characters[i].sprite, characters[i].direction == DIRECTION_LEFT);
			
			// we must correct the sprite only when flipped as flipping is just using a negative width size for it
			if(R_MaterialFlipped(characters[i].sprite) == true) x += characters[i].fighter->image->size.x;
			
			// move the sprite on the screen
			R_UpdateMaterial(characters[i].sprite, x, characters[i].screen_position.y);
		}
	}
}

void Battle_Input(keys_t* keys)
{
	uint8_t player = 0;

	// translate the ip to player
	for(player = 0; player < b_maxplayers; player ++)
	{
		if(
			characters[player].addr.ip == keys->from->ip &&
			characters[player].addr.port == keys->from->port
			)
		{
		    break;
		}   
	}   

	Com_Assert(player == b_maxplayers, "Battle_Input: player overflow\n");
	
	// movement
	if(characters[player].action != ACTION_READY)
	{
		if(characters[player].action != ACTION_SHIELD)
		{
			if((keys->down & KEY_UP) && !(keys->held & KEY_B) && (characters[player].jump != JUMP_FINAL))
			{
				characters[player].player_speed.y = -1500;
				characters[player].jump = (jumpState_t)(characters[player].jump + JUMP_BASIC);

				Battle_LoadCharacterFrames(player, characters[player].fighter->frames.jump);
			}

			if( (keys->down & KEY_UP) && (keys->held & KEY_B) && (characters[player].jump != JUMP_FINAL) )
			{
				characters[player].player_speed.y = -1500;
				characters[player].jump = (jumpState_t)(characters[player].jump + JUMP_BASIC);

				Battle_LoadCharacterFrames(player, characters[player].fighter->frames.jump);
			}
		
			if(keys->held & KEY_LEFT)
			{
				if(characters[player].drop == 0) characters[player].absolute_position.x += -((characters[player].running == true) ? 2 * g_speed : g_speed);

				characters[player].direction = DIRECTION_LEFT;
			}
			
			if(keys->held & KEY_RIGHT)
			{
				if(characters[player].drop == 0) characters[player].absolute_position.x += ((characters[player].running == true) ? 2 * g_speed : g_speed);
				characters[player].direction = DIRECTION_RIGHT;
			}
			
			if(keys->held & KEY_DOWN && keys->down & KEY_Y)
			{
			   characters[player].action = ACTION_LOWKICK;
			   characters[player].drop = 0;
			}
			else if(keys->down & KEY_A)
			{
			   characters[player].action = ACTION_PUNCH;
			}
			else if(keys->down & KEY_Y)
			{
			   characters[player].action = ACTION_KICK;
			}
			else if(keys->held & KEY_DOWN && characters[player].action != ACTION_JUMP)
			{
			   if(characters[player].lastAction != ACTION_CROUCH && (characters[player].lastAction != ACTION_FALL || characters[player].action != ACTION_FALL))
			   {
				   // change to crouch animation
				   Battle_LoadCharacterFrames(player, characters[player].fighter->frames.crouch);
			   }
			   
			   characters[player].drop += 8;
			   characters[player].action = ACTION_CROUCH;
			}
			else
			{
			   characters[player].drop = 0;

			   if(characters[player].action != ACTION_FALL)
				   characters[player].action = ACTION_STAND;
			}

			if(keys->held & KEY_B)
			{
				characters[player].running = true;
			}
		}
		else
		{
			if( (keys->held & KEY_R) && (keys->held & KEY_L) )
			{
				characters[player].action = ACTION_SHIELD;
			}
			else
			{
				characters[player].action = ACTION_STAND;
				
				if(characters[player].shield == 0)
				{
					characters[player].shield = g_shield;
				}
			}
		}		
	}
	else
	{
		uint32_t mask = KEY_A | KEY_B | KEY_X | KEY_Y | KEY_UP | KEY_DOWN | KEY_LEFT | KEY_RIGHT;
		
		if(keys->held & mask)
		{
			characters[player].spawn = 0;
		}
	}
	
	// we must free adr_t now
	Memory_Free(keys->from);
	Memory_Free(keys);
}

void Battle_Event(event_t* ev)
{
	switch(ev->value)
	{
		 case EVENT_KEYPRESS:
		 case EVENT_KEYPRESSRELAY:
			 Battle_Input((keys_t*)ev->data);
			 break;

		 case EVENT_BATTLESYNC:
			 Battle_Sync((msg_t*)ev->data);
			 break;

		 default:
			 Com_Error("Battle_Event: unknown event type\n");
			 break;
	}
}

void Battle_Sync(msg_t* msg)
{
	// check maximum number of updates needed
	uint8_t players = MSG_ReadByte(msg);

	// load data for every user
	for(uint8_t current = 0; current < players; current ++)
	{
		uint8_t player = MSG_ReadByte(msg);

		// read the status
		characters[player].status = MSG_ReadByte(msg);

		// skip player if is not connected
		if(characters[player].status == 0) continue;

		// update address data
		characters[player].addr.ip = MSG_ReadInteger(msg);
		characters[player].addr.port = MSG_ReadShort(msg);

		characters[player].canplay = (MSG_ReadByte(msg) > 0) ? true : false;

		characters[player].absolute_position.x = (int32_t)MSG_ReadInteger(msg);
		characters[player].absolute_position.y = (int32_t)MSG_ReadInteger(msg);

		characters[player].player_speed.x = (int32_t)MSG_ReadInteger(msg);
		characters[player].player_speed.y = (int32_t)MSG_ReadInteger(msg);

		characters[player].damage = MSG_ReadShort(msg);
		characters[player].jump = (jumpState_t)MSG_ReadByte(msg);
		characters[player].lifes = MSG_ReadByte(msg);
		characters[player].spawn = MSG_ReadShort(msg);
		characters[player].shield = (int16_t)MSG_ReadShort(msg);
		characters[player].inmortal = MSG_ReadByte(msg) == 1;

		characters[player].kills = MSG_ReadByte(msg);
		
		characters[player].action = (actionState_t)MSG_ReadByte(msg);
		characters[player].direction = (direction_t)MSG_ReadByte(msg);
	}

	Memory_Free(msg->data);
	Memory_Free(msg);

	Client_Sync(characters);
}

msg_t* Battle_SyncData(client_t* sv_clients)
{
	// creat msg_t
	msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));

	// initialize msg_t struct
	MSG_Init(msg, NULL, 1);

	// write event type
	MSG_WriteByte(msg, EVENT_BATTLESYNC);

	// write players count
	MSG_WriteByte(msg, b_maxplayers);

	for(uint8_t players = 0; players < b_maxplayers; players ++)
	{
		// update addresses first
		if(sv_clients[players].addr != NULL)
		{
			Com_Memcpy(&characters[players].addr, sv_clients[players].addr, sizeof(adr_t));

			// set player status
			characters[players].status = 1;
		}

		Com_Printf(Com_LogLevel_Debug, "Writing sync data for player %d\n", players);
		MSG_WriteByte(msg, players);

		// player status (1 implies fully connected character)
		MSG_WriteByte(msg, characters[players].status);

		// status == 0 => not connected, we can skip it
		if(characters[players].status == 0) continue;

		// write address info
		MSG_WriteInteger(msg, characters[players].addr.ip);
		MSG_WriteShort(msg, characters[players].addr.port);

		// movement status (cam be hit?)
		MSG_WriteByte(msg, characters[players].canplay);

		// position
		MSG_WriteInteger(msg, characters[players].absolute_position.x);
		MSG_WriteInteger(msg, characters[players].absolute_position.y);

		// current gravity parameters
		MSG_WriteInteger(msg, characters[players].player_speed.x);
		MSG_WriteInteger(msg, characters[players].player_speed.y);

		// fight information (damage, lifes, shield, etc)
		MSG_WriteShort(msg, characters[players].damage);
		MSG_WriteByte(msg, characters[players].jump);
		MSG_WriteByte(msg, characters[players].lifes);
		MSG_WriteShort(msg, characters[players].spawn);
		MSG_WriteShort(msg, characters[players].shield);
		MSG_WriteByte(msg, characters[players].inmortal);

		// kills
		MSG_WriteByte(msg, characters[players].kills);

		// current action
		MSG_WriteByte(msg, characters[players].action);

		// current direction
		MSG_WriteByte(msg, characters[players].direction);
	}

	return msg;
}

void Battle_SetPlayerAsset(uint8_t player, const char* spr)
{
	Com_Assert(b_maxplayers < player, "Battle_SetPlayerAsset: player overflow\n");

	// search sprite and pallete
	assetHeader_t* asset = Assets_Get(spr, ASSET_CHARACTER);

	Com_Assert(asset == NULL, "Battle_SetPlayerAsset: cannot find asset \"%s\"\n", spr);

	fighter_t* fighter = (fighter_t*) asset->data;

	characters[player].fighter = fighter;

	// load all the frames for the character
	Battle_LoadCharacterFrames(player, characters[player].fighter->frames.stand);
}
