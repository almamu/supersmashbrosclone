#include "common.h"
#include "input.h"
#include "events.h"
#include "network.h"
#include "server.h"
#include "client.h"

extern bool cl_inbattle;
extern uint8_t cl_clientnum;
extern bool cl_connected;
extern uint8_t dedicated;
extern adr_t net_adr;
uint32_t last_down;
uint32_t last_held;

void Input_Event(event_t* ev)
{
   // what about direct modification for sv_clients? probably not good?
   // shall we use events?
	keys_t* keys = (keys_t*)(ev->data);

	if(!keys)
	{
		Com_Error("Input_Event: not-allocated data\n");
	}
	
	// check who should handle this
	if(cl_connected == false)
	{
		Server_KeyInput(keys);
	}
	
	if(ev->type == EVENT_KEYPRESSRELAY || cl_connected == true)
	{
		Client_KeyInput(keys);
	}

	// free keys
	Memory_Free(keys->from);
	Memory_Free(keys);
}

void Input_Frame()
{
	scanKeys();

	// get keys tatus
	uint32_t down = keysDown() & KEY_MASK;
	uint32_t held = keysHeld() & KEY_MASK;
	
	// skip update if not needed
	if(down == last_down && held == last_held)
	{
	   return;
	}
	
	last_down = down;
	last_held = held; 
	
	// build msg_t data
	msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));
	MSG_Init(msg, NULL, 1);
	
	// write adr_t
	MSG_WriteData(msg, &net_adr, sizeof(adr_t));
	
	// write type
	MSG_WriteByte(msg, EVENT_KEYPRESS);
	
	// write key data
	MSG_WriteInteger(msg, held);
	MSG_WriteInteger(msg, down);
	
	// queue network event and local event
	Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);
}
