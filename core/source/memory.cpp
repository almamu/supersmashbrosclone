#include "common.h"
#include "memory.h"

memoryChunk_t* memory_first = NULL;
memoryChunk_t* memory_last = NULL;

void Memory_Init()
{
#ifdef MEM_DEBUG
	// alloc the base memoryChunk_t
	memory_first = (memoryChunk_t*)malloc(sizeof(memoryChunk_t));

	// set initial values
	Com_Memset(memory_first, 0, sizeof(memoryChunk_t));

	memory_first->next = NULL;
	memory_first->length = strlen(MEMORY_VERSION) + 1;
	memory_first->data = (void*)MEMORY_VERSION;
	memory_first->previous = NULL;

	// make sure memory_last is not null so we can add some more there
	memory_last = memory_first;
#endif /* MEM_DEBUG */
}

#ifdef MEM_DEBUG
void* Memory_Alloc(uint32_t length)
{
	Com_Assert(length == 0, "Memory_Alloc: allocating 0 bytes!\n");
	Com_Printf(Com_LogLevel_EngineDebug, "Memory_Alloc: allocating %d bytes\n", length);

	// memory_first alloc the memoryChunk_t entry
	memoryChunk_t* chunk = (memoryChunk_t*)malloc(sizeof(memoryChunk_t));

	// set initial values
	Com_Memset(chunk, 0, sizeof(memoryChunk_t));

	chunk->next = NULL;
	chunk->length = length;
	chunk->data = malloc(chunk->length);
	chunk->previous = memory_last;

	// update 'iterators' to make sure we can continue adding data to memory
	memory_last->next = chunk;
	memory_last = chunk;

	return chunk->data;
}

void* Memory_Realloc(void* data, uint32_t length)
{
	Com_Printf(Com_LogLevel_EngineDebug, "Memory_Realloc: re-allocating %d bytes\n", length);
	Com_Assert(data == NULL, "Memory_Realloc: pointer is null\n");
	Com_Assert(length == 0, "Memory_Realloc: length is 0, this will dealloc it\n");

	// find the correct memoryChunk_t entry
	memoryChunk_t* chunk = memory_first;

	while(chunk != NULL)
	{
		// compare addresses
		if(chunk->data == data)
		{
			break;
		}

		chunk = chunk->next;
	}

	Com_Assert(chunk == NULL, "Memory_Realloc: reallocating an inexistent block\n");

	// realloc and update data
	data = chunk->data = realloc(data, length);
	chunk->length = length;

	return data;
}

int Memory_Info(void* data)
{
	memoryChunk_t* chunk = memory_first;

	while(chunk != NULL)
	{
		if(chunk->data == data)
		{
			return chunk->length;
		}

		chunk = chunk->next;
	}

	Com_Assert(chunk == NULL, "Memory_Info: Cannot find block info for %x\n", data);

	return NULL;
}

void Memory_Free(void* data)
{
	Com_Printf(Com_LogLevel_EngineDebug, "Memory_Free: freeing block\n");

	// find the correct memoryChunk_t entry
	memoryChunk_t* chunk = memory_first;

	while(chunk != NULL)
	{
		// compare addresses
		if(chunk->data == data)
		{
			break;
		}

		chunk = chunk->next;
	}

	Com_Assert(chunk == NULL, "Memory_Free: freeing an inexistent block\n");

	// memory_first free data
	free(data);

	// remove ourselves from the linked list
	if(chunk->previous != NULL)
	{
		chunk->previous->next = chunk->next;

		if(chunk->next != NULL)
		{
			chunk->next->previous = chunk->previous;
		}
		else
		{
			memory_last = chunk->previous;
		}
	}

	// now we can safely free the chunk
	free(chunk);
}

#endif

void* Memory_Clone(void* data, uint32_t length)
{
#ifdef MEM_DEBUG
	Com_Printf(Com_LogLevel_EngineDebug, "Memory_Clone: cloning block of %d bytes\n", length);
#endif /* MEM_DEBUG */

	// create a clone of it
	void* dst = Memory_Alloc(length);
	Com_Memcpy(dst, data, length);

	return dst;
}
