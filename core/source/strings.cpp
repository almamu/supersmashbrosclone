#include "common.h"

char* String_Create(int length)
{
	length = length + 1;

	// first alloc space for string
	char* str = (char*)Memory_Alloc(length);

	// make sure the string is correct
	Com_Memset(str, 0, length);

	return str;
}

char* String_Clone(const char* string)
{
	// get string length
	int length = strlen(string);

	// create memory block for string data
	char* str = String_Create(length);

	// copy string
	strcpy(str, string);

	return str;
}

void String_Destroy(char* string)
{
	Memory_Free(string);
}