#include "common.h"
#include "battle.h"
#include "network.h"
#include "events.h"
#include "client.h"
#include "server.h"
#include "input.h"
#include "messages.h"

#include <time.h>

extern uint16_t net_port;
extern bool cl_inbattle;
extern uint8_t dedicated;
extern uint8_t b_maxplayers;
extern adr_t net_adr;
bool cl_connected;

uint32_t handshake_seed = 0;
uint32_t handshake = 0;

uint8_t cl_clientnum = 0;
uint8_t cl_maxplayers = 0;

int64_t cl_lastTime = 0;
int64_t cl_lastServerTime = 0;

basicClient_t* cl_clients = NULL;

adr_t cl_srv;

void Client_Init()
{
	Com_Memset(&cl_srv, 0, sizeof(adr_t));

	cl_connected = false;

	if(cl_clients) Memory_Free(cl_clients);
}

void Client_Connect(adr_t* adr)
{
	// copy data to cl_srvAdr
	Com_Memcpy(&cl_srv, adr, sizeof(adr_t));

	// randomize local port so we can run both client and server
	// on the same machine
	net_port = rand() % 0xFFFF;
	// net_port = 2501;

	if(adr->ip != ADDR_SELF)
	{
		// call connect function here
		Com_Printf(Com_LogLevel_Debug, "Experimental connection system starting\n");

		// initialize udp socket
		Net_UdpInit();

		// set client mode
		Net_UdpClient(cl_srv);
	}
	else
	{
		net_adr.ip = ADDR_SELF;
		net_adr.port = net_port;
	}

	// build handshake generation message
	msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));

	MSG_Init(msg, NULL, 1);

	handshake_seed = time(NULL);
	srand(handshake_seed);

	handshake = rand() % 0xFFFFFFFF;

	// write adr
	MSG_WriteData(msg, &net_adr, sizeof(adr_t));

	// write message type
	MSG_WriteByte(msg, EVENT_HANDSHAKEREQ);

	// write version data
	MSG_WriteByte(msg, PROTOCOL);

	// write handshake seed
	MSG_WriteInteger(msg, handshake_seed);

	// queue output event
	Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);

	// prepare new message
	msg = (msg_t*)Memory_Alloc(sizeof(msg_t));

	MSG_Init(msg, NULL, 1);

	// write adr
	MSG_WriteData(msg, &net_adr, sizeof(adr_t));

	// send handshake to server
	MSG_WriteByte(msg, EVENT_HANDSHAKE);

	// write handshake data
	MSG_WriteInteger(msg, handshake);

	// queue output event
	Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);

	// setup time data
	cl_lastServerTime = time(NULL);
	cl_lastTime = time(NULL);
}

void Client_Frame()
{
	if(dedicated || !cl_connected) return;

	time_t diff = difftime(time(NULL), cl_lastTime);

	// fire key events
	if(cl_inbattle || dedicated > 0)
	{
		for(uint8_t i = 0; i < cl_maxplayers; i ++)
		{
			if(cl_clients[i].keys.down != 0 || cl_clients[i].keys.held != 0)
			{
				keys_t* keys = (keys_t*) Memory_Alloc(sizeof(keys_t));
				keys->from = (adr_t*) Memory_Alloc(sizeof(adr_t));

				keys->down = cl_clients[i].keys.down;
				keys->held = cl_clients[i].keys.held;
				Com_Memcpy(keys->from, cl_clients[i].adr, sizeof(adr_t));

				Sys_QueueEvent(EVENT_BATTLE, keys, EVENT_KEYPRESS, 0);
			}
		}
	}

	// by the moment just check times
	if(diff > 5)
	{
		cl_lastTime = time(NULL);

		// send client ping
		msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));

		MSG_Init(msg, NULL, 1);

		// add needed data
		MSG_WriteData(msg, &net_adr, sizeof(adr_t));
		MSG_WriteByte(msg, EVENT_SERVERPING);
		MSG_WriteLong(msg, time(NULL));

		Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_OUTPUT, 0);
	}

	diff = difftime(time(NULL), cl_lastServerTime);
	
	if(diff > 10)
	{
		Com_Error("Lost connection to remote server\n");
	}
}

void Client_Event(event_t* ev)
{
	switch(ev->type)
	{
		case EVENT_HANDSHAKERES:
			{
				Com_Printf(Com_LogLevel_Debug, "Received handshakeres from server!\n");

				handshakeRes_t* res = (handshakeRes_t*)ev->data;

				if(res->result == false) Com_Error("Wrong handshake\n");

				cl_clientnum = res->player;
				cl_maxplayers = res->maxplayers;

				Memory_Free(res);

				// alloc data for clients
				cl_clients = (basicClient_t*)Memory_Alloc(sizeof(basicClient_t) * cl_maxplayers);

				// empty the struct
				Com_Memset(cl_clients, 0,  sizeof(basicClient_t) * cl_maxplayers);

				cl_connected = true;
			}
			break;

		case EVENT_HANDSHAKEFAIL:
			{
				switch(ev->value)
				{
					case 0:
						Com_Error("Server protocol is different\n");
						break;

					case 1:
						Com_Error("Server has no enough free slots\n");
						break;

					case 2:
						Com_Error("Handshake mismatch\n");
						break;

					default:
						Com_Error("Unknown error message\n");
						break;
				}
			}
			break;

		case EVENT_CLIENTPING:
			{
				// update server time
				msg_t* msg = (msg_t*) ev->data;

				// skip address and type
				msg->current = sizeof(adr_t) + 1;

				// read ping time
				cl_lastServerTime = MSG_ReadLong(msg);

				Memory_Free(msg->data);
				Memory_Free(msg);
			}
			break;

		default:
			Com_Error("Unknown event %d\n", ev->type);
			break;
	}
}

void Client_KeyInput(keys_t* keys)
{
   uint32_t index = 0;
   
   for(index = 0; index < cl_maxplayers; index ++)
   {
      if(
		  cl_clients[index].adr != NULL &&
		  cl_clients[index].adr->ip == keys->from->ip &&
		  cl_clients[index].adr->port == keys->from->port
		  )
      {
         break;
      }   
   }
	
	if(index == cl_maxplayers)
	{
	   Com_Error("Client_KeyInput: players overflow\n");
	}
	
	cl_clients[index].keys.held = keys->held;
	cl_clients[index].keys.down = keys->down;
}

void Client_Sync(character_t* characters)
{
	// skip sync if players count is not the same
	if(b_maxplayers != cl_maxplayers || cl_connected == false) return;

	// update addresses
	for(uint8_t index = 0; index < b_maxplayers; index ++)
	{
		if(characters[index].status == 0)
		{
			cl_clients[index].connected = false;

			if(cl_clients[index].adr != NULL) Memory_Free(cl_clients[index].adr);

			continue;
		}
		
		if(cl_clients[index].adr == NULL) cl_clients[index].adr = (adr_t*)Memory_Alloc(sizeof(adr_t));

		// copy address data
		Com_Memcpy(cl_clients[index].adr, &characters[index].addr, sizeof(adr_t));

		cl_clients[index].connected = true;
	}
}
