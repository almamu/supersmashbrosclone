#include "common.h"
#include "cmap.h"

cmap_t* cmap_data = NULL;

void Cmap_Load(const char* file, uint16_t width, uint16_t height)
{
	Com_Assert(cmap_data != NULL, "Cmap_Load: please free cmap data first\n");
   
   // alloc cmap struct
   cmap_data = (cmap_t*)Memory_Alloc(sizeof(cmap_t));
   Com_Memset(cmap_data, 0, sizeof(cmap_t));
   
   // load dat file (tileset)
   char str[256];
   
   sprintf(str, "%s.dat", file);
   
   file_t* fp = FileSystem_Open(str, FileSystemMode_Read);
   
   Com_Assert(fp == NULL, "Cmap_Load: cannot open cmap file \"%s\"\n", str);

   cmap_data->tiles_size = fp->size;
   cmap_data->tiles = (uint8_t*)Memory_Alloc(cmap_data->tiles_size);
   
   Com_Assert(cmap_data->tiles == NULL, "Cmap_Load: cannot allocate space for tiles\n");
   
   FileSystem_Read(fp, sizeof(char), cmap_data->tiles_size, cmap_data->tiles);

	FileSystem_Close(fp);
	
	sprintf(str, "%s.cmp", file);
	
	fp = FileSystem_Open(str, FileSystemMode_Read);
	
	Com_Assert(fp == NULL, "Cmap_Load: cannot open cmap file \"%s\"\n", str);
	
	cmap_data->map_size = fp->size;	
	cmap_data->map = (uint8_t*)Memory_Alloc(cmap_data->map_size);
	
	Com_Assert(cmap_data->map == NULL, "Cmap_Load: cannot allocate space for map\n");
	   
	FileSystem_Read(fp, sizeof(char), cmap_data->map_size, cmap_data->map);
	
	FileSystem_Close(fp);
	
	cmap_data->width = width;
	cmap_data->height = height;
}

uint8_t Cmap_GetPoint(uint32_t x, uint32_t y)
{
   Com_Assert(cmap_data == NULL, "Cmap_GetPoint: cmap not loaded\n");
   
   if(x < 0 || y < 0 || x >= cmap_data->width || y >= cmap_data->height) return 0;
   
   uint16_t columns = cmap_data->width >> 3;
   
   uint16_t tile_x = x >> 3;
   uint16_t tile_y = (y >> 3) + 1;
   
   uint16_t pixel_x = x - (tile_x << 3);
   uint16_t pixel_y = (y + 8) - (tile_y << 3);
   
   int32_t address = (((tile_y * columns) + tile_x) << 1);
   uint8_t lobyte = *(cmap_data->map + address);
   uint8_t hibyte = *(cmap_data->map + (address + 1));
   uint16_t tile = (hibyte << 8) | lobyte;
   
   address = ((tile << 6) + (pixel_y << 3) + pixel_x);
   lobyte = *(cmap_data->tiles + address);
   
   return lobyte;
}
   
void Cmap_Free()
{
   if(cmap_data == NULL) return Com_Error("Cmap_Free: trying to free an already free-ed cmap data\n");
   if(cmap_data->tiles != NULL) Memory_Free(cmap_data->tiles);
   if(cmap_data->map != NULL) Memory_Free(cmap_data->map);
   
   Memory_Free(cmap_data);
   
   cmap_data = NULL;
}

