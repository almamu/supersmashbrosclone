#include "common.h"
#include "filesystem.h"
#include "mvars.h"

char* fs_basepath = NULL;

void FileSystem_Init(const char* basepath)
{
	if(basepath)
	{
		fs_basepath = String_Clone(basepath);
	}
	else
	{
		fs_basepath = "";
	}
}

file_t* FileSystem_Open(const char* filename, FileSystemMode mode)
{
	Com_Printf(Com_LogLevel_EngineDebug, "FileSystem_Open: Opening file %s in mode %d\n", filename, mode);

	// reserve some memory for file data
	file_t* file = (file_t*)Memory_Alloc(sizeof(file_t));
	Com_Memset(file, 0, sizeof(size_t));

	// guess the real opening mode and open the file
	switch (mode)
	{
		case FileSystemMode_Read:
			file->pointer = fopen(filename, "rb+");
			break;

		case FileSystemMode_Write:
			file->pointer = fopen(filename, "wb+");
			break;

		default:
		case FileSystemMode_Append:
			file->pointer = fopen(filename, "ab+");
			break;
	}

	// make sure we can open the file
	Com_Assert(file->pointer == NULL, "FileSystem_Open: Cannot open file %s", filename);

	// store filename
	file->filename = String_Clone(filename);

	// get file size
	fseek(file->pointer, 0, FileSystemSeek_End);
	file->size = ftell(file->pointer);
	fseek(file->pointer, 0, FileSystemSeek_Begin);

	// finally return file
	return file;
}

int FileSystem_Read(file_t* file, uint32_t size, uint32_t count, void* buffer)
{
	Com_Assert(file == NULL, "FileSystem_Read: File information is NULL");
	Com_Assert(file->pointer == NULL, "FileSystem_Read: File pointer is NULL");

	return fread(buffer, size, count, file->pointer);
}

int FileSystem_Write(file_t* file, uint32_t size, uint32_t count, void* buffer)
{
	Com_Assert(file == NULL, "FileSystem_Write: File information is NULL");
	Com_Assert(file->pointer == NULL, "FileSystem_Write: File pointer is NULL");

	return fwrite(buffer, size, count, file->pointer);
}

void FileSystem_Seek(file_t* file, int space, FileSystemSeek mode)
{
	fseek(file->pointer, space, (int)mode);
}

void FileSystem_Close(file_t* file)
{
	Com_Printf(Com_LogLevel_EngineDebug, "FileSystem_Close: Closing file %s\n", file->filename);
	fclose(file->pointer);

	Memory_Free(file->filename);
	Memory_Free(file);
}
