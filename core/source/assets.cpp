﻿#include "common.h"
#include "assets.h"
#include <graphics.h>
#include "cmap.h"

fastfile_t** fastfiles = NULL;
assetHeader_t** assets = NULL;

uint32_t ffCount = 0;
uint32_t assetsCount = 0;

// setup assets loaders/unloaders
assetLoader_t assetLoaders[] =
{
	{ASSET_MAP, MapAsset_Load},
	{ASSET_CHARACTER, CharacterAsset_Load},
	{ASSET_ICON, IconAsset_Load},
	{ASSET_GFX, GfxAsset_Load},
	{ASSET_SOUND, SoundAsset_Load},
	{ASSET_MENU, MenuAsset_Load},
	{ASSET_IMAGE, ImageAsset_Load},
	{ASSET_COLMAP, CollisionAsset_Load}
};

assetUnloader_t assetUnloaders[] =
{
	{ASSET_MAP, MapAsset_Unload},
	{ASSET_CHARACTER, CharacterAsset_Unload},
	{ASSET_ICON, IconAsset_Unload},
	{ASSET_GFX, GfxAsset_Unload},
	{ASSET_SOUND, SoundAsset_Unload},
	{ASSET_MENU, MenuAsset_Unload},
	{ASSET_IMAGE, ImageAsset_Unload},
	{ASSET_COLMAP, CollisionAsset_Unload}
};


void FastFile_Init()
{
	fastfiles = (fastfile_t**)Memory_Alloc(sizeof(fastfile_t*) * MAX_FASTFILES);
	assets = (assetHeader_t**)Memory_Alloc(sizeof(assetHeader_t*) * MAX_ASSETS);

	Com_Memset(fastfiles, 0, sizeof(fastfile_t*) * MAX_FASTFILES);
	Com_Memset(assets, 0, sizeof(assetHeader_t*) * MAX_ASSETS);
}

fastfile_t* FastFile_Load(const char* file)
{
	Com_Printf(Com_LogLevel_Info, "FastFile_Load: loading fastfile %s\n", file);

	file_t* fp = FileSystem_Open(file, FileSystemMode_Read);

	Com_Assert(fp == NULL, "FastFile_Load: cannot open fastfile %s\n", file);

	// alloc needed space
	fastfile_t* ff = (fastfile_t*)Memory_Alloc(sizeof(fastfile_t));

	Com_Memset(ff, 0, sizeof(fastfile_t));

	// read data
	FileSystem_Read(fp, sizeof(uint32_t), 1, &ff->count);

	// set fastfile name
	ff->name = String_Clone(file);

	// alloc list space
	ff->assets = (assetHeader_t**)Memory_Alloc(sizeof(assetHeader_t*) * ff->count);

	Com_Memset(ff->assets, 0, sizeof(assetHeader_t*) * ff->count);

	// register fastfile
	fastfiles[ffCount] = ff;

	// load every asset info
	for(uint32_t i = 0; i < ff->count; i ++)
	{
		assetType_t type = ASSET_MAX;
		assetData_t data = NULL;

		// read type
		FileSystem_Read(fp, sizeof(assetType_t), 1, &type);

		// read asset name
		uint32_t len = 0;
		FileSystem_Read(fp, sizeof(uint32_t), 1, &len);

		char* name = String_Create(len);
		
		// load image name
		FileSystem_Read(fp, sizeof(char), len, name);
		FileSystem_Read(fp, sizeof(uint32_t), 1, &len);
		
		char* path = String_Create(len);
		
		// load asset path
		FileSystem_Read(fp, sizeof(char), len, path);
		
		// call asset loader
		if(type < ASSET_MAX)
		{
			Com_Printf(Com_LogLevel_Info, "Fastfile_Load: loading asset \"%s\"\n", name);
			data = assetLoaders[type].loader(fp, name, path);
		}

		// alloc asset data
		ff->assets[i] = Assets_Register(name, path, type, data);
	}
	
	Com_Printf(Com_LogLevel_Info, "Fastfile_Load: fastfile \"%s\" loaded\n", ff->name);
	Com_Printf(Com_LogLevel_Debug, "Fastfile_Load: loaded %d assets\n", ff->count);

	// now that we've loaded it, increase count
	ffCount ++;
	
	// close file
	FileSystem_Close(fp);

	// return fastfile data
	return ff;
}

fastfile_t* FastFile_Get(const char* fastfile)
{
	for(uint32_t i = 0; i < ffCount; i ++)
	{
		int length = abs((int32_t)(strlen(fastfile) - strlen(fastfiles[i]->name)));

		if(strncmp(fastfiles[i]->name, fastfile, length) == 0)
		{			
			return fastfiles[i];
		}
	}
	
	Com_Error("FastFile_Get: cannot find fastfile \"%s\"\n", fastfile);
	
	return NULL;
}

void FastFile_Free(const char* fastfile)
{
	Com_Printf(Com_LogLevel_Debug, "FastFile_Free: trying to unload fastfile \"%s\"\n", fastfile);

	uint32_t index = 0;

	for(; index < ffCount; index ++)
	{
		int length = abs((int32_t)(strlen(fastfile) - strlen(fastfiles[index]->name)));

		if(strncmp(fastfiles[index]->name, fastfile, length) == 0)
		{
			break;
		}
	}

	if(index == ffCount)
	{
		Com_Error("FastFile_Free: trying to unload fastfile \"%s\" when not loaded\n", fastfile);
	}

	for(uint32_t i = 0; i < fastfiles[index]->count; i ++)
	{
		// Assets_Unload(fastfiles[index]->assets[i]);
		Assets_Free(fastfiles[index]->assets[i]);
	}

	// free fastfile data
	Memory_Free(fastfiles[index]->assets);
	String_Destroy(fastfiles[index]->name);
	Memory_Free(fastfiles[index]);

	ffCount --;

	// make sure fastfile entry is null
	fastfiles[index] = NULL;

	// sync fastfiles
	FastFile_Sync();
}

void FastFile_Sync()
{
	Com_Printf(Com_LogLevel_Debug, "FastFile_Sync: syncing fastfiles\n");

	// shift data if needed
	for(uint32_t index = 0; index < (MAX_FASTFILES - 1); index ++)
	{
		if(fastfiles[index] == NULL)
		{
			fastfiles[index] = fastfiles[index + 1];
			fastfiles[index + 1] = NULL;
		}
	}
}

assetHeader_t* Assets_Register(char* name, char* path, assetType_t type, assetData_t data)
{
	Com_Printf(Com_LogLevel_Info, "Assets_Register: registering asset \"%s\"\n", name);

	if(assetsCount == MAX_ASSETS)
	{
		Com_Error("Assets_Register: assets overflow\n");
	}

	assetHeader_t* asset = (assetHeader_t*)Memory_Alloc(sizeof(assetHeader_t));
	
	asset->name = name;
	asset->path = path;
	asset->ref = 1;
	asset->type = type;
	asset->data = data;

	// add asset to the list
	assets[assetsCount] = asset;
	assetsCount ++;

	return asset;
}

assetHeader_t* Assets_Get(const char* name, assetType_t type)
{
	Com_Printf(Com_LogLevel_Debug, "Assets_Get: searching asset %s\n", name);

	for(uint32_t i = 0; i < assetsCount; i ++)
	{
		if(assets[i]->type != type) continue;

		int length1 = strlen(assets[i]->name);
		int length2 = strlen(name);

		if(length1 == length2 && strncmp(assets[i]->name, name, length1) == 0)
		{
			assets[i]->ref ++;
			
			return assets[i];
		}
	}

	Com_Printf(Com_LogLevel_Debug, "Assets_Get: cannot find asset \"%s\" (%d) in list\n", name, type);

	return NULL;
}

void Assets_Free(assetHeader_t* header)
{
	Com_Printf(Com_LogLevel_Debug, "Assets_Free: decreasing asset \"%s\" (%d) refs\n", header->name, header->type);

	// decrease ref count
	header->ref --;

	if(header->ref == 0)
	{
		Assets_Unload(header);
	}
}

void Assets_Sync()
{
	Com_Printf(Com_LogLevel_Debug, "Assets_Sync: syncing assets\n");

	// shift data if needed
	for(uint32_t index = 0; index < (MAX_ASSETS - 1); index ++)
	{
		if(assets[index] == NULL)
		{
			assets[index] = assets[index + 1];
			assets[index + 1] = NULL;
		}
	}
}

void Assets_Unload(assetHeader_t* header)
{
	if(header->ref > 0)
	{
		Com_Printf(Com_LogLevel_Warning, "WARNING: Unloading asset with %d refs\n", header->ref);
	}

	Com_Printf(Com_LogLevel_Info, "Assets_Unload: unloading asset \"%s\" (%d)\n", header->name, header->type);

	// get asset index
	uint32_t index = 0;

	for(index = 0; index < assetsCount; index ++)
	{
		// check asset name
		if(assets[index]->type != header->type) continue;

		int length1 = strlen(assets[index]->name);
		int length2 = strlen(header->name);

		if(length1 == length2 && strncmp(assets[index]->name, header->name, length1) == 0)
		{
			break;
		}
	}

	if(index == assetsCount)
	{
		Com_Error("Assets_Unload: trying to unload unknown asset \"%s\" (%d)\n", header->name, header->type);
	}

	// call asset type free-er function
	if(header->type < ASSET_MAX)
	{
		assetUnloaders[header->type].unloader(header);
	}

	// free asset data
	String_Destroy(header->name);
	String_Destroy(header->path);
	Memory_Free(header);

	// make sure asset in list stays as null
	assets[index] = NULL;

	// sync assets
	Assets_Sync();
}

// assets loaders
assetData_t MapAsset_Load(file_t* fp, const char*name, const char* path)
{
	map_t* map = (map_t*)Memory_Alloc(sizeof(map_t));
	
	// prepare image struct
	map->image = (image_t*)Memory_Alloc(sizeof(image_t));
	
	Com_Memset(map->image, 0, sizeof(image_t));
	
	// load image size from data
	FileSystem_Read(fp, sizeof(coord_t), 1, &map->image->size);

	map->image->frames = 0;
	
	Com_Memcpy(&map->size, &map->image->size, sizeof(coord_t)); // important to get the map size

	// load spawns data
	FileSystem_Read(fp, sizeof(uint8_t), 1, &map->spawnCount);

	if(map->spawnCount > 0)
	{
		map->spawns = (coord_t*)Memory_Alloc(sizeof(coord_t) * map->spawnCount);
	
		FileSystem_Read(fp, sizeof(coord_t), map->spawnCount, map->spawns);
	}
	else
	{
		map->spawns = NULL;
	}

	return map;
}

void CharacterAsset_LoadFrames(frames_t* &frames, const char* basepath, const char* name, image_t* info, file_t* chr)
{
   // initialize frames information
   frames = (frames_t*)Memory_Alloc(sizeof(frames_t));
   Com_Memset(frames, 0, sizeof(frames_t));
   
   // basic frame information
   FileSystem_Read(chr, sizeof(uint8_t), 1, &frames->count);
   FileSystem_Read(chr, sizeof(uint8_t), 1, &frames->speed);
   
   if(frames->count == 0)
   {
	   Com_Printf(Com_LogLevel_Debug, "Skipping loading frames for %s, no frames available\n", name);
	   return;
   }

   // alloc space for frames info
   frames->frames = (assetHeader_t**)Memory_Alloc(sizeof(assetHeader_t*) * frames->count);
   Com_Memset(frames->frames, 0, sizeof(assetHeader_t*) * frames->count);
   
   // load frames info
   for(uint8_t frame = 0; frame < frames->count; frame ++)
   {
	   // get filename length
	   uint32_t length = 0;
	   FileSystem_Read(chr, sizeof(uint32_t), 1, &length);

	   // tmp buffer to hold filename
	   char* filename = String_Create(length);

	   // read filename
	   FileSystem_Read(chr, sizeof(char), length, filename);

	   // alloc memory for asset path
	   length = strlen(basepath) + strlen(filename);

	   char* assetPath = String_Create(length);

	   // build asset path
	   strcpy(assetPath, basepath);
	   strcat(assetPath, filename);

	   // alloc memory for asset name
	   length = strlen(name) + 1 + strlen(filename);

	   char* assetName = String_Create(length);
	   Com_Memset(assetName, 0, length);

	   // build asset name
	   strcpy(assetName, name);
	   strcat(assetName, "_");
	   strcat(assetName, filename);

	   // search the asset first, if it exists, increase references
	   assetHeader_t* asset = Assets_Get(assetName, ASSET_IMAGE);

	   // asset already loaded
	   if(asset != NULL)
	   {
		   // reference was already increased in Assets_Get

		   // free useless data
		   String_Destroy(assetName);
		   String_Destroy(assetPath);
		   String_Destroy(filename);

		   // finally add the asset to the frame
		   frames->frames[frame] = asset;

		   // skip asset registration as it already exists
		   continue;
	   }

		// generate image asset data
		image_t* header = (image_t*)Memory_Alloc(sizeof(image_t));

		// copy image data
		Com_Memcpy(header, info, sizeof(image_t));

		// register asset
		asset = Assets_Register(assetName, assetPath, ASSET_IMAGE, header);

		// register the asset data in the fastfile (a fastfile is being loaded right now)
		fastfiles[ffCount]->count ++; // increase assets count (this way we do not use places for other assets that are pending load)
		fastfiles[ffCount]->assets = (assetHeader_t**)Memory_Realloc(fastfiles[ffCount]->assets, sizeof(assetHeader_t*) * fastfiles[ffCount]->count);
		fastfiles[ffCount]->assets[fastfiles[ffCount]->count - 1] = asset; // finally add the asset to list

		// finally add the asset to the frame
		frames->frames[frame] = asset;

		// we do not need this anymore, free it
		String_Destroy(filename);
   }
}

assetData_t CharacterAsset_Load(file_t* fp, const char* name, const char* path)
{
   // load every info needed from the fastfile first
   fighter_t* character = (fighter_t*)Memory_Alloc(sizeof(fighter_t));
   Com_Memset(character, 0, sizeof(fighter_t));

   character->image = (image_t*)Memory_Alloc(sizeof(image_t));
   Com_Memset(character->image, 0, sizeof(image_t));
   
   // read image info
   FileSystem_Read(fp, sizeof(coord_t), 1, &character->image->size);
   character->image->frames = 0; // this is not used by animated sprites

   // open character file
   file_t* chr = FileSystem_Open(path, FileSystemMode_Read);

   if(chr == NULL) Com_Error("CharacterAsset_Load: cannot open character file %s\n", path);

   char* basepath = NULL;

   // build base path
   {
	   uint32_t index = strlen(path);

	   while(index > 0)
	   {
		   if(path[index - 1] == '/') break;
		   index --;
	   }

	   if(index == 0) // default basepath to /
	   {
		   basepath = String_Clone("/");
		   //basepath = (char*)Memory_Alloc(sizeof(char) * 2);
		   //basepath[0] = '/';
		   //basepath[1] = 0;
	   }
	   else // copy over parts of the path
	   {
		   basepath = String_Create(index);
		   Com_Memcpy(basepath, path, index);
		   basepath[index] = 0;
	   }
   }

   // this will load a load of assets
   CharacterAsset_LoadFrames(character->frames.stand, basepath, name, character->image, chr);
   CharacterAsset_LoadFrames(character->frames.walk, basepath, name, character->image, chr);
   CharacterAsset_LoadFrames(character->frames.kick, basepath, name, character->image, chr);
   CharacterAsset_LoadFrames(character->frames.lowkick, basepath, name, character->image, chr);
   CharacterAsset_LoadFrames(character->frames.punch, basepath, name, character->image, chr);
   CharacterAsset_LoadFrames(character->frames.lowpunch, basepath, name, character->image, chr);
   CharacterAsset_LoadFrames(character->frames.crouch, basepath, name, character->image, chr);
   CharacterAsset_LoadFrames(character->frames.jump, basepath, name, character->image, chr);

   FileSystem_Close(chr);

   // this is of no more use
   String_Destroy(basepath);

	// read character name info
	uint32_t length = 0;
	FileSystem_Read(fp, sizeof(uint32_t), 1, &length);

	character->name = String_Create(length);
	FileSystem_Read(fp, sizeof(char), length, character->name);
	
	Com_Printf(Com_LogLevel_Info, "CharacterAsset_Load: loaded %s character\n", character->name);
	
	return character;
}

assetData_t IconAsset_Load(file_t* fp, const char*name, const char* path)
{
	return NULL;
}

assetData_t GfxAsset_Load(file_t* fp, const char*name, const char* path)
{
	return NULL;
}

assetData_t SoundAsset_Load(file_t* fp, const char*name, const char* path)
{
	return NULL;
}

assetData_t MenuAsset_Load(file_t* fp, const char*name, const char* path)
{
	return NULL;
}

assetData_t ImageAsset_Load(file_t* fp, const char*name, const char* path)
{
	// alloc image_t
	image_t* image = (image_t*)Memory_Alloc(sizeof(image_t));
	
	// load image struct
	FileSystem_Read(fp, sizeof(image_t), 1, image);

	return image;
}

assetData_t CollisionAsset_Load(file_t* fp, const char*name, const char* path)
{
   coord_t* size = (coord_t*)Memory_Alloc(sizeof(coord_t));
   
   FileSystem_Read(fp, sizeof(coord_t), 1, size);
   
   Cmap_Load(path, size->x, size->y);
   
   return size;
}
   
// assets unloaders
void MapAsset_Unload(assetHeader_t* asset)
{
	map_t* map = (map_t*)asset->data;

	// free map data
	Memory_Free(map->spawns);
	Memory_Free(map->image);
	Memory_Free(asset->data);
}

void CharacterAsset_UnloadFrames(frames_t* &frames)
{
	if(!frames) return;

	// free entries, data is free'd by Fastfile_Free
	Memory_Free(frames->frames);
	Memory_Free(frames);

	// this isn't really needed
	frames = NULL;
}

void CharacterAsset_Unload(assetHeader_t* asset)
{
	fighter_t* character = (fighter_t*)asset->data;
	
	// free every frame info entry
	CharacterAsset_UnloadFrames(character->frames.stand);
	CharacterAsset_UnloadFrames(character->frames.walk);
	CharacterAsset_UnloadFrames(character->frames.kick);
	CharacterAsset_UnloadFrames(character->frames.lowkick);
	CharacterAsset_UnloadFrames(character->frames.punch);
	CharacterAsset_UnloadFrames(character->frames.lowpunch);
	CharacterAsset_UnloadFrames(character->frames.crouch);
	
	// free structure
	Memory_Free(character->image);
	Memory_Free(character->name);
	Memory_Free(character);
}

void IconAsset_Unload(assetHeader_t* asset)
{

}

void GfxAsset_Unload(assetHeader_t* asset)
{

}

void SoundAsset_Unload(assetHeader_t* asset)
{

}

void MenuAsset_Unload(assetHeader_t* asset)
{

}

void ImageAsset_Unload(assetHeader_t* asset)
{
	// free image data
	Memory_Free(asset->data);
}

void CollisionAsset_Unload(assetHeader_t* asset)
{
   // free image data
   Memory_Free(asset->data);
}   
