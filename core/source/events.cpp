#include "common.h"
#include "events.h"
#include "battle.h"
#include "input.h"
#include "network.h"
#include "server.h"
#include "client.h"

/* RIP OFF QUAKE 3 ENGINE */
#define	MAX_QUEUED_EVENTS		256
#define	MASK_QUEUED_EVENTS	( MAX_QUEUED_EVENTS - 1 )

event_t eventQueue[MAX_QUEUED_EVENTS];
int eventHead = 0;
int eventTail = 0;
uint8_t sys_packetReceived[MAX_MSGLEN];

void Sys_QueueEvent(eventType_t type, void* data, uint32_t value, uint32_t value2)
{
	event_t* ev;

	ev = &eventQueue[eventHead & MASK_QUEUED_EVENTS];

	if(eventHead - eventTail >= MAX_QUEUED_EVENTS)
	{
		// overflow
		Com_Error("Sys_QueueEvent: overflow\n");

		if(data)
		{
			Memory_Free(data);
		}

		eventTail ++;
	}

	eventHead ++;

	ev->data = data;
	ev->value = value;
	ev->value2 = value2;
	ev->type = type;
}

event_t Sys_GetEvent()
{
	// return if we have data
	if(eventHead > eventTail)
	{
		eventTail ++;
		return eventQueue[(eventTail - 1) & MASK_QUEUED_EVENTS];
	}

	// non-graphic frames there
	Input_Frame();

	// return any event we have
	if(eventHead > eventTail)
	{
		eventTail ++;
		return eventQueue[(eventTail - 1) & MASK_QUEUED_EVENTS];
	}

	// last resort, return empty event
	event_t ev;

	Com_Memset(&ev, 0, sizeof(ev));

	return ev;
}

void Sys_EventLoop()
{
	event_t ev = Sys_GetEvent();

	// fire up the correct event handler
	switch(ev.type)
	{
		case EVENT_KEYPRESS:
		case EVENT_KEYPRESSRELAY:
			Input_Event(&ev);
			break;

		case EVENT_BATTLE:
		case EVENT_BATTLESYNC:
			Battle_Event(&ev);
			break;

		case EVENT_NETWORK:
		   Net_Event(&ev);
		   break;
		   
		case EVENT_SERVER:
		case EVENT_HANDSHAKE:
		case EVENT_HANDSHAKEREQ:
		case EVENT_SERVERPING:
		   Server_Event(&ev);
		   break;
		   
		case EVENT_HANDSHAKERES:
		case EVENT_HANDSHAKEFAIL:
		case EVENT_CLIENTPING:
			Client_Event(&ev);
			break;

		case EVENT_NONE:
			break;

		default:
			// unhandled event
			Com_Error("Sys_EventLoop: unhandled event\n");
			break;
	}
}
