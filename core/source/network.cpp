#include <nds.h>
#include <dswifi9.h>
#include "common.h"
#include "network.h"
#include "events.h"
#include "input.h"
#include "messages.h"
#include "server.h"

struct in_addr net_ip, net_gateway, net_mask, net_dns1, net_dns2;
adr_t net_adr;

SOCKET net_socket = 0;
uint16_t net_port = 2500;
bool net_enabled = true;

extern uint8_t dedicated;
extern bool cl_connected;

uint8_t net_buffer[2048];
char net_lastadr[22];

void Sys_InitNetwork()
{
   if(net_enabled == false)
   {
      return Com_Printf(Com_LogLevel_NetworkDebug, "Sys_InitNetwork: skipping network initialization\n");
   }

   Com_Printf(Com_LogLevel_Info, "Connecting to WFC\n");

   if(!Wifi_InitDefault(WFC_CONNECT))
   {
      return Com_Error("Sys_InitNetwork: cannot connect to wifi\n");
   }
   
   // get ip data
   net_ip = Wifi_GetIPInfo(&net_gateway, &net_mask, &net_dns1, &net_dns2);

   // convert net_ip to our adr_t struct
   Com_Memset(&net_adr, 0, sizeof(adr_t));

   // port will be updated later
   net_adr.ip = net_ip.s_addr;
}
   
SOCKET Net_UdpInit()
{
	Com_Printf(Com_LogLevel_NetworkDebug, "Net_UdpInit: creating socket\n");

	net_socket = socket(AF_INET, SOCK_DGRAM, 0);

	// set max input buffer
	int bufsize = 1024; // 1kb
	setopt(net_socket, SOL_SOCKET, SO_RCVBUF, (char*)&bufsize,  sizeof(bufsize));
   
	// set non-blocking mode
	unsigned long nonblocking = 1;
	ioctl(net_socket, FIONBIO, &nonblocking);

	// set broadcast flag
	setopt(net_socket, SOL_SOCKET, SO_BROADCAST, (char*)&nonblocking, sizeof(nonblocking));

	Com_Printf(Com_LogLevel_NetworkDebug, "Net_UdpInit: socket configured\n");

	return net_socket;
}

bool Net_UdpListen()
{
	struct sockaddr_in addr;

	Com_Memset(&addr, 0, sizeof(sockaddr_in));

	addr.sin_port = htons(net_port);
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(net_socket, (struct sockaddr*)&addr, sizeof(sockaddr_in)) == SOCKET_ERROR)
	{
		Com_Error("Net_UdpListen: cannot bind on port %d\n", net_port);
		return false;
	}

	Com_Printf(Com_LogLevel_NetworkDebug, "Net_UdpListen: listener started on port %d\n", ntohs(addr.sin_port));

	net_adr.port = ntohs(addr.sin_port);

	return true;
}

bool Net_UdpClient(adr_t adr)
{
	char ip[16];

	sprintf(ip, "%d.%d.%d.%d",
		(adr.ip & 0xFF000000) >> 24,
		(adr.ip & 0x00FF0000) >> 16,
		(adr.ip & 0x0000FF00) >> 8,
		adr.ip & 0x000000FF
	);

	struct sockaddr_in addr;

	Com_Memset(&addr, 0, sizeof(sockaddr_in));

	addr.sin_port = htons(net_port); // local port can be different from remote port
	addr.sin_family = AF_INET;
	//addr.sin_addr.s_addr = inet_addr(ip);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(net_socket, (struct sockaddr*) &addr, sizeof(sockaddr_in)) == SOCKET_ERROR)
	{
		Com_Error("Net_UdpClient: cannot bind in port %d (%d)\n", net_port, WSAGetLastError());
		return false;
	}

	Com_Printf(Com_LogLevel_NetworkDebug, "Net_UdpClient: binded on port %d\n", ntohs(addr.sin_port));

	// update our adr_t port data
	//net_adr.port = ntohs(addr.sin_port);
	net_adr.ip = adr.ip;
	net_adr.port = adr.port;

	return true;
}

adr_t Net_ResolveHostname(const char* addr)
{
	adr_t adr;

	Com_Memset(&adr, 0, sizeof(adr_t));

	struct hostent* host = gethostbyname(addr);

	if(host != NULL && host->h_length > 0)
	{
		for(int i = 0; i < host->h_length; i ++)
		{
			adr.ip = ntohl(((sockaddr_in*)&host->h_addr_list[i])->sin_addr.s_addr);

			if(adr.ip != 0) break;
		}
	}

	if(adr.ip == 0x00000000)
	{
		Com_Error("Net_ResolveHostname: cannot resolve hostname %s\n", addr);
	}

	return adr;
}

adr_t Net_ResolveAddress(const char* addr)
{
	adr_t adr;

	Com_Memset(&adr, 0, sizeof(adr_t));

	in_addr ip;
	Com_Memset(&ip, 0, sizeof(in_addr));

	adr.ip = ntohl(inet_addr(addr));

	return adr;
}

msg_t* Net_ReceiveBroadcast(SOCKET socket)
{
	struct sockaddr_in addr;
	socklen_t addrlen = sizeof(sockaddr_in);

	Com_Memset(&addr, 0, addrlen);

	int32_t received = recvfrom(socket, (char*)&net_buffer[0], 2048, 0, (sockaddr*)&addr, &addrlen);

	if(received <= 0) return NULL;

	Com_Assert(received < 4, "Net_ReceiveBroadcast: packet fragmentation\n");

	uint32_t length = 0;
	Com_Memcpy(&length, &net_buffer[0], sizeof(uint32_t));

	adr_t sock_adr;
	adr_t data_adr;

	Com_Memset(&sock_adr, 0, sizeof(adr_t));
	Com_Memset(&data_adr, 0, sizeof(adr_t));

	sock_adr.ip = ntohl(addr.sin_addr.s_addr);
	sock_adr.port = ntohs(addr.sin_port);

	msg_t* msg = (msg_t*)Memory_Alloc(sizeof(msg_t));

	MSG_Init(msg, NULL, length);
	MSG_WriteData(msg, &net_buffer[4], length);

	// read packet address
	MSG_ReadData(msg, &data_adr, sizeof(adr_t));

	// compare addresses to make sure no one is trying to cheat on this
	/*if(sock_adr.ip != data_adr.ip || sock_adr.port != data_adr.port)
	{
		Com_Printf(Com_LogLevel_NetworkDebug, "Net_ReceiveBroadcast: wrong packet header (%x, %x) expected (%x, %x), ignoring data\n", data_adr.ip, data_adr.port, sock_adr.ip, sock_adr.port);

		// free message data
		Memory_Free(msg->data);
		Memory_Free(msg);

		return NULL;
	}*/

	Com_Printf(Com_LogLevel_NetworkDebug, "Received %d packet from %s\n", msg->cursize, Net_AdrToString(&sock_adr));

	// Net_Handle needs to read the adr, so better reset the message
	MSG_Reset(msg);

	// TODO: FIXME - Dirty hack to fix network stack!
	Com_Memcpy(msg->data, &sock_adr, sizeof(adr_t));

	// finally return the message
	return msg;
}

bool Net_SendTo(SOCKET socket, msg_t* msg, adr_t* adr)
{
	Com_Printf(Com_LogLevel_NetworkDebug, "Sending %d packet to %s\n", msg->cursize, Net_AdrToString(adr));

	// network info
	struct sockaddr_in addr;
   
	addr.sin_family = AF_INET;
	addr.sin_port = htons(adr->port);
	addr.sin_addr.s_addr = htonl(adr->ip);
   
	int32_t cur = 0;
	int32_t length = msg->cursize - msg->current + sizeof(adr_t); // when Net_SendTo is called the adr_t data is already readed from msg_t
	int32_t sent = 0;

	uint8_t* packet = (uint8_t*)Memory_Alloc(length + sizeof(int32_t));

	// build final packet data
	Com_Memcpy(&packet[0], &length, sizeof(int32_t));
	Com_Memcpy(&packet[sizeof(int32_t)], &net_adr, sizeof(adr_t));
	Com_Memcpy(&packet[sizeof(int32_t) + sizeof(adr_t)], &msg->data[msg->current], length - sizeof(adr_t));

	// add length flag to length to make our life a bit easier in these two functions
	length += sizeof(int32_t);

	// make sure we send everything
	while(cur < length && sent != -1)
	{
		cur += sent = sendto(socket, (char*)&packet[cur], length - cur, 0, (sockaddr*)&addr, sizeof(sockaddr_in));
	}

	if(sent == -1)
	{
		Com_Error("Lost connection to remote host\n");
	}
	else if(cur < length)
	{
	   Com_Printf(Com_LogLevel_Warning, "Net_SendTo: buffer data left in stream\n");
	}
	   
	// free packet data
	Memory_Free(packet);

	return cur == length;
}

void Net_Handle(msg_t* msg)
{	   
   // alloc adr_t
   adr_t adr;

   // get adr_t
   MSG_ReadData(msg, &adr, sizeof(adr_t));

	// get message type
	int type = MSG_ReadByte(msg);
	
	// handle type and queue the needed event
	switch(type)
	{
	   case EVENT_KEYPRESS:
	      {
	         keys_t* keys = (keys_t*)Memory_Alloc(sizeof(keys_t));
	         
	         // clone adr
	         adr_t* memadr = (adr_t*)Memory_Alloc(sizeof(adr_t));
	         Com_Memcpy(memadr, &adr, sizeof(adr_t));
	         
	         // set adr
	         keys->from = memadr;
	         
	         // read two ints
	         keys->held = MSG_ReadInteger(msg);
	         keys->down = MSG_ReadInteger(msg);
	         
	         // queue event
	         Sys_QueueEvent(EVENT_KEYPRESS, keys, 0, 0);
	      }   
	      break;

	   case EVENT_KEYPRESSRELAY:
		   {
			   keys_t* keys = (keys_t*) Memory_Alloc(sizeof(keys_t));

			   // read key data
			   keys->held = MSG_ReadInteger(msg);
			   keys->down = MSG_ReadInteger(msg);

			   // read adr_t data
			   keys->from = (adr_t*) Memory_Alloc(sizeof(adr_t));
			   MSG_ReadData(msg, keys->from, sizeof(adr_t));

			   // queue event
			   Sys_QueueEvent(EVENT_KEYPRESSRELAY, keys, 0, 0);
		   }
		   break;
	      
      case EVENT_CLIENTPING:
		case EVENT_SERVERPING:
         {
				msg_t* buf = (msg_t*)Memory_Alloc(sizeof(msg_t));
	
				MSG_Init(buf, NULL, msg->cursize);
				MSG_WriteData(buf, msg->data, msg->cursize);
	
				Sys_QueueEvent((type == EVENT_CLIENTPING) ? EVENT_CLIENTPING : EVENT_SERVERPING, buf, 0, 0);
         } 
         break;
         
     case EVENT_HANDSHAKE:
        {
           // create handshake struct
           serverHandshake_t* hand = (serverHandshake_t*)Memory_Alloc(sizeof(serverHandshake_t));
           
           // read handshake
           hand->handshake = MSG_ReadInteger(msg);

		   // prepare adr_t
		   hand->addr = (adr_t*)Memory_Alloc(sizeof(adr_t));

		   Com_Memcpy(hand->addr, &adr, sizeof(adr_t));
           
           Sys_QueueEvent(EVENT_HANDSHAKE, hand, 0, 0);
        }
        break;
        
	 case EVENT_HANDSHAKEREQ:
		 {
			 handshakeReq_t* hand = (handshakeReq_t*)Memory_Alloc(sizeof(handshakeReq_t));

			 // read needed data
			 hand->protocol = MSG_ReadByte(msg);
			 hand->seed = MSG_ReadInteger(msg);

			 Com_Memcpy(&hand->addr, &adr, sizeof(adr_t));

			 Sys_QueueEvent(EVENT_HANDSHAKEREQ, hand, 0, 0);
		 }
		 break;

	 case EVENT_HANDSHAKERES:
		 {
			 handshakeRes_t* hand = (handshakeRes_t*)Memory_Alloc(sizeof(handshakeRes_t));

			 // read needed data
			 hand->result = MSG_ReadByte(msg);
			 hand->player = MSG_ReadByte(msg);
			 hand->maxplayers = MSG_ReadByte(msg);

			 // queue event
			 Sys_QueueEvent(EVENT_HANDSHAKERES, hand, 0, 0);
		 }
		 break;

	 case EVENT_HANDSHAKEFAIL:
		 {
			 uint8_t reason = MSG_ReadByte(msg);

			 Sys_QueueEvent(EVENT_HANDSHAKEFAIL, NULL, reason, 0);
		 }
	     break;
	 case EVENT_BATTLESYNC:
		 {
			 msg_t* buf = (msg_t*)Memory_Alloc(sizeof(msg_t));

			 // initialize message
			 MSG_Init(buf, NULL, msg->cursize - msg->current);

			 // write only needed data
			 MSG_WriteData(buf, &msg->data[msg->current], msg->cursize - msg->current);

			 // queue event
			 Sys_QueueEvent(EVENT_BATTLESYNC, buf, EVENT_BATTLESYNC, 0);
		 }
		 break;

	 default:
		 {
			 Com_Error("Unexpected Net_Event %d\n", type);
		 }
		 break;
	}
}
   
void Net_Event(event_t* ev)
{
	switch(ev->value)
	{
		 case NETWORK_INPUT:
		    {
		       msg_t* msg = (msg_t*)ev->data;

		       Net_Handle(msg);
		       
		       // make sure we free data
		       Memory_Free(msg->data);
		       Memory_Free(msg);
			 }   
			 break;

		 case NETWORK_OUTPUT:
		    {
		       // get msg_t
		       msg_t* msg = (msg_t*)ev->data;
		       
		       // read adr
		       adr_t adr;
		       
			   // read destiny adr_t
		       MSG_ReadData(msg, &adr, sizeof(adr_t));

			    // send data
			    if(adr.ip == ADDR_SELF)
			    {
				  MSG_Reset(msg);

			      // queue the message
			      Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_INPUT, 0);
			    }
			    else if(adr.ip == ADDR_BOT)
			    {
			       Com_Printf(Com_LogLevel_NetworkDebug, "Ignoring bot packet\n");
			       
			       Memory_Free(msg->data);
			       Memory_Free(msg);
			    }   
				 else
				 {
					Net_SendTo(net_socket, msg, &adr);
					
					Memory_Free(msg->data);
					Memory_Free(msg);
				 }
		    }   
		    break;
		    
		 default:
			 Com_Error("Net_Event: unknown event type\n");
			 break;
	}
}

void Net_Frame()
{
   // skip frame if net is not enabled yet
	// if(dedicated == 0 && cl_connected == false) return;
	if(net_enabled == false || !net_socket) return;

   // did we get any packet?
   msg_t* msg = Net_ReceiveBroadcast(net_socket);
   
   // yes, enqueue packet
   if(msg)
   {
      Sys_QueueEvent(EVENT_NETWORK, msg, NETWORK_INPUT, 0);
   }
}

void Net_SocketClose(SOCKET socket)
{
   shutdown(socket, 0);
   shutdown(socket, 1);
   
   closesocket(socket);
}

// =======================================
// Conversor functions here
// =======================================
const char* Net_AdrToString(adr_t* adr)
{
	sprintf(&net_lastadr[0], "%d.%d.%d.%d:%d", 
			(adr->ip & 0xFF000000) >> 24,
			(adr->ip & 0x00FF0000) >> 16,
			(adr->ip & 0x0000FF00) >> 8,
			(adr->ip & 0x000000FF),
			adr->port
		);

	return net_lastadr;
}

adr_t Net_StringToAdr(const char* str)
{
	adr_t addr;
	int ip[4];
	int port;
	Com_Memset(&addr, 0, sizeof(adr_t));

	// get numbers
	sscanf(str, "%d.%d.%d.%d:%d", &ip[0], &ip[1], &ip[2], &ip[3], &port);

	// copy to the address structure
	addr.ip |= ip[0]			& 0x000000FF;
	addr.ip |= (ip[1] << 8)		& 0x000000FF;
	addr.ip |= (ip[2] << 16)	& 0x00FF0000;
	addr.ip |= (ip[3] << 24)	& 0xFF000000;
	addr.port = port;

	// return data
	return addr;
}