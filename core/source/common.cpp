#include "common.h"
#include "events.h"
#include "input.h"
#include "battle.h"
#include "network.h"
#include "server.h"
#include "client.h"

#include <time.h>

int com_loglevel = Com_LogLevel_None;

void Com_Memset(void* dst, uint8_t value, uint32_t len)
{
   memset(dst, value, len);
}

void Com_Memcpy(void* dst, const void* src, uint32_t len)
{
   memcpy(dst, src, len);
}

void Com_DMAMemcpy(void* dest, const void* source, uint32_t len)
{
#ifdef __NDSBUILD
	uint32_t dst = (uint32_t)dest;
	uint32_t src = (uint32_t)source;

	// check if data is aligned correctly
	if( (dst | src) & 1)
	{
		return Com_Memcpy(dest, source, len);
	}

	while(dmaBusy(3)) ; // wait until dma channel 3 is free

	// cache memory
	DC_FlushRange(source, len);

	// depending on how the data is aligned we must copy the data different
	if((src | dst | len) & 3)
	{
		dmaCopyHalfWords(3, source, dest, len);
	}
	else
	{
		dmaCopyWords(3, source, dest, len);
	}

	DC_InvalidateRange(dest, len);
#else
	memcpy(dest, source, len);
#endif /* __NDSBUILD */
}

int Com_Memcmp(void* src, void* cmp, uint32_t len)
{
   return memcmp(src, cmp, len);  
}   

int Com_MemcmpEx(void* src, uint8_t value, uint32_t len)
{
	void* data = Memory_Alloc(len);
	Com_Memset(data, value, len);

	int res = Com_Memcmp(src, data, len);

	Memory_Free(data);

	return res;
}

void Com_Print(const char* msg)
{
	iprintf("%s", msg);
	nocashMessage(msg);
}

void Com_Error(const char* msg, ...)
{
	char line[2048];
	
	va_list args;
	va_start(args, msg);
	
	vsnprintf(line, 2048, msg, args);
	
	va_end(args);
	
	Com_Printf(Com_LogLevel_Error, "ERROR: %s", line);
}

void Com_Printf(int level, const char* msg, ...)
{
	if(com_loglevel & level)
	{
		char line[2048];
	
		va_list args;
		va_start(args, msg);
	
		vsnprintf(line, 2048, msg, args);
	
		va_end(args);
	
		Com_Print(line);
	}
}

void Com_Fatal(const char* msg, ...)
{
	char line[2048];
	
	va_list args;
	va_start(args, msg);
	
	vsnprintf(line, 2048, msg, args);
	
	va_end(args);
	
	Com_Printf(Com_LogLevel_Fatal, "FATAL: %s", line);
}

void Com_Init(int level, const char* sys_cmdline)
{
	com_loglevel = level;

	consoleDemoInit();
	consoleClear();
   
	setBrightness(0, 0);
	setBrightness(1, 0);
	
	// preparate random
	srand(time(NULL));
	
	// TODO: handle command input
}
   
void Com_Attach()
{
	consoleDemoInit();
}

void Com_Frame()
{
	Input_Frame();
	// Sound_Frame();
	Net_Frame();
	
	// TODO: Implement frametime checking (it should fix this in a better way)
	// exec it seven times so the game does a full update cycle every frame
	// (should be enough for normal udp flow in a frame)
	// (it also prevents overflows)
	// (increased with server code)
	Sys_EventLoop();
	Sys_EventLoop();
	Sys_EventLoop();
	Sys_EventLoop();
	
	Server_Frame();
	Client_Frame(); // TODO: Add checks to execute this only when client is running
	
	Sys_EventLoop();
	Sys_EventLoop();
	Sys_EventLoop();

	if(cl_inbattle)
	{
		Battle_Frame();
	}
}

