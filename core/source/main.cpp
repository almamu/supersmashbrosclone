#include "common.h"
#include "battle.h"
#include "events.h"
#include "assets.h"
#include "input.h"
#include "network.h"
#include "graphics.h"
#include "messages.h"
#include "server.h"
#include "client.h"
#include "filesystem.h"

#include <time.h>

extern bool net_enabled;
extern uint16_t net_port;
extern bool cl_connected;

assetHeader_t* asset;
map_t* map_r;
extern camera_t camera;

int main(int argc, char **argv)
{
	defaultExceptionHandler();
	
	// initialize nitrofs
	NF_SetRootFolder("NITROFS", NULL);

	// init console output
	Com_Init(~Com_LogLevel_EngineDebug, "");

	// set error & message handlers for NFlib
	NF_SetErrorHandler(Com_Fatal);
	NF_SetMessageHandler(Com_Printf);

	// initialize memory manager
	Memory_Init();
	
	// initialize filesystem
	FileSystem_Init ("nitrofiles");

	// initialize render
	R_Init(128, 24, 128);
	
	// attach the console (this actually shows the console after the render has been initialized)
	Com_Attach();

	// first: disable networking
	net_enabled = true;
	
	// init fastfiles and assets system
	FastFile_Init();

	// init network system
	Sys_InitNetwork();
	
	// get server mode from command line
	int srv_mode = 0;

	if(argc > 1)
	{
		if(strcmp(argv[1], "-dedicated") == 0)
		{
			srv_mode = 2;
		}
		else if(strcmp(argv[1], "-local") == 0)
		{
			srv_mode = 1;
		}
		else if(strcmp(argv[1], "-offline") == 0)
		{
			srv_mode = 0;
		}
		else if(strcmp(argv[1], "-client") == 0)
		{
			srv_mode = 3;
		}
	}

	// init server system
	Server_Init(2, srv_mode);
	
	const char* fastfiles[] = { "characters/pikachu.ff", "stages/yoshi/yoshi.ff" };

	// start battle engine
	Battle_Setup(fastfiles, 2);

	// set assets for players
	Battle_SetPlayerAsset(0, "pikachu");
	Battle_SetPlayerAsset(1, "pikachu");

	if(srv_mode == 0)
	{
		Com_Printf(Com_LogLevel_Info, "Connecting to offline host...\n");
		
		adr_t adr;

		Com_Memset(&adr, 0, sizeof(adr_t));

		adr.ip = ADDR_SELF;
		adr.port = 0;

		Client_Init();
		Client_Connect(&adr);

		time_t started = time(NULL);

		while(difftime(time(NULL), started) < 10 && cl_connected == false)
		{
			Com_Frame();

			R_Frame();
		}

		Com_Assert(!cl_connected, "Cannot connect to local server\n");
	}
	else if(srv_mode == 3)
	{
		Com_Printf(Com_LogLevel_Info, "Connecting to localhost\n");

		adr_t adr = Net_ResolveAddress("127.0.0.1");
		//adr_t adr = Net_ResolveHostname("pc.almamu.glintdg.com");
		adr.port = 2500;

		Client_Init();
		Client_Connect(&adr);

		time_t started = time(NULL);

		while(difftime(time(NULL), started) < 10 && cl_connected == false)
		{
			Com_Frame();

			R_Frame();
		}

		Com_Assert(!cl_connected, "Cannot connect to localhost\n");
	}

	while(true)
	{
		Com_Frame();
		
		R_Frame();
	}

	return 0; 

}

