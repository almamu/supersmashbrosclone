#include "common.h"
#include "messages.h"

void MSG_Init(msg_t* buf, uint8_t* data, uint32_t length)
{
	Com_Memset(buf, 0, sizeof(msg_t));

	if(data == NULL)
	{
		buf->maxsize = sizeof(uint8_t) * length;
		buf->data = (uint8_t*)Memory_Alloc(buf->maxsize); // alloc enough space  

		Com_Memset(buf->data, 0, buf->maxsize);
	}
	else
	{   
		buf->data = data;
		buf->maxsize = buf->cursize = length;
	}   
}

void MSG_Copy(msg_t* buf, uint8_t* data, uint32_t length, msg_t* src)
{
	if(length < src->cursize)
	{
		Com_Error("MSG_Copy: can't copy into a smaller msg_t buffer\n");
	}

	Com_Memcpy(buf, src, sizeof(msg_t));

	buf->data = data;

	Com_Memcpy(buf->data, src->data, src->cursize);
}

void MSG_Clear(msg_t* buf)
{
	buf->cursize = 0;
	buf->current = 0;
}   

void MSG_Reset(msg_t* buf)
{
	buf->current = 0;
}

uint64_t MSG_ReadLong(msg_t* buf)
{
	if(buf->current + sizeof(uint64_t) > buf->cursize)
	{
		Com_Error("MSG_ReadLong: trying to read out of buffer\n");
	}

	int pos = buf->current;

	buf->current += sizeof(uint64_t);

	uint64_t value = 0;

	Com_Memcpy(&value, &buf->data[pos], sizeof(uint64_t));

	return value;
}

uint32_t MSG_ReadInteger(msg_t* buf)
{
	if(buf->current + sizeof(uint32_t) > buf->cursize)
	{
		Com_Error("MSG_ReadInteger: trying to read out of buffer\n");
	}

	int pos = buf->current;

	buf->current += sizeof(uint32_t);

	uint32_t value = 0;

	Com_Memcpy(&value, &buf->data[pos], sizeof(uint32_t));

	return value;
}

uint16_t MSG_ReadShort(msg_t* buf)
{
	if(buf->current + sizeof(uint16_t) > buf->cursize)
	{
		Com_Error("MSG_ReadShort: trying to read out of buffer\n");
	}

	int pos = buf->current;

	buf->current += sizeof(uint16_t);

	uint16_t value = 0;

	Com_Memcpy(&value, &buf->data[pos], sizeof(uint16_t));

	return value;
}

uint8_t MSG_ReadByte(msg_t* buf)
{
	if(buf->current + sizeof(uint8_t) > buf->cursize)
	{
		Com_Error("MSG_ReadByte: trying to read out of buffer\n");
	}

	int pos = buf->current;

	buf->current += sizeof(uint8_t);

	return buf->data[pos];
}

void MSG_ReadData(msg_t* buf, void* data, uint32_t length)
{
	if(buf->current + length > buf->cursize)
	{
		Com_Error("MSG_ReadData: trying to read %d + %d bytes out of buffer, length = %d\n", buf->current, length, buf->cursize);
	}

	for(uint32_t i = 0; i < length; i ++)
	{
		((uint8_t*)data)[i] = MSG_ReadByte(buf);
	}
}

char* MSG_ReadString(msg_t* buf)
{
	uint32_t length = MSG_ReadInteger(buf);

	if(buf->current + length > buf->cursize)
	{
		Com_Error("MSG_ReadString: trying to read out of buffer\n");
	}

	char* data = String_Create(length);
	MSG_ReadData(buf, &data[0], length);

	// we must care about this pointer and free it as soon as we finish using it
	return data;
}

void MSG_WriteLong(msg_t* buf, uint64_t value)
{
	if(buf->cursize + sizeof(uint64_t) > buf->maxsize)
	{
		// realloc buffer
		buf->data = (uint8_t*)Memory_Realloc(buf->data, buf->cursize + sizeof(uint64_t));

		buf->maxsize = buf->cursize + sizeof(uint64_t);
	}

	Com_Memcpy(&buf->data[buf->cursize], &value, sizeof(uint64_t));

	// increase cursize
	buf->cursize += sizeof(uint64_t);
}

void MSG_WriteInteger(msg_t* buf, uint32_t value)
{
	if(buf->cursize + sizeof(uint32_t) > buf->maxsize)
	{
		// realloc buffer
		buf->data = (uint8_t*)Memory_Realloc(buf->data, buf->cursize + sizeof(uint32_t));

		buf->maxsize = buf->cursize + sizeof(uint32_t);
	}

	Com_Memcpy(&buf->data[buf->cursize], &value, sizeof(uint32_t));

	// increase cursize
	buf->cursize += sizeof(uint32_t);
}

void MSG_WriteShort(msg_t* buf, uint16_t value)
{
	if(buf->cursize + sizeof(uint16_t) > buf->maxsize)
	{
		// realloc buffer
		buf->data = (uint8_t*)Memory_Realloc(buf->data, buf->cursize + sizeof(uint16_t));

		buf->maxsize = buf->cursize + sizeof(uint16_t);
	}

	Com_Memcpy(&buf->data[buf->cursize], &value, sizeof(uint16_t));

	// increase cursize
	buf->cursize += sizeof(uint16_t);
}

void MSG_WriteByte(msg_t* buf, uint8_t value)
{
	if(buf->cursize + sizeof(uint8_t) > buf->maxsize)
	{
		// realloc buffer
		buf->data = (uint8_t*)Memory_Realloc(buf->data, buf->cursize + sizeof(uint8_t));

		buf->maxsize = buf->cursize + sizeof(uint8_t);
	}

	buf->data[buf->cursize] = value;

	// increase cursize
	buf->cursize += sizeof(uint8_t);
}

void MSG_WriteData(msg_t* buf, const void* data, uint32_t length)
{
	if(buf->cursize + length > buf->maxsize)
	{
		buf->data = (uint8_t*)Memory_Realloc(buf->data, buf->cursize + length);

		buf->maxsize = buf->cursize + length;
	}

	Com_Memcpy(&buf->data[buf->cursize], data, length);

	buf->cursize += length;
}

void MSG_WriteString(msg_t* buf, const char* data)
{
	// get string length
	uint32_t length = strlen(data);

	// write length to the buffer
	MSG_WriteInteger(buf, length);

	// write string to buffer
	MSG_WriteData(buf, &data[0], length);
}   
