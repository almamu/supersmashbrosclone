#ifndef __COMMON_H__
#define __COMMON_H__

// define version data for handshake stuff
#define VERSION		1
#define BUILD		1
#define PROTOCOL	((VERSION << 4) | BUILD)

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <nf_lib.h>
#include <nds.h>

#ifdef __NDSBUILD
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <nds/arm9/console.h>
#endif /* !__NDSBUILD */

#ifndef _WIN32
#define max(x,y) (x<y)?y:x
#endif /* _WIN32 */

enum Com_LogLevel
{
	Com_LogLevel_None = 0,
	Com_LogLevel_Info = 1,
	Com_LogLevel_EngineDebug = 2,
	Com_LogLevel_NetworkDebug = 4,
	Com_LogLevel_Debug = 8,
	Com_LogLevel_Trace = 16,
	Com_LogLevel_Warning = 32,
	Com_LogLevel_Error = 64,
	Com_LogLevel_Fatal = 128,
	Com_LogLevel_Any = 0xFFFFFFFF,
};

// custom assertion macro
#define Com_Assert(COND, MSG, ...)	\
	if (COND)							\
	{									\
		Com_Error(MSG, ##__VA_ARGS__);	\
		while(true)						\
		{								\
			scanKeys();					\
			swiWaitForVBlank();			\
		}								\
	}

#include "memory.h"
#include "strings.h"
#include "filesystem.h"

struct coord_t
{
	int32_t x;
	int32_t y;
};

struct padding_t
{
	int32_t top;
	int32_t left;
	int32_t right;
	int32_t bottom;
};

typedef coord_t speed_t;

void Com_Memset(void* dst, uint8_t value, uint32_t len);
void Com_Memcpy(void* dst, const void* src, uint32_t len);
void Com_DMAMemcpy(void* dst, const void* src, uint32_t len);
int  Com_Memcmp(void* src, void* cmp, uint32_t len);
int  Com_MemcmpEx(void* src, uint8_t value, uint32_t len);
void Com_Init(int level, const char* sys_cmdline);
void Com_Attach();
void Com_Print(const char* msg);
void Com_Error(const char* msg, ...);
void Com_Fatal(const char* msg, ...);
void Com_Printf(int level, const char* msg, ...);
void Com_Frame();
extern int main(int argc, char** argv);

#endif /* __COMMON_H__ */
