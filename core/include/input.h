/**
* @file include/input.h
*
* @brief Keypad input functions
*/

/**
* @defgroup input Input system
*
* Contains all the keypad input functions and event handlers
* @{
*/
#ifndef __INPUT_H__
#define __INPUT_H__

#include "common.h"
#include "events.h"
#include "network.h"

/**
* @def KEY_MASK
* @brief Bitmask to get meaningful key statuses
*/
#define KEY_MASK (~(KEY_TOUCH | KEY_LID))

/**
* @struct keys_t
* @brief Input data message
*
* @var from
* Address the input comes from
*
* @var held
* Held status
*
* @var down
* Down status
*/
struct keys_t
{
	adr_t* from;
	uint32_t held;
	uint32_t down;
};

/**
* @fn void Input_Event(event_t* ev);
* @brief Input event handler
*/
void Input_Event(event_t* ev);

/**
* @fn void Input_Frame();
* @brief Per-frame input checks, queues input events when needed
*/
void Input_Frame();

#endif /* __INPUT_H__ */
/** @} */