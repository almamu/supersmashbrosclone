/**
* @file include/server.h
*
* @brief Server functions
*/

/**
* @defgroup server Server system
*
* Contains all the server functions that allow the game to act as
* local, lan or network dedicated server
*
* @{
*/
#ifndef __SERVER_H__
#define __SERVER_H__

#include "common.h"
#include "network.h"
#include "input.h"
#include "events.h"

/**
* @struct keyState_t
* @brief This structure stores the keypad state
*
* @var held
* Indicates which buttons are held for more than one frame
*
* @var down
* Indicates which buttons are down for one frame
*/
struct keyState_t
{
   uint32_t held;
   uint32_t down;
};

/**
* @enum clientState_t
* @brief This enum indicates the connection state for one client
*
* @var STATE_WAITING
* Waiting for connection
*
* @var STATE_HANDSHAKE
* The client is receiving/sending the main handshake
*
* @var STATE_READY
* The client is connected correctly
*/
enum clientState_t
{
   STATE_WAITING,
   STATE_HANDSHAKE,
   STATE_READY,
};

/**
* @enum dedicated_e
* @brief This enum indicates the mode the server is running on
*
* @var LOCAL_DEDICATED
* No connection should be done, we are playing alone
*
* @var LAN_DEDICATED
* The server should listen only on lan
*
* @var NET_DEDICATED
* The server should listen on every address to accept external connections
*/
enum dedicated_e
{
	LOCAL_DEDICATED = 0,
	LAN_DEDICATED = 1,
	NET_DEDICATED = 2,
};

/**
* @struct serverHandshake_t
* @brief Server handshake data
*
* @var addr
* Address data of user
*
* @var handshake
* Handshake data
*/
struct serverHandshake_t
{
   adr_t* addr;
   uint32_t handshake;
};

/**
* @struct handshakeRes_t
* @brief Handshake request packet
*
* @var addr
* Address data of user
*
* @var protocol
* Protocol version to be used
*
* @var seed
* Seed used for next random generations
*/
struct handshakeReq_t
{
	adr_t addr;
	uint8_t protocol;
	uint32_t seed;
};

/**
* @struct handshakeRes_t
* @brief Handshake result packet
*
* @var result
* Indicates if handshake was correct or not
*
* @var player
* Tells the game which player number we are
*
* @var maxplayers
* Tells the game how many players we need to alloc memory for
*/
struct handshakeRes_t
{
	uint8_t result;
	uint8_t player;
	uint8_t maxplayers;
};

/**
* @struct client_t
* @brief Client information
*
* @var addr
* Client address
*
* @var keys
* Keys status
*
* @var state
* Connection status
*
* @var handshake
* Handshake data
*
* @var network
* Network data (like last time a packet was sent)
*/
struct client_t
{
   // client addr
   adr_t* addr;
   
   // key state
   keyState_t keys;
   
   // client state
   clientState_t state;
   
   // handshake data
   uint32_t handshake;

   // network status data
   struct network_t
   {
	   time_t last_time;
   }network;
};

/**
* @fn void Server_Init(uint8_t maxplayers, uint8_t mode);
* @brief Initializes the server data
*
* @param maxplayers Number of max players the server will allow to connect at once
* @param mode Mode the server should listen
*/
void Server_Init(uint8_t maxplayers, uint8_t mode);

/**
* @fn void Server_Frame()
* @brief Does basic per-frame checks like connection status
*/
void Server_Frame();

/**
* @fn void Server_KeyInput(keys_t* keys);
* @brief Fires a key event depending on which mode the server is running on
*/
void Server_KeyInput(keys_t* keys);

/**
* @fn void Server_Event(event_t* ev)
* @brief Event handler for server events
*/
void Server_Event(event_t* ev);

/**
* @fn void Server_FullSync();
* @brief Forces the full synchronization of every client connected to the server
*/
void Server_FullSync();

#endif /* __SERVER_H__ */

/**
* @}
*/