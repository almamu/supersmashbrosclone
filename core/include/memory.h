#ifndef __MEMORY_H__
#define __MEMORY_H__

#include "common.h"

#define MEMORY_VERSION "MemoryManager r1; placebo entry"

struct memoryChunk_t
{
	memoryChunk_t* next;
	memoryChunk_t* previous;
	void* data;
	uint32_t length;
};

void Memory_Init();

#ifdef MEM_DEBUG
void* Memory_Alloc(uint32_t length);
void* Memory_Realloc(void* data, uint32_t length);
int Memory_Info(void* data);
void Memory_Free(void* data);
#else
#define Memory_Alloc	malloc
#define Memory_Realloc	realloc
#define Memory_Free		free
#define Memory_Info(x)	sizeof(x)
#endif

void* Memory_Clone(void* data, uint32_t length);

#endif /* __MEMORY_H__ */
