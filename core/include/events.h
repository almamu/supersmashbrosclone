#ifndef __EVENTS_H__
#define __EVENTS_H__

#include "common.h"
#include "messages.h"

enum eventType_t
{
	EVENT_NONE = 0,
	EVENT_KEYPRESS = 1,
	EVENT_KEYPRESSRELAY = 2,
	EVENT_BATTLE = 3,
	EVENT_BATTLESYNC = 4,
	EVENT_NETWORK = 5,
	EVENT_CLIENTPING = 6,
	EVENT_SERVERPING = 7,
	EVENT_HANDSHAKE = 8,
	EVENT_HANDSHAKERES = 9,
	EVENT_HANDSHAKEREQ = 10,
	EVENT_HANDSHAKEFAIL = 11,
	EVENT_SERVER = 12,
};

struct event_t
{
	eventType_t type;
	void* data;
	uint32_t value;
	uint32_t value2;
};


void Sys_QueueEvent(eventType_t type, void* data, uint32_t value, uint32_t value2);
event_t Sys_GetEvent();
void Sys_EventLoop();

#endif /* __EVENTS_H__ */
