#ifndef __ASSETS_H__
#define __ASSETS_H__

#include "common.h"
#include "messages.h"

#define MAX_FASTFILES	256
#define MAX_ASSETS		1024

typedef void* assetData_t;

enum assetType_t
{
	ASSET_MAP = 0, // game maps
	ASSET_CHARACTER = 1, // characters
	ASSET_ICON = 2, // game icons
	ASSET_GFX = 3, // attacks fx
	ASSET_SOUND = 4, // game sound
	ASSET_MENU = 5, // game menu
	ASSET_IMAGE = 6, // general image
	ASSET_COLMAP = 7,
	ASSET_MAX,
	ASSET_DUMMY = 0xFFFFFFFF // dummy value to make the type 4 bytes long
};

struct assetHeader_t
{
	char* name;
	char* path;
	uint32_t ref;
	assetType_t type;
	assetData_t data;
};

struct assetLoader_t
{
	assetType_t type;
	assetData_t (*loader)(file_t* fp, const char* path, const char* name);
};

struct assetUnloader_t
{
	assetType_t type;
	void (*unloader)(assetHeader_t*);
};

struct image_t
{
	coord_t size;
	uint8_t frames;
};

struct map_t
{
	coord_t size;
	uint8_t spawnCount;
	coord_t* spawns;
	image_t* image;
};

struct frames_t
{
	assetHeader_t** frames;
	uint8_t speed;
	uint8_t count;
};

struct fighter_t
{
   char* name;
   image_t* image;
   
   struct
   {
	   frames_t* stand;
	   frames_t* walk;
	   frames_t* kick;
	   frames_t* lowkick;
	   frames_t* punch;
	   frames_t* lowpunch;
	   frames_t* crouch;
	   frames_t* jump;
	}frames;
};
   
struct fastfile_t
{
	char* name;
	uint32_t count;
	assetHeader_t** assets;
};

void FastFile_Init();
fastfile_t* FastFile_Load(const char* file);
fastfile_t* FastFile_Get(const char* fastfile);
void FastFile_Free(const char* fastfile);
void FastFile_Sync();

assetHeader_t* Assets_Register(char* name, char* path, assetType_t type, assetData_t data);
assetHeader_t* Assets_Get(const char* name, assetType_t type);
void Assets_Free(assetHeader_t* header);
void Assets_Sync();
void Assets_Unload(assetHeader_t* header);

// assets loaders
assetData_t MapAsset_Load(file_t* fp, const char*name, const char* path);
assetData_t CharacterAsset_Load(file_t* fp, const char*name, const char* path);
assetData_t IconAsset_Load(file_t* fp, const char*name, const char* path);
assetData_t GfxAsset_Load(file_t* fp, const char*name, const char* path);
assetData_t SoundAsset_Load(file_t* fp, const char*name, const char* path);
assetData_t MenuAsset_Load(file_t* fp, const char*name, const char* path);
assetData_t ImageAsset_Load(file_t* fp, const char*name, const char* path);
assetData_t CollisionAsset_Load(file_t* fp, const char*name, const char* path);

// assets unloaders
void MapAsset_Unload(assetHeader_t* asset);
void CharacterAsset_Unload(assetHeader_t* asset);
void IconAsset_Unload(assetHeader_t* asset);
void GfxAsset_Unload(assetHeader_t* asset);
void SoundAsset_Unload(assetHeader_t* asset);
void MenuAsset_Unload(assetHeader_t* asset);
void ImageAsset_Unload(assetHeader_t* asset);
void CollisionAsset_Unload(assetHeader_t* asset);

#endif /* __ASSETS_H__ */
