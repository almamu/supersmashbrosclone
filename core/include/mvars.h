/**
* @file include/mvars.h
*
* @todo Add register functions for every mvar type and hide Mvar_Register generic function
* @brief Memory vars functions
*/

/**
* @defgroup mvars Memory vars system
*
* Functions to change values of variables used elsewhere
* namely mvars (MemoryVars)
* @{
*/
#ifndef __MVARS_H__
#define __MVARS_H__

#include "common.h"

/**
* @enum mvarType_t
*
* @var MVAR_TYPE_NONE
* No type/null type
*
* @var MVAR_TYPE_STRING
* String type
*
* @var MVAR_TYPE_BYTE
* uint8_t type
*
* @var MVAR_TYPE_SHORT
* uint16_t type
*
* @var MVAR_TYPE_INT
* uint32_t type
*
* @var MVAR_TYPE_LONG
* uint64_t type
*
* @var MVAR_TYPE_FLOAT
* float type
*
* @var MVAR_TYPE_DOUBLE
* double type
*
* @var MVAR_TYPE_MAX
* MVAR types limit
*
* @var MVAR_TYPE_DUMMY
* Dummy value, used to make the enum sizes match between compilers
*/
enum mvarType_t
{
   MVAR_TYPE_NONE = 0,
   MVAR_TYPE_STRING = 1,
   MVAR_TYPE_BYTE = 2,
   MVAR_TYPE_SHORT = 3,
   MVAR_TYPE_INT = 4,
   MVAR_TYPE_LONG = 5,
   MVAR_TYPE_FLOAT = 6,
   MVAR_TYPE_DOUBLE = 7,
   MVAR_TYPE_MAX,
   MVAR_TYPE_DUMMY = 0xFFFFFFFF,
};

/**
* @struct mvar_t
* @brief MemoryVar data
*
* @var name
* MemoryVar console name
*
* @var type
* @var pointer
* Data pointer
*/
struct mvar_t
{
   const char* name;
   mvarType_t type;
   void* pointer;
};

/**
* @struct mvarListEntry_t
* @brief Mvar linked list entry
*
* @var value
* @var next
* @var prev
*/
struct mvarListEntry_t
{
   mvar_t value;
   mvarListEntry_t* next;
   mvarListEntry_t* prev;
};

/**
* @fn void Mvars_Init();
* @brief Prepares linked list, initializes mvars data and loads default values
*/
void Mvars_Init();

/**
* @fn mvar_t* Mvars_Register(const char* name, mvarType_t type, void* pointer);
* @brief Registers a mvar of any type
*
* @param name Mvar console name
* @param type
* @param pointer Pointer to mvar data
*/
mvar_t* Mvars_Register(const char* name, mvarType_t type, void* pointer);

/**
* @fn mvar_t* Mvars_RegisterString(const char* name, const char* value);
* @brief Registers a mvar of string type
*
* @param name Mvar console name
* @param value Default mvar value
*
* @return Registered mvar
*/
mvar_t* Mvars_RegisterString(const char* name, const char* value);

/**
* @fn void Mvars_SetMvarValue(mvar_t* var, void* pointer);
* @brief Sets a mvar value pointer, usage is discouraged as there are some Mvars_Set functions
*
* @param var mvar_t pointer to mvar to update
* @param pointer Data pointer
*/
void Mvars_SetMvarValue(mvar_t* var, void* pointer);

/**
* @fn void Mvars_SetString(const char* name, const char* value);
* @brief Updates a mvar string value
*
* @param name
* @param value
*/
void Mvars_SetString(const char* name, const char* value);

/**
* @fn void Mvars_SetInteger(const char* name, uint32_t value);
* @brief Updates a mvar integer value
*
* @param name
* @param value
*/
void Mvars_SetInteger(const char* name, uint32_t value);

/**
* @fn void Mvars_SetShort(const char* name, uint16_t value);
* @brief Updates a mvar short value
*
* @param name
* @param value
*/
void Mvars_SetShort(const char* name, uint16_t value);

/**
* @fn void Mvars_SetByte(const char* name, uint8_t value);
* @brief Updates a mvar byte value
*
* @param name
* @param value
*/
void Mvars_SetByte(const char* name, uint8_t value);

/**
* @fn void Mvars_Unregister(const char* name);
* @brief Unregisters a mvar and frees its data (pointer included)
*
* @param name
*/
void Mvars_Unregister(const char* name);

#endif /* __MVARS_H__ */

/** @} */