/**
* @file include/messages.h
*
* @brief Messaging system function used in networking core
*/

/**
* @defgroup messages Messaging system
*
* Functions to read and build packets for networking core
* @{
*/
#ifndef __MESSAGES_H__
#define __MESSAGES_H__

/**
* @def MAX_MSGLEN
* @brief Maximum size of a message in bytes
*/
#define MAX_MSGLEN 16384

/**
* @struct msg_t
* @brief Message information structure, contains info like length, max size and current buffer
*
* @var data
* Pointer to message data
*
* @var maxsize
* Current buffer size
*
* @var cursize
* Current position to append to
*
* @var current
* Current read position
*/
struct msg_t
{
	uint8_t	*data;
	uint32_t	maxsize;
	uint32_t	cursize;
	uint32_t	current;
};

/**
* @fn void MSG_Init(msg_t* buf, uint8_t* data, uint32_t length);
* @brief Initializes a msg_t structure
*
* @param buf
* Message to initialize
*
* @param data
* Data the message should point to (null for new buffer)
*
* @param length
* Buffer size if data is not null, new buffer size otherwise
*/
void MSG_Init(msg_t* buf, uint8_t* data, uint32_t length);

/**
* @fn void MSG_Copy(msg_t* buf, uint8_t* data, uint32_t length, msg_t* src);
* @brief Copies one message buffer into another overwriting (this is used to initialize a new buffer rather than normal copy function)
*
* @param buf
* Message to copy data to
*
* @param data
* Buffer to copy into
*
* @param length
* Data parameter length
*
* @param src
* Message to copy data from
*/
void MSG_Copy(msg_t* buf, uint8_t* data, uint32_t length, msg_t* src);

/**
* @fn void MSG_Clear(msg_t* buf);
* @brief Clears message data (more likely resets the write and read pointers to zero without actually changing anything)
*
* @param buf
* Message to clear
*/
void MSG_Clear(msg_t* buf);

/**
* @fn void MSG_Reset(msg_t* buf);
* @brief Like clear but only modifies the read pointer
*
* @param buf
* Message to reset
*/
void MSG_Reset(msg_t* buf);

// read data
/**
* @fn uint64_t MSG_ReadLong(msg_t* buf);
* @brief Reads an uint64_t from buffer in current position
*
* @param buf
* Message to read from
*/
uint64_t MSG_ReadLong(msg_t* buf);

/**
* @fn uint32_t MSG_ReadInteger(msg_t* buf);
* @brief Reads an uint32_t from buffer in current position
*
* @param buf
* Message to read from
*/
uint32_t MSG_ReadInteger(msg_t* buf);

/**
* @fn uint16_t MSG_ReadShort(msg_t* buf);
* @brief Reads an uint16_t from buffer in current position
*
* @param buf
* Message to read from
*/
uint16_t MSG_ReadShort(msg_t* buf);

/**
* @fn uint8_t MSG_ReadByte(msg_t* buf);
* @brief Reads an uint8_t from buffer in current position
*
* @param buf
* Message to read from
*/
uint8_t MSG_ReadByte(msg_t* buf);

/**
* @fn void MSG_ReadData(msg_t* buf, void* data, uint32_t length);
* @brief Reads length bytes from buf and stores them in data
*
* @param buf
* Message to read data from
*
* @param data
* Buffer to output data to
*
* @param length
* Number of bytes to read
*/
void MSG_ReadData(msg_t* buf, void* data, uint32_t length);

// extended readers

/**
* @fn char* MSG_ReadString(msg_t* buf);
* @brief Reads a size-prefixed string
*
* @param buf
* Message to read data from
*/
char* MSG_ReadString(msg_t* buf);

// write data
/**
* @fn void MSG_WriteLong(msg_t* buf, uint64_t value);
* @brief Writes an uint64_t into message
*
* @param buf
* Message to write data to
*
* @param value
* Value to write
*/
void MSG_WriteLong(msg_t* buf, uint64_t value);

/**
* @fn void MSG_WriteInteger(msg_t* buf, uint32_t value);
* @brief Writes an uint32_t into message
*
* @param buf
* Message to write data to
*
* @param value
* Value to write
*/
void MSG_WriteInteger(msg_t* buf, uint32_t value);

/**
* @fn void MSG_WriteShort(msg_t* buf, uint16_t value);
* @brief Writes an uint16_t into message
*
* @param buf
* Message to write data to
*
* @param value
* Value to write
*/
void MSG_WriteShort(msg_t* buf, uint16_t value);

/**
* @fn void MSG_WriteByte(msg_t* buf, uint8_t value);
* @brief Writes an uint8_t into message
*
* @param buf
* Message to write data to
*
* @param value
* Value to write
*/
void MSG_WriteByte(msg_t* buf, uint8_t value);

/**
* @fn void MSG_WriteData(msg_t* buf, const void* data, uint32_t length);
* @brief Writes an array of bytes into message
*
* @param buf
* Message to write data to
*
* @param data
* Buffer to write data from
*
* @param length
* Buffer length
*/
void MSG_WriteData(msg_t* buf, const void* data, uint32_t length);

// extended writters
/**
* @fn void MSG_WriteString(msg_t* buf, const char* data);
* @brief Writes a size-prefixed string
*
* @param buf
* Message to write string to
*
* @param data
* String to write
*/
void MSG_WriteString(msg_t* buf, const char* data);

#endif /* __MESSAGES_H__ */

/** @} */
