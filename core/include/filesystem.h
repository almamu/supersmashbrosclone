#ifndef __FILESYSTEM_H__
#define __FILESYSTEM_H__

#include "common.h"

/*
* @brief FileSystem file entry information
*/
struct file_t
{
	FILE* pointer;
	char* filename;
	int size;
};

/*
* @brief FileSystem Seek modes
*/
enum FileSystemSeek
{
	FileSystemSeek_Begin = SEEK_SET,
	FileSystemSeek_Cur = SEEK_CUR,
	FileSystemSeek_End = SEEK_END,
};

/*
* @brief FileSystem opening modes
*/
enum FileSystemMode
{
	FileSystemMode_Write = 0,
	FileSystemMode_Read = 1,
	FileSystemMode_Append = 2,
};

/**
 * @brief Initializes the filesystem
 *
 * @param const char* The base path to load files off from
 *
 * @retval void
 */
void FileSystem_Init(const char* basepath);

/*
* @brief Opens a file
*
* @param const char* Name of the file to open
* @param FileSystemMode Mode the file will be openned
*
* @retval file_t* File information pointer
*/
file_t* FileSystem_Open(const char* filename, FileSystemMode mode);

/*
* @brief Reads some bytes from the file
*
* @param file_t* File information pointer to read data from
* @param uint32_t Size of a single memory block
* @param uint32_t Number of times to read size bytes
* @param void* Buffer with output buffer data
*
* @retval int Bytes readed
*/
int FileSystem_Read(file_t* file, uint32_t size, uint32_t count, void* buffer);

/*
* @brief Writes some bytes to the file
*
* @param file_t* File information pointer to write data to
* @param uint32_t Number of bytes to write from buffer
* @param uint32_t Number of times to read size bytes
* @param void* Data to write to file
*
* @retval int Bytes written
*/
int FileSystem_Write(file_t* file, uint32_t size, uint32_t count, void* buffer);

/*
* @brief Seeks to a specific part of the file
*
* @param file_t* File information pointer to seek in
* @param int Number of bytes to move arount
* @param FileSystemSeek Seek mode
*/
void FileSystem_Seek(file_t* file, int space, FileSystemSeek mode);

/*
* @brief Closes an already open file
*
* @param file_t* File to close
*/
void FileSystem_Close(file_t* file);

#endif /* __FILESYSTEM_H__ */