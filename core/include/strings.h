#ifndef __STRINGS_H__
#define __STRINGS_H__

/*
* @brief Allocates space for a string
*
* @param int Length of the string to reserve
*
* @retval char* Created string
*/
char* String_Create(int length);

/*
* @brief Clonates string
*
* @param const char* String to clone
*
* @retval char* Cloned string
*/
char* String_Clone(const char* string);

/*
* @brief Frees the allocated string
*
* @param char* String to free
*/
void String_Destroy(char* string);

#endif /* __STRING_H__ */