#ifndef __BATTLE_H__
#define __BATTLE_H__

#include "common.h"
#include "network.h"
#include "events.h"
#include "assets.h"
#include <graphics.h>
#include "server.h"

#define G_GRAVITY 48
#define G_SPEED 250
#define G_SHIELD 250
#define G_SPAWN 640
#define G_ATKWEIGHT (64 << 8)
#define G_ATKHEIGHT (32 << 8)
#define G_FRAMESHIFT 2
#define G_FRAMELIMIT (1 << 6)

enum jumpState_t
{
	JUMP_GROUND = 0,
	JUMP_BASIC = 1,
	JUMP_EXTENDED = 2,
	JUMP_FINAL = 3,
};

enum actionState_t
{
	ACTION_READY = 0,
	ACTION_STAND = 1,
	ACTION_WALK = 2,
	ACTION_CROUCH = 3,
	ACTION_FALL = 4,
	ACTION_JUMP = 5,
	ACTION_SHIELD = 6,
	ACTION_KICK = 7,
	ACTION_LOWKICK = 8,
	ACTION_PUNCH = 9,
	ACTION_LOWPUNCH = 10,
	ACTION_SMASH = 11,
};

enum direction_t
{
	DIRECTION_LEFT = 0,
	DIRECTION_DOWN = 1,
	DIRECTION_RIGHT = 2,
	DIRECTION_UP = 3,
	DIRECTION_NONE = 4,
};

struct character_t
{
	// sprite coordinates
	coord_t absolute_position;
	coord_t screen_position;
	speed_t player_speed;

	direction_t direction;

	// frame status information
	frames_t* frameInfo;
	uint8_t frame;
	uint8_t lastFrame;
	uint8_t frameStatus;
	uint32_t frameCycles;
	material_t** frames;
	sprite_t* sprite;

	// character status
	uint16_t damage;
	bool running;
	jumpState_t jump;
	uint16_t drop;
	uint8_t status;
	actionState_t action;
	actionState_t lastAction;
	int16_t shield;

	// gameplay information
	uint8_t kills;
	uint8_t falls;
	uint8_t lifes;
	uint16_t spawn; // spawn time when the user is inmortal
	bool inmortal;
	bool canplay;
	int16_t hit_by;
	
	// character info
	fighter_t* fighter;

	// network info
	adr_t addr;
};

struct cpuTarget_t
{
	coord_t position;
	int32_t leftlimit;
	int32_t rightlimit;
};

extern bool cl_inbattle;
extern uint8_t sv_maxplayers;

void Battle_Setup(const char** fastfile, uint32_t count);
void Battle_Frame();
void Battle_Event(event_t* ev);
void Battle_Sync(msg_t* msg);
void Battle_SetPlayerAsset(uint8_t player, const char* spr);
msg_t* Battle_SyncData(client_t* sv_clients);

#endif /* __BATTLE_H__ */
