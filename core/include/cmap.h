#ifndef __CMAP_H__
#define __CMAP_H__

struct cmap_t
{
   uint8_t* tiles;
   uint8_t* map;
   
   uint32_t tiles_size;
   uint32_t map_size;
   
   uint16_t width;
	uint16_t height;
};
   
void Cmap_Load(const char* path, uint16_t width, uint16_t height);
uint8_t Cmap_GetPoint(uint32_t x, uint32_t y);
void Cmap_Free();

#endif /* __CMAP_H__ */

