#ifndef __CLIENT_H__
#define __CLIENT_H__

#include "events.h"
#include "input.h"
#include "battle.h"

enum clientEvent_e
{
	
};

struct basicClient_t
{
	adr_t* adr;

	struct keys_t
	{
		uint32_t held;
		uint32_t down;
	}keys;

	bool connected;
};

void Client_Init();
void Client_Connect(adr_t* adr);
void Client_Frame();
void Client_Event(event_t* ev);
void Client_KeyInput(keys_t* keys);
void Client_Sync(character_t* characters);

#endif /* __CLIENT_H__ */

