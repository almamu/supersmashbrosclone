/**
* @file include/network.h
*
* @brief Networking functions
*/

/**
* @defgroup networking Networking system
*
* Contains all the networking functions that take care of connection
* data sending and receiving and event queueing for those i/o
*
* @{
*/
#ifndef __NETWORK_H__
#define __NETWORK_H__

#include <dswifi9.h>
#include <nf_wifi.h>
#include "common.h"
#include "events.h"

/**
 * @def net_errno errno
 */
#define net_errno errno

/**
* @def ADDR_BROADCAST 0xFFFFFFFF
* @brief Broadcast IP
*/
#define ADDR_BROADCAST	0xFFFFFFFF

/**
* @def ADDR_SELF 0x00000001
* @brief IP to point to our client
*/
#define ADDR_SELF		0x00000001

/**
* @def ADD_BOT 0x00000000
* @brief IP used to identify bots from human players
*/
#define ADDR_BOT		0x00000000

/**
* @def ADDR_LOOPBACK 0x7f000001
* @brief localhost
*/
#define ADDR_LOOPBACK	0x7f000001

/**
* @struct adr_t
* @brief Address data
*
* @var ip
* @var port
*/
struct adr_t
{
	uint32_t ip; // ADDR_SELF => local play
	uint16_t port; // when ip == ADDR_SELF indicates the player number
};

/**
* @enum networkEvent_e
* @brief Type of network event
*
* @var NETWORK_INPUT
* @var NETWORK_OUTPUT
*/
enum networkEvent_e
{
   NETWORK_INPUT = 0,
   NETWORK_OUTPUT = 1,
};

/**
* @fn void Sys_InitNetwork();
* @brief Initializes networking system
*/
void Sys_InitNetwork();

/**
* @fn SOCKET Net_UdpInit();
* @brief Opens an udp socket and uses it as default socket
*/
SOCKET Net_UdpInit();

/**
* @fn bool Net_UdpListen();
* @brief Sets default socket mode to listen server
*/
bool Net_UdpListen();

/**
* @fn bool Net_UdpClient(adr_t adr);
* @biref Sets default socket mode to client
*/
bool Net_UdpClient(adr_t adr);

/**
* @fn adr_t Net_ResolveHostname(const char* addr);
* @brief Resolves an hostname to a adr_t struct
*
* @param addr Hostname
*/
adr_t Net_ResolveHostname(const char* addr);

/**
* @fn adr_t Net_ResolveAddress(const char* addr);
* @brief Resolves an IP to a adr_t struct
* @param addr IP
*/
adr_t Net_ResolveAddress(const char* addr);

/**
* @fn void Net_Event(event_t* ev);
* @brief Network event handler
*/
void Net_Event(event_t* ev);

/**
* @fn void Net_Handle(msg_t* msg);
* @brief Handles incoming packets from network
*/
void Net_Handle(msg_t* msg);

/**
* @fn msg_t* Net_ReceiveBroadcast(SOCKET sock);
* @brief Checks for incoming packets from broadcast ip
*
* @param sock Socket to check packets from
*
* @retval Message data if any, null otherwise
*/
msg_t* Net_ReceiveBroadcast(SOCKET sock);

/**
* @fn bool Net_SendTo(SOCKET sock, uint8_t* data, adr_t* adr);
* @brief Sends data to adr over the sock specified
*
* @param sock Socket to send data over
* @param data Data to be sent
* @param adr Address to send data to
*/
bool Net_SendTo(SOCKET sock, uint8_t* data, adr_t* adr);

/**
* @fn void Net_Frame();
* @brief Per-frame networking checks, looks for packets on default socket
*/
void Net_Frame();

/**
* @fn void Net_SocketClose(SOCKET socket);
* @brief Closes the specified socket
*/
void Net_SocketClose(SOCKET socket);

/**
* @fn const char* Net_AdrToString(adr_t* adr);
* @brief Converts an adr_t struct back to its ip representation
*/
const char* Net_AdrToString(adr_t* adr);

/**
* @fn adr_t Net_StringToAdr(const char* str);
* @brief Converts an IP string to its adr_t representation
*/
adr_t Net_StringToAdr(const char* str);

#endif /* __NETWORK_H__ */

/** @} */