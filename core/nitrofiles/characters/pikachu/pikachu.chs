# Pikachu's animation file
# animation nodes: stand, walk, punch, lowpunch, kick, lowkick

# node structure:
# animation node, frames, speed, frame1, frame2, frame3...
animation stand, 2, 1, stand1, stand2
animation walk, 0, 0
animation run, 0, 0
animation punch, 0, 0
animation lowpunch, 0, 0
animation kick, 0, 0
animation lowkick, 0, 0
animation crouch, 2, 1, crouch1, crouch2
animation jump, 1, 1, jump
