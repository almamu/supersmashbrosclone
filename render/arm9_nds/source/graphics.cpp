#include <common.h>
// #include "graphics_common.h"
#include "graphics.h"

// this is not really used from here, but is needed in some background functions
camera_t camera;

material_t** materials = NULL;
graphic_t** graphics = NULL;
sprite_t** sprites = NULL;
background_t** backgrounds = NULL;

uint16_t r_maxmaterials = 0;
uint16_t r_maxgraphics = 0;
uint16_t r_maxsprites = 0;
uint16_t r_maxbackgrounds = 8;

// this is a private function that should not be called directly unless you know what you're doing
void R_UpdateBackground(background_t* background);
void R_SetBackgroundVisibility(uint8_t screen, uint8_t layer, bool show); // internal use
void R_SetBackgroundOffsets(uint8_t screen, uint8_t layer, uint32_t x, uint32_t y);

void R_BackgroundDump(background_t* bg)
{
	Com_Printf(
		"Background (%d)\n"
		"Asset:\t%s\n"
		"Layer:\t%d\n"
		"Pal:\t0x%x\n"
		"Screen:\t%d\n"
		"Ram:\n"
			"\tMap:\n"
			"\t\tBase:\t%d\n"
			"\t\tLength:\t%d\n"
			"\t\tData:\t0x%x\n"
			"\tTileset:\n"
			"\t\tBase:\t%d\n"
			"\t\tLength:\t%d\n"
			"\t\tData:\t0x%x\n"
		"Vram:\n"
			"\tMap:\n"
			"\t\tBase:\t%d\n"
			"\t\tLength:\t%d\n"
			"\t\tData:\t0x%x\n"
			"\tTileset:\n"
			"\t\tBase:\t%d\n"
			"\t\tLength:\t%d\n"
			"\t\tData:\t0x%x\n"
			"\t\tTileLength:\t0x%x\n",
		sizeof(background_t),
		bg->asset->name, bg->layer, bg->pal.data, bg->screen,
		bg->ram.map.base, bg->ram.map.length, bg->ram.map.data,
		bg->ram.tileset.base, bg->ram.tileset.length, bg->ram.tileset.data,
		bg->vram.map.base, bg->vram.map.length, bg->vram.map.data,
		bg->vram.tileset.base, bg->vram.tileset.length, bg->vram.tileset.data,
		bg->vram.tiles.tiles);
}

void R_Init(uint8_t maxMaterials, uint8_t maxSprites, uint8_t maxGraphics)
{
	Com_Assert(maxMaterials == 0, "R_Init: invalid materials count\n");
	Com_Assert(maxSprites == 0, "R_Init: invalid sprites count\n");
	Com_Assert(maxGraphics == 0, "R_Init: invalid graphics count\n");

	Com_Printf("R_Init: Initializing render: %d materials, %d sprites, %d vram slots\n", maxMaterials, maxSprites, maxGraphics);

	// initialize arrays
	backgrounds = (background_t**)Memory_Alloc(sizeof(background_t*) * r_maxbackgrounds); // we only need 4 backgrounds
	materials = (material_t**)Memory_Alloc(sizeof(material_t*) * maxMaterials);
	graphics = (graphic_t**)Memory_Alloc(sizeof(graphic_t*) * maxGraphics);
	sprites = (sprite_t**)Memory_Alloc(sizeof(sprite_t*) * maxSprites);

	Com_Memset(backgrounds, 0, sizeof(background_t*) * r_maxbackgrounds);
	Com_Memset(materials, 0, sizeof(material_t*) * maxMaterials);
	Com_Memset(graphics, 0, sizeof(graphic_t*) * maxGraphics);
	Com_Memset(sprites, 0, sizeof(sprite_t*) * maxSprites);

	// initialize 2D and 3D engine
	NF_Set2D(0, 0);
	NF_Set2D(1, 0);

	// initialize tiled bgs
	NF_InitTiledBgBuffers();
	NF_InitTiledBgSys(0);
	NF_InitTiledBgSys(1);

	// initialize sprites
	NF_InitSpriteBuffers();
	NF_InitSpriteSys(0);
	NF_InitSpriteSys(1);

	// keep max materials and sprites count
	r_maxmaterials = maxMaterials;
	r_maxgraphics = maxGraphics;
	r_maxsprites = maxSprites;

	// initialize backgrounds stuff
	REG_DISPCNT		|= DISPLAY_BG_EXT_PALETTE;	// extended palettes
	REG_DISPCNT_SUB	|= DISPLAY_BG_EXT_PALETTE;	// extended palettes

	// setup A bank for main screen
	vramSetBankA(VRAM_A_MAIN_BG);
	Com_Memset((void*)R_VRAM_MAIN_ADDRESS, 0, 131072); // 128kb

	// setup E bank for extended palettes in main screen
	vramSetBankE(VRAM_E_LCD);
	Com_Memset((void*)R_VRAM_MAIN_LCD_ADDRESS, 0, 32768); // 32kb

	// setup C bank for sub screen
	vramSetBankC(VRAM_C_SUB_BG);
	Com_Memset((void*)R_VRAM_SUB_ADDRESS, 0, 131072); // 128kb

	// setup H bank for extended palettes in sub screen
	vramSetBankH(VRAM_H_LCD);
	Com_Memset((void*)R_VRAM_SUB_LCD_ADDRESS, 0, 32768); // 32kb

	// change background visibility
	for(uint8_t screen = 0; screen < 2; screen ++)
	{
		for(uint8_t layer = 0; layer < 4; layer ++)
		{
			R_SetBackgroundVisibility(screen, layer, false);
		}
	}
}

material_t* R_LoadMaterial(assetHeader_t* asset, image_t* image, bool transparent)
{
	uint16_t first = r_maxmaterials;

	for(int cur = 0; cur < r_maxmaterials; cur ++)
	{
		if(materials[cur] != NULL)
		{
			if(materials[cur]->asset == asset)
			{
				// increase material references
				materials[cur]->refs ++;

				// return material
				return materials[cur];
			}
		}
		else
		{
			if(first == r_maxmaterials) first = cur;
		}
	}

	Com_Assert(first == r_maxmaterials, "R_LoadMaterial: not material slots left\n");
   
	Com_Printf("R_LoadMaterial: loading \"%s\": %dx%d\n", asset->path, image->size.x, image->size.y);

	// load gfx and pal
	NF_LoadSpriteGfx(asset->path, first, image->size.x, image->size.y);
	NF_LoadSpritePal(asset->path, first);

	// initialize material data
	material_t* material = (material_t*)Memory_Alloc(sizeof(material_t));
	Com_Memset(material, 0, sizeof(material_t));

	material->palID = material->ramID = first;
	material->refs = 1;
	material->asset = asset;
	material->image = (image_t*)asset->data;
	material->graphic = NULL;

	// increase assets references
	material->asset->ref ++;

	// return the material pointer
	return materials[first] = material;
}

sprite_t* R_DisplayMaterial(material_t* material, uint32_t x, uint32_t y)
{
	Com_Assert(material == NULL, "R_DisplayMaterial: material is NULL\n");
	graphic_t* graphic = NULL;

	if(material->graphic != NULL)
	{
		graphic = material->graphic;
	}
	else
	{
		// add it to the list
		uint16_t gfxID = r_maxgraphics;

		for(int cur = 0; cur < r_maxgraphics; cur ++)
		{
			if(graphics[cur] == NULL)
			{
				gfxID = cur; break;
			}
		}

		Com_Assert(gfxID == r_maxgraphics, "R_DisplayMaterial: not free gfx slots\n");

		// update list
		graphics[gfxID] = material->graphic = graphic = (graphic_t*)Memory_Alloc(sizeof(graphic_t));
		Com_Memset(graphic, 0, sizeof(graphic_t));

		// initialize data
		graphic->refs = 0;
		graphic->material = material;
		graphic->vramID = graphic->palID = gfxID;

		// increase material refs
		material->refs ++;

		// load data to vram
		NF_Vram3dSpriteGfx(graphic->material->ramID, graphic->vramID, true);
		NF_Vram3dSpritePal(graphic->material->palID, graphic->palID);
	}

	// one reference
	graphic->refs ++;

	// allocate sprite info
	sprite_t* sprite = (sprite_t*)Memory_Alloc(sizeof(sprite_t));
	Com_Memset(sprite, 0, sizeof(sprite_t));

	// initialize data
	sprite->flipped = false;
	sprite->graphic = graphic;
	sprite->priority = 0;

	// find a free sprite slot
	uint16_t sprID = r_maxsprites;

	for(int cur = 0; cur < r_maxsprites; cur ++)
	{
		if(sprites[cur] == NULL)
		{
			sprID = cur;
			break;
		}
	}

	Com_Assert(sprID == r_maxsprites, "R_DisplayMaterial: not free sprite slots\n");

	sprite->spriteID = sprID;

	// create sprite
	NF_Create3dSprite(sprite->spriteID, sprite->graphic->vramID, sprite->graphic->palID, x, y);
	NF_Show3dSprite(sprite->spriteID, true);

	// sort them
	NF_Sort3dSprites();

	return sprites[sprID] = sprite;
}

void R_UpdateMaterial(sprite_t* sprite, uint32_t x, uint32_t y)
{
	Com_Assert(sprite == NULL, "R_UpdateMaterial: sprite is NULL\n");

	// move the sprite to the new position
	NF_Move3dSprite(sprite->spriteID, x, y);
}

void R_UpdateMaterialPriority(sprite_t* sprite, uint32_t priority)
{
	Com_Assert(sprite == NULL, "R_UpdateMaterialPriority: sprite is NULL\n");

	NF_Set3dSpritePriority(sprite->spriteID, priority);
}

void R_FlipMaterial(sprite_t* sprite, bool flip)
{
	Com_Assert(sprite == NULL, "R_FlipMaterial: sprite is NULL\n");

	sprite->flipped = flip;

	if(sprite->flipped)
		NF_Scale3dSprite(sprite->spriteID, -sprite->graphic->material->image->size.x, sprite->graphic->material->image->size.y);
	else
		NF_Scale3dSprite(sprite->spriteID, sprite->graphic->material->image->size.x, sprite->graphic->material->image->size.y);
}

bool R_MaterialFlipped(sprite_t* sprite)
{
	Com_Assert(sprite == NULL, "R_MaterialFlipped: sprite is NULL\n");

	return sprite->flipped;
}

void R_HideMaterial(sprite_t* sprite)
{
	Com_Assert(sprite == NULL, "R_HideMaterial: sprite is NULL\n");

	NF_Delete3dSprite(sprite->spriteID);

	sprites[sprite->spriteID] = NULL;

	// decrease reference of graphic info (vram)
	sprite->graphic->refs --;

	// check vram graphic info (we could need to unload data not in use in vram)
	if(sprite->graphic->refs == 0)
	{
		// remove from the list
		graphics[sprite->graphic->vramID] = NULL;

		// decrease references and/or unload material
		R_UnloadMaterial(sprite->graphic->material);

		// unload data from vram
		NF_Free3dSpriteGfx(sprite->graphic->vramID);

		// free gfx data
		Memory_Free(sprite->graphic);
	}

	// free sprite memory
	Memory_Free(sprite);
}

void R_UnloadMaterial(material_t* material)
{
	Com_Assert(material == NULL, "R_UnloadMaterial: material is NULL\n");

	material->refs --;

	if(material->refs > 0) return;

	// remove the material from the list
	materials[material->ramID] = NULL;

	// decrease assets references
	material->asset->ref --;

	// unload sprite gfx
	NF_UnloadSpriteGfx(material->ramID);

	// free material info
	Memory_Free(material);
}
   
void R_Frame()
{
	// update every background
	for(uint8_t slot = 0; slot < r_maxbackgrounds; slot ++)
	{
		// update the background if loaded
		if(backgrounds[slot] != NULL) R_UpdateBackground(backgrounds[slot]);
	}

	// vertical sync
	swiWaitForVBlank();
}


// Background functions
u32 R_GetTileMapAddress(uint16_t width, uint16_t height, int16_t tile_x, int16_t tile_y) // RIP OFF NFlib!
{
	uint16_t size_x = R_TILES(width);
	uint16_t size_y = R_TILES(height);

	// make sure we stay in range
	if(tile_x < 0) tile_x += size_x;
	if(tile_y < 0) tile_y += size_y;
	if(tile_x > size_x) tile_x %= size_x;
	if(tile_y > size_y) tile_y %= size_y;

	uint32_t address = tile_y * size_x + tile_x;

	Com_Assert(
		address > (width * height), "Buffer overflow: %dx%d@0x%x of %dx%d@0x%x\n",
		tile_x,
		tile_y,
		address,
		size_x,
		size_y,
		(width * height)
	);

	// current tile
	return address;
}

uint16_t R_GetTileOfMap(background_t* background, int16_t tile_x, int16_t tile_y)
{
	// get tile address in map
	uint32_t address = R_GetTileMapAddress(background->image->size.x, background->image->size.y, tile_x, tile_y);

	// current tile
	return background->ram.map.data[address];
}

uint32_t R_GetTileVMapAddress(uint16_t width, uint16_t height, int16_t tile_x, int16_t tile_y)
{
	// Obtiene las medidas en tiles del mapa
	u16 size_x = R_TILES(width);	// Tiles de ancho
	u16 size_y = R_TILES(height);	// Tiles de alto
	u16 block_x = (tile_x >> 5);			// n� de pantalla (X)
	u16 block_y = (tile_y >> 5);			// n� de pantalla (Y)
	u32 row_size = ((size_x >> 5) << 11);	// Tama�o en bytes de una fila de pantallas

	// Protegete de los fuera de rango
	if(tile_x < 0) tile_x += size_x;
	if(tile_y < 0) tile_y += size_y;
	if(tile_x > size_x) tile_x %= size_x;
	if(tile_y > size_y) tile_y %= size_y;

	// Calcula la posicion de memoria que deberas leer
	// El mapa esta ordenado en bloques de 32x32 tiles, en filas.
	u32 scr_y = (block_y * row_size);	// Desplazamiento en memoria, bloques de pantallas (32x32) sobre Y
	u32 scr_x = (block_x << 11);		// Desplazamiento en memoria, bloques de pantallas (32x32) sobre X
	u32 tls_y = ((tile_y - (block_y << 5)) << 5);	// Desplazamiento en memoria, tiles sobre X
	u32 tls_x = (tile_x - (block_x << 5));			// Desplazamiento en memoria, tiles sobre Y
	u32 address =  scr_y + scr_x + ((tls_y + tls_x) << 1); 

	// Devuelve el la direccion en el buffer del Tile
	return address >> 1;
}

uint16_t R_GetTileOfVramMap(background_t* background, int16_t tile_x, int16_t tile_y)
{
	// get tile address in map
	uint32_t address = R_GetTileVMapAddress(R_VRAM_BG_WIDTH, R_VRAM_BG_HEIGHT, tile_x, tile_y);

	// current tile
	return background->vram.map.data[address];
}

void R_SetTileOfMap(background_t* background, int16_t tile_x, int16_t tile_y, uint16_t tile)
{
	uint32_t address = R_GetTileMapAddress(background->image->size.x, background->image->size.y, tile_x, tile_y);

	// set bytes
	background->ram.map.data[address] = tile;

	// mark the background to be updated
	background->scrolled = true;
}

void R_SetTileOfVramMap(background_t* background, int16_t tile_x, int16_t tile_y, uint16_t tile)
{
	if(tile > background->ram.tiles.tiles)
	{
		Com_Printf("R_SetTileOfVramMap: tile id %d out of range (of %d)\n", tile, background->ram.tiles.tiles);
		return;
	}

	// basic vram destination data
	uint32_t baseaddress = ((background->screen == 1) ? R_VRAM_SUB_ADDRESS : R_VRAM_MAIN_ADDRESS);
	uint32_t addr = 0;

	// get tile address in map
	uint32_t address = R_GetTileVMapAddress(R_VRAM_BG_WIDTH, R_VRAM_BG_HEIGHT, tile_x, tile_y);

	// first take a look at what's currently in the position
	uint16_t vram_tile = R_GetTileOfVramMap(background, tile_x, tile_y);

	// get the real tile
	uint32_t ram_tile = background->vram.tiles.status[vram_tile].ramTile;

	// if the tile we are modifying is the same we do not need to update anything
	if(ram_tile == tile) return;

	if(R_IS_TILE_LOADED(ram_tile) == true)
	{
		Com_Assert(ram_tile >= background->ram.tiles.tiles, "RAM Tile (%d) out of bounds\n", ram_tile);

		if(background->vram.tiles.status[vram_tile].mapRefs > 0)
		{
			background->vram.tiles.status[vram_tile].mapRefs --;
		}
	}

	// we know the new tile we want, load it to the correct place and increase by 1 the refs
	if(R_IS_TILE_LOADED(background->ram.tiles.status[tile].vramTile) == false)
	{
		bool found = false;

		// find free place and load it there
		for(uint32_t i = 0; i < background->vram.tiles.tiles; i ++)
		{
			if(
				background->vram.tiles.status[i].mapRefs == 0 ||
				R_IS_TILE_LOADED(background->vram.tiles.status[i].ramTile) == false
			)
			{
				vram_tile = i;
				found = true;
				break;
			}
		}

		Com_Assert(found == false, "\nR_SetTileOfVramMap: cannot find free tileset info");

		// generate address
		addr = baseaddress + (background->vram.tileset.base << 14) + (vram_tile * R_VRAM_TILE_LENGTH);

		// copy tileset data over
		Com_DMAMemcpy((void*)(addr), background->ram.tileset.data[tile].info, R_VRAM_TILE_LENGTH);

		// update the vram tile position
		background->ram.tiles.status[tile].vramTile = vram_tile;
		background->vram.tiles.status[vram_tile].ramTile = tile;

		background->vram.tiles.status[vram_tile].mapRefs = 1;
	}

	// increase references now
	background->vram.tiles.status[vram_tile].mapRefs ++;

	// so now we can safely assign it
	background->vram.map.data[address] = vram_tile;

	// now we need to update the map's RAM data to make sure this fits the vram contents
	// addr = baseaddress + (background->vram.map.base << 11) + address;

	// Com_DMAMemcpy((void*)(addr), &background->vram.map.data[address], R_VRAM_MAP_TILE_LENGTH);
}

background_t* R_PrecacheBackground(assetHeader_t* asset, uint8_t screen, uint8_t layer, bool repeat)
{
	// get the array position
	uint8_t slot = (screen == 0) ? layer : (4 + layer);

	// check if there is already a background and stop the program
	Com_Assert(backgrounds[slot] != NULL, "R_PrecacheBackgroundTileset: layer %d in screen %d already in use\n", layer, screen);

	// increase asset references
	asset->ref ++;

	// initialize data structure
	background_t* background = (background_t*)Memory_Alloc(sizeof(background_t));
	Com_Memset(background, 0, sizeof(background_t));

	// store needed data
	background->asset = asset;
	background->screen = screen;
	background->layer = layer;
	background->image = ((map_t*)asset->data)->image;
	background->hidden = true;
	background->repeat = repeat;

	// vram data
	background->vram.map.base = 0;
	background->vram.map.length = R_VRAM_MAP_BLOCK_LENGTH * R_VRAM_MAP_BLOCKS;

	background->vram.tileset.base = 0;
	background->vram.tileset.length = R_VRAM_TILESET_LENGTH;

	// allocate ram for vram buffers
	background->vram.map.data = (uint16_t*)Memory_Alloc(sizeof(uint8_t) * background->vram.map.length);

	Com_Memset(background->vram.map.data, 0, sizeof(uint8_t) * background->vram.map.length);

	// scroll info
	background->scroll.x = 0;
	background->scroll.y = 0;
	background->scrolled = true;

	// make sure the background is not visible yet
	R_SetBackgroundVisibility(background->screen, background->layer, false);

	// build filenames
	char* sbifile = (char*)Memory_Alloc(sizeof(char) * strlen(asset->path) + 4 + 1);
	strcpy(sbifile, asset->path);
	strcat(sbifile, ".sbi");

	// open the img file
	file_t* fp = FileSystem_Open(sbifile, FileSystemMode_Read);

	char head[4];
	FileSystem_Read(fp, sizeof(char), 4, &head);

	// make sure the header is ok
	Com_Assert(head[0] != 'S' || head[1] != 'S' || head[2] != 'B' || head[3] != 'I', "R_PrecacheBackground: file's %s header incorrect", sbifile);

	// get palette size
	FileSystem_Read(fp, sizeof(uint16_t), 1, &background->pal.length);

	// allocate space for palette data
	background->pal.data = (uint16_t*)Memory_Alloc(R_VRAM_PALETTE_LENGTH);
	if(background->pal.length < R_VRAM_PALETTE_LENGTH) Com_Memset(background->pal.data, 0, R_VRAM_PALETTE_LENGTH);

	// read palette
	FileSystem_Read(fp, sizeof(uint8_t), background->pal.length, background->pal.data);

	// get the tileset size
	FileSystem_Read(fp, sizeof(uint32_t), 1, &background->ram.tileset.length);

	// set tile limits for vram an ram
	background->ram.tiles.tiles = background->ram.tileset.length >> 6;
	background->vram.tiles.tiles = R_VRAM_TILESET_TILES;

	// allocate space for tileset buffer
	background->ram.tileset.data = (r_tile_t*)Memory_Alloc(background->ram.tileset.length);

	// read the whole tileset to RAM
	FileSystem_Read(fp, sizeof(uint8_t), background->ram.tileset.length, background->ram.tileset.data);

	// tileset status indicator
	background->ram.tiles.status = (r_ram_tile_t*)Memory_Alloc(sizeof(r_ram_tile_t) * background->ram.tiles.tiles);
	background->vram.tiles.status = (r_vram_tile_t*)Memory_Alloc(sizeof(r_vram_tile_t) * background->vram.tiles.tiles);

	// initialize every tile data
	for(uint32_t i = 0; i < background->ram.tiles.tiles; i ++)
	{
		background->ram.tiles.status[i].vramTile = R_VRAM_TILE_NOT_LOADED;
	}

	for(uint32_t i = 0; i < background->vram.tiles.tiles; i ++)
	{
		background->vram.tiles.status[i].mapRefs = 0;
		background->vram.tiles.status[i].ramTile = R_VRAM_TILE_NOT_LOADED;
	}

	// go to the end and get the size
	FileSystem_Read(fp, sizeof(uint16_t), 1, &background->ram.map.length);

	// allocate space for map buffer
	background->ram.map.data = (uint16_t*)Memory_Alloc(background->ram.map.length);

	// read the whole map to RAM
	FileSystem_Read(fp, sizeof(uint8_t), background->ram.map.length, background->ram.map.data);

	// finally close the file
	FileSystem_Close(fp);

	// we do not need the filename anymore
	Memory_Free(sbifile);

	// NFLIB COMPATIBILITY
	{
		uint8_t counter = 0;
		uint8_t start = 255;
		
		// find free tiles blocks
		for(int i = 0; i < NF_BANKS_TILES[background->screen]; i ++)
		{
			if(NF_TILEBLOCKS[background->screen][i] == 0)
			{
				if(counter == 0) start = i;

				counter ++;

				if(counter == R_VRAM_TILESET_BLOCKS) break;
			}
			else
			{
				start = 255;
				counter = 0;
			}
		}

		Com_Assert(start == 255 || counter < R_VRAM_TILESET_BLOCKS, "R_PrecacheBackground: cannot find %d free tiles blocks\n", R_VRAM_TILESET_BLOCKS);

		background->vram.tileset.base = start;

		// mark banks as used
		for(int i = background->vram.tileset.base; i < background->vram.tileset.base + R_VRAM_TILESET_BLOCKS; i ++)
		{
			NF_TILEBLOCKS[screen][i] = 255;
		}

		counter = 0;
		start = 255;

		// find free map blocks
		for(int i = 0; i < NF_BANKS_MAPS[screen]; i ++)
		{
			if(NF_MAPBLOCKS[screen][i] == 0)
			{
				if(counter == 0) start = i;

				counter ++;

				if(counter == R_VRAM_MAP_BLOCKS) break;
			}
			else
			{
				start = 255;
				counter = 0;
			}
		}

		Com_Assert(start == 255 || counter < R_VRAM_MAP_BLOCKS, "R_PrecacheBackgorund: cannot find %d free map blocks\n", R_VRAM_MAP_BLOCKS);

		background->vram.map.base = start;

		// mark banks as used
		for(int i = background->vram.map.base; i < background->vram.map.base + R_VRAM_MAP_BLOCKS; i ++)
		{
			NF_MAPBLOCKS[screen][i] = 255;
		}
	}

	// create the background on screen
	if(background->screen == 0)
	{
		switch(background->layer)
		{
			case 0: REG_BG0CNT = BgType_Text8bpp | BG_64x32 | BG_PRIORITY_0 | BG_PALETTE_SLOT0 | BG_COLOR_256 | BG_TILE_BASE(background->vram.tileset.base) | BG_MAP_BASE(background->vram.map.base); break;
			case 1: REG_BG1CNT = BgType_Text8bpp | BG_64x32 | BG_PRIORITY_1 | BG_PALETTE_SLOT1 | BG_COLOR_256 | BG_TILE_BASE(background->vram.tileset.base) | BG_MAP_BASE(background->vram.map.base); break;
			case 2: REG_BG2CNT = BgType_Text8bpp | BG_64x32 | BG_PRIORITY_2 | BG_COLOR_256 | BG_TILE_BASE(background->vram.tileset.base) | BG_MAP_BASE(background->vram.map.base); break;
			case 3: REG_BG3CNT = BgType_Text8bpp | BG_64x32 | BG_PRIORITY_3 | BG_COLOR_256 | BG_TILE_BASE(background->vram.tileset.base) | BG_MAP_BASE(background->vram.map.base); break;
		}

		// copy palette to vram
		vramSetBankE(VRAM_E_LCD);
		NF_DmaMemCopy((void*)(R_VRAM_MAIN_LCD_ADDRESS + (layer << 13)), background->pal.data, R_VRAM_PALETTE_LENGTH);
		vramSetBankE(VRAM_E_BG_EXT_PALETTE);
	}
	else
	{
		switch(background->layer)
		{
			case 0: REG_BG0CNT_SUB = BgType_Text8bpp | BG_64x32 | BG_PRIORITY_0 | BG_PALETTE_SLOT0 | BG_COLOR_256 | BG_TILE_BASE(background->vram.tileset.base) | BG_MAP_BASE(background->vram.map.base); break;
			case 1: REG_BG1CNT_SUB = BgType_Text8bpp | BG_64x32 | BG_PRIORITY_1 | BG_PALETTE_SLOT1 | BG_COLOR_256 | BG_TILE_BASE(background->vram.tileset.base) | BG_MAP_BASE(background->vram.map.base); break;
			case 2: REG_BG2CNT_SUB = BgType_Text8bpp | BG_64x32 | BG_PRIORITY_2 | BG_COLOR_256 | BG_TILE_BASE(background->vram.tileset.base) | BG_MAP_BASE(background->vram.map.base); break;
			case 3: REG_BG3CNT_SUB = BgType_Text8bpp | BG_64x32 | BG_PRIORITY_3 | BG_COLOR_256 | BG_TILE_BASE(background->vram.tileset.base) | BG_MAP_BASE(background->vram.map.base); break;
		}

		// copy palette to vram
		vramSetBankH(VRAM_H_LCD);
		NF_DmaMemCopy((void*)(R_VRAM_SUB_LCD_ADDRESS + (layer << 13)), background->pal.data, R_VRAM_PALETTE_LENGTH);
		vramSetBankH(VRAM_H_SUB_BG_EXT_PALETTE);
	}

	// add the background to the list and return the pointer
	return backgrounds[slot] = background;
}

void R_DisplayBackground(background_t* background)
{
	if(background->hidden == true)
	{
		R_SetBackgroundVisibility(background->screen, background->layer, true);
		background->hidden = false;
	}
}

void R_HideBackground(background_t* background)
{
	if(background->hidden == false)
	{
		R_SetBackgroundVisibility(background->screen, background->layer, false);
		background->hidden = true;
	}
}

// scroll background
void R_ScrollBackground(background_t* background, int32_t x, int32_t y)
{
	// this isn't a real background scroll as we're moving it to the same position
	if(background->scroll.x == x && background->scroll.y == y) return;

	// just update scroll position and mark it for update
	background->scroll.x = x;
	background->scroll.y = y;
	background->scrolled = true;

	int32_t max_x = background->image->size.x - camera.size.x;
	int32_t max_y = background->image->size.y - camera.size.y;

	// small checks to not overflow the map
	if(background->repeat == false)
	{
		// make sure the scroll stays in place if needed
		if(background->scroll.x > max_x) background->scroll.x = max_x;
		if(background->scroll.y > max_y) background->scroll.y = max_y;
	}
	else
	{
		// make sure the scroll stays in place if needed
		if(background->scroll.x > max_x) background->scroll.x -= max_x;
		if(background->scroll.y > max_y) background->scroll.y -= max_y;
	}
}

// updates vram tiles if needed
void R_UpdateBackground(background_t* background)
{
	// we can skip the background update, it's been already updated
	if(background->scrolled == false || background->hidden == true) return;

	// mark the background as not scrolled (this will prevent re-update)
	background->scrolled = false;

	// min range
	int32_t basetile_x = R_TILES(background->scroll.x) - 1;
	int32_t basetile_y = R_TILES(background->scroll.y) - 1;
	
	// max range
	int32_t maxtile_x = R_TILES(background->scroll.x + camera.size.x) + 1;
	int32_t maxtile_y = R_TILES(background->scroll.y + camera.size.y) + 1;

	// first we need to clean the borders
	for(int32_t y = basetile_y - 1; y < maxtile_y + 1; y ++)
	{
		R_SetTileOfVramMap(background, basetile_x, y, 0);
		R_SetTileOfVramMap(background, maxtile_x, y, 0);
	}

	for(int32_t x = basetile_x - 1; x < maxtile_x + 1; x ++)
	{
		R_SetTileOfVramMap(background, x, basetile_y, 0);
		R_SetTileOfVramMap(background, x, maxtile_y, 0);
	}

	for(int32_t y = basetile_y; y < maxtile_y; y ++)
	{
		for(int32_t x = basetile_y; x < maxtile_y; x ++)
		{
			// get the tile number
			uint16_t tile = R_GetTileOfMap(background, x, y);

			// set tile
			R_SetTileOfVramMap(background, x, y, tile);
		}
	}

	// finally copy the map data from structure to vram
	uint32_t baseaddress = ((background->screen == 1) ? R_VRAM_SUB_ADDRESS : R_VRAM_MAIN_ADDRESS);
	uint32_t address = baseaddress + (background->vram.map.base << 11);
	
	uint32_t blockx = (background->scroll.x >> 8);
	uint32_t mapmovex = (blockx << 11);

	uint32_t x = background->scroll.x - (blockx << 8);

	Com_DMAMemcpy((void*)(address), background->vram.map.data /*+ mapmovex*/, R_VRAM_MAP_BLOCK_LENGTH);

	// do not forget to scroll the background
	R_SetBackgroundOffsets(background->screen, background->layer, x, background->scroll.y);
}

void R_UnloadBackground(background_t* background)
{
	// remove the background from the list
	uint8_t slot = (background->screen == 0) ? background->layer : (4 + background->layer);

	// first of all remove from the slot
	backgrounds[slot] = NULL;

	// hide bg first
	R_SetBackgroundVisibility(background->screen, background->layer, false);

	// decrease asset ref
	background->asset->ref --;

	// free map data
	Memory_Free(background->ram.map.data);

	// free vram map data
	Memory_Free(background->vram.map.data);
	
	// free palette data
	Memory_Free(background->pal.data);

	// free tileset data
	Memory_Free(background->ram.tiles.status);
	Memory_Free(background->vram.tiles.status);

	// free the struct data
	Memory_Free(background);
}

void R_SetBackgroundVisibility(uint8_t screen, uint8_t layer, bool show)
{
	if(screen == 0)
	{
		switch(layer)
		{
			case 0: if(show) REG_DISPCNT |= DISPLAY_BG0_ACTIVE; else REG_DISPCNT &= ~DISPLAY_BG0_ACTIVE; break;
			case 1: if(show) REG_DISPCNT |= DISPLAY_BG1_ACTIVE; else REG_DISPCNT &= ~DISPLAY_BG1_ACTIVE; break;
			case 2: if(show) REG_DISPCNT |= DISPLAY_BG2_ACTIVE; else REG_DISPCNT &= ~DISPLAY_BG2_ACTIVE; break;
			case 3: if(show) REG_DISPCNT |= DISPLAY_BG3_ACTIVE; else REG_DISPCNT &= ~DISPLAY_BG3_ACTIVE; break;
		}
	}
	else
	{
		switch(layer)
		{
			case 0: if(show) REG_DISPCNT_SUB |= DISPLAY_BG0_ACTIVE; else REG_DISPCNT_SUB &= ~DISPLAY_BG0_ACTIVE; break;
			case 1: if(show) REG_DISPCNT_SUB |= DISPLAY_BG1_ACTIVE; else REG_DISPCNT_SUB &= ~DISPLAY_BG1_ACTIVE; break;
			case 2: if(show) REG_DISPCNT_SUB |= DISPLAY_BG2_ACTIVE; else REG_DISPCNT_SUB &= ~DISPLAY_BG2_ACTIVE; break;
			case 3: if(show) REG_DISPCNT_SUB |= DISPLAY_BG3_ACTIVE; else REG_DISPCNT_SUB &= ~DISPLAY_BG3_ACTIVE; break;
		}
	}
}

void R_SetBackgroundOffsets(uint8_t screen, uint8_t layer, uint32_t x, uint32_t y)
{
	if(screen == 0)
	{
		switch(layer)
		{
			case 0: REG_BG0HOFS = x; REG_BG0VOFS = y; break;
			case 1: REG_BG1HOFS = x; REG_BG1VOFS = y; break;
			case 2: REG_BG2HOFS = x; REG_BG2VOFS = y; break;
			case 3: REG_BG3HOFS = x; REG_BG3VOFS = y; break;
		}
	}
	else
	{
		switch(layer)
		{
			case 0: REG_BG0HOFS_SUB = x; REG_BG0VOFS_SUB = y; break;
			case 1: REG_BG1HOFS_SUB = x; REG_BG1VOFS_SUB = y; break;
			case 2: REG_BG2HOFS_SUB = x; REG_BG2VOFS_SUB = y; break;
			case 3: REG_BG3HOFS_SUB = x; REG_BG3VOFS_SUB = y; break;
		}
	}
}
