#ifndef __NDSRENDER_H__
#define __NDSRENDER_H__

#include "graphics_common.h"

// base vram addresses
#define R_VRAM_MAIN_ADDRESS			(0x06000000)
#define R_VRAM_SUB_ADDRESS			(0x06200000)

// base vram lcd addresses
#define R_VRAM_MAIN_LCD_ADDRESS		(0x06880000)
#define R_VRAM_SUB_LCD_ADDRESS		(0x06898000)

#endif /* !__NDSRENDER_H__ */
