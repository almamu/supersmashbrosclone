#ifndef __WINRENDER_H__
#define __WINRENDER_H__

#include "graphics_common.h"

#define R_VRAM_WINTILESET_WIDTH	(256)
#define R_VRAM_TILES_PER_ROW	(R_TILES(R_VRAM_WINTILESET_WIDTH))

#endif /* !__WINRENDER_H__ */
