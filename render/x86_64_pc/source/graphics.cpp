#include <nds.h>
#include "graphics.h"

// this is not really used from here, but is needed in some background functions
camera_t camera;

material_t** materials = NULL;
graphic_t** graphics = NULL;
sprite_t** sprites = NULL;
background_t** backgrounds = NULL;

uint8_t r_maxmaterials = 0;
uint8_t r_maxgraphics = 0;
uint8_t r_maxsprites = 0;
uint8_t r_maxbackgrounds = 8;

extern SDL_Surface* r_window;
extern SDL_Surface* r_screens[2];
extern SDL_Rect r_screens_size[2];
extern SDL_Surface* r_backgrounds[2][4];
extern SDL_Rect r_backgrounds_size[2][4];
extern SDL_Surface* r_sprites[2][4];
extern SDL_Rect r_sprites_size[2][4];
extern uint32_t r_transparentColor;

// this is a private function that should not be called directly unless you know what you're doing
void R_UpdateBackground(background_t* background);
void R_SetBackgroundVisibility(uint8_t screen, uint8_t layer, bool show); // internal use
void R_SetBackgroundOffsets(uint8_t screen, uint8_t layer, uint32_t x, uint32_t y);

//
// Got from LazyFoo's SDL tutorials for fast coding
// TODO: REWRITE THESE ON A FANCY FASHION :D
//
uint16_t get_pixel32( SDL_Surface *surface, int x, int y )
{
	//convert the pixels to 32 bit
	uint16_t *pixels = (uint16_t *)surface->pixels;

	// get the requested pixel
	return pixels[ ( y * surface->w ) + x ];
}

void put_pixel32( SDL_Surface *surface, int x, int y, uint16_t pixel )
{
	// convert the pixels to 32 bit
	uint16_t *pixels = (uint16_t *)surface->pixels;

	// set the pixel
	pixels[ ( y * surface->w ) + x ] = pixel;
}

SDL_Surface *flip_surface( SDL_Surface *surface, int flags )
{
	// pointer to the soon to be flipped surface
	SDL_Surface *flipped = NULL;
	
	// if the image is color keyed
	if( surface->flags & SDL_SRCCOLORKEY )
	{
		flipped = SDL_CreateRGBSurface( SDL_HWSURFACE, surface->w, surface->h, surface->format->BitsPerPixel, surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, 0 );
	}
	// otherwise
	else
	{
		flipped = SDL_CreateRGBSurface( SDL_HWSURFACE, surface->w, surface->h, surface->format->BitsPerPixel, surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, surface->format->Amask );
	}

	// if the surface must be locked
	if( SDL_MUSTLOCK( surface ) )
	{
		// lock the surface
		SDL_LockSurface( surface );
	}

	// go through columns
	for( int x = 0, rx = flipped->w - 1; x < flipped->w; x++, rx-- )
	{
		// go through rows
		for( int y = 0, ry = flipped->h - 1; y < flipped->h; y++, ry-- )
		{
			// get pixel
			uint32_t pixel = get_pixel32( surface, x, y );

			// copy pixel
			put_pixel32( flipped, rx, y, pixel );
		}
	}

	 // unlock surface
	if( SDL_MUSTLOCK( surface ) )
	{
		SDL_UnlockSurface( surface );
	}
	
	// copy color key
	if( surface->flags & SDL_SRCCOLORKEY )
	{
		SDL_SetColorKey( flipped, SDL_RLEACCEL | SDL_SRCCOLORKEY, surface->format->colorkey );
	}
	
	// return flipped surface
	return flipped;
}

void R_Init(uint8_t maxMaterials, uint8_t maxSprites, uint8_t maxGraphics)
{
	Com_Assert(maxMaterials == 0, "R_Init: invalid materials count\n");
	Com_Assert(maxSprites == 0, "R_Init: invalid sprites count\n");
	Com_Assert(maxGraphics == 0, "R_Init: invalid graphics count\n");

	Com_Printf(Com_LogLevel_Info, "R_Init: Initializing render: %d materials, %d sprites, %d vram slots\n", maxMaterials, maxSprites, maxGraphics);

	// initialize arrays
	backgrounds = (background_t**)Memory_Alloc(sizeof(background_t*) * r_maxbackgrounds); // we only need 4 backgrounds
	materials = (material_t**)Memory_Alloc(sizeof(material_t*) * maxMaterials);
	graphics = (graphic_t**)Memory_Alloc(sizeof(graphic_t*) * maxGraphics);
	sprites = (sprite_t**)Memory_Alloc(sizeof(sprite_t*) * maxSprites);

	Com_Memset(backgrounds, 0, sizeof(background_t*) * r_maxbackgrounds);
	Com_Memset(materials, 0, sizeof(material_t*) * maxMaterials);
	Com_Memset(graphics, 0, sizeof(graphic_t*) * maxGraphics);
	Com_Memset(sprites, 0, sizeof(sprite_t*) * maxSprites);

	// initialize 2D and 3D engine
	NF_Set2D(0, 0);
	NF_Set2D(1, 0);

	// initialize tiled bgs
	NF_InitTiledBgBuffers();
	NF_InitTiledBgSys(0);
	NF_InitTiledBgSys(1);

	// initialize sprites
	NF_InitSpriteBuffers();
	NF_InitSpriteSys(0);
	NF_InitSpriteSys(1);

	// keep max materials and sprites count
	r_maxmaterials = maxMaterials;
	r_maxgraphics = maxGraphics;
	r_maxsprites = maxSprites;

	// change background visibility
	for(uint8_t screen = 0; screen < 2; screen ++)
	{
		for(uint8_t layer = 0; layer < 4; layer ++)
		{
			R_SetBackgroundVisibility(screen, layer, false);
		}
	}
}

material_t* R_LoadMaterial(assetHeader_t* asset, image_t* image, bool transparent)
{
	Com_Assert(asset == NULL, "R_LoadMaterial: asset is NULL\n");
	Com_Assert(image == NULL, "R_LoadMaterial: image is NULL\n");
	
	uint8_t first = r_maxmaterials;

	for(int cur = 0; cur < r_maxmaterials; cur ++)
	{
		if(materials[cur] != NULL)
		{
			if(materials[cur]->asset == asset)
			{
				// increase material references
				materials[cur]->refs ++;

				// return material
				return materials[cur];
			}
		}
		else
		{
			if(first == r_maxmaterials) first = cur;
		}
	}

	Com_Assert(first == r_maxmaterials, "R_LoadMaterial: not material slots left\n");
   
	Com_Printf(Com_LogLevel_Info, "R_LoadMaterial: loading \"%s\": %dx%d\n", asset->path, image->size.x, image->size.y);

	// load gfx and pal
	NF_LoadSpriteGfx(asset->path, first, image->size.x, image->size.y);
	NF_LoadSpritePal(asset->path, first);

	// initialize material data
	material_t* material = (material_t*)Memory_Alloc(sizeof(material_t));
	Com_Memset(material, 0, sizeof(material_t));

	material->palID = material->ramID = first;
	material->refs = 1;
	material->asset = asset;
	material->image = (image_t*)asset->data;

	// increase assets references
	material->asset->ref ++;

	// build pal filename
	char* pal = String_Create(strlen(material->asset->path) + 4);
	strcpy(pal, material->asset->path);
	strcat(pal, ".pal");
	
	// open pal file
	file_t* pal_fp = FileSystem_Open(pal, FileSystemMode_Read);

	// allocate space for pallete
	uint16_t* pallete = (uint16_t*) Memory_Alloc(pal_fp->size);
	
	// and load it
	FileSystem_Read(pal_fp, pal_fp->size, 1, pallete);

	// the pal file can now be closed
	FileSystem_Close(pal_fp);

	// build img filename
	char* img = String_Create(strlen(material->asset->path) + 4);
	strcpy(img, material->asset->path);
	strcat(img, ".img");

	// open img file
	file_t* img_fp = FileSystem_Open(img, FileSystemMode_Read);

	// allocate space for image data
	uint8_t* imagebuffer = (uint8_t*) Memory_Alloc(img_fp->size);

	// and load it
	FileSystem_Read(img_fp, img_fp->size, 1, imagebuffer);

	// the image file can now be closed
	FileSystem_Close(img_fp);

	// reconstruct the image on a custom surface
	const SDL_PixelFormat& fmt = *(r_window->format);

	// first create the bg surface
	material->surf = SDL_CreateRGBSurface(
		SDL_HWSURFACE,
		material->image->size.x,
		material->image->size.y,
		fmt.BitsPerPixel,
		fmt.Rmask, fmt.Gmask, fmt.Bmask, fmt.Amask
	);

	uint16_t pixels[2] = {0, 0};

	uint32_t tiles_x = R_TILES(material->image->size.x);
	uint32_t tiles_y = R_TILES(material->image->size.y);

	int n = 0;

	for(uint16_t ty = 0; ty < tiles_y; ty ++)
	{
		for(uint16_t tx = 0; tx < tiles_x; tx ++)
		{
			uint16_t data[R_VRAM_TILE_LENGTH];

			for(int i = 0; i < R_VRAM_TILE_LENGTH; i += 2, n ++)
			{
				uint8_t pos = imagebuffer[n];

				data[i + 1] = pallete[(pos & 0xF0) >> 4];
				data[i] = pallete[(pos & 0x0F)];
			}

			SDL_Rect rect = {(int16_t)(tx * R_TILE_WIDTH), (int16_t)(ty * R_TILE_HEIGHT), R_TILE_WIDTH, R_TILE_HEIGHT};
			SDL_Surface* t = SDL_CreateRGBSurfaceFrom(&data, R_TILE_WIDTH, R_TILE_HEIGHT, fmt.BitsPerPixel, R_TILE_WIDTH * 2, 31, 31 << 5, 31 << 10, 0);
			SDL_BlitSurface(t, NULL, material->surf, &rect);
		}
	}

	// store graphic rect
	SDL_Rect spr_rect = {0, 0, material->image->size.x, material->image->size.y};
	material->rect = spr_rect;

	// return the material pointer
	return materials[first] = material;
}

sprite_t* R_DisplayMaterial(material_t* material, uint32_t x, uint32_t y)
{
	Com_Assert(material == NULL, "R_DisplayMaterial: material is NULL\n");
	Com_Assert(material->image == NULL, "R_DisplayMaterial: image is NULL\n");

	graphic_t* graphic = NULL;

	// add it to the list
	uint8_t gfxID = r_maxgraphics;

	for(int cur = 0; cur < r_maxgraphics; cur ++)
	{
		if(graphics[cur] == NULL)
		{
			gfxID = cur; break;
		}
	}

	Com_Assert(gfxID == r_maxgraphics,  "R_DisplayMaterial: not free gfx slots\n");

	// update list
	graphics[gfxID] = graphic = (graphic_t*)Memory_Alloc(sizeof(graphic_t));
	Com_Memset(graphic, 0, sizeof(graphic_t));

	// initialize data
	graphic->refs = 0;
	graphic->material = material;
	graphic->vramID = graphic->palID = gfxID;

	// increase material refs
	material->refs ++;

	// one reference
	graphic->refs ++;

	// allocate sprite info
	sprite_t* sprite = (sprite_t*)Memory_Alloc(sizeof(sprite_t));
	Com_Memset(sprite, 0, sizeof(sprite_t));

	// initialize data
	sprite->flipped = false;
	sprite->graphic = graphic;
	sprite->priority = 0;

	// find a free sprite slot
	uint8_t sprID = r_maxsprites;

	for(int cur = 0; cur < r_maxsprites; cur ++)
	{
		if(sprites[cur] == NULL)
		{
			sprID = cur;
			break;
		}
	}

	Com_Assert(sprID == r_maxsprites, "R_DisplayMaterial: not free sprite slots\n");

	sprite->spriteID = sprID;

	// create sprite
	NF_Create3dSprite(sprite->spriteID, sprite->graphic->vramID, sprite->graphic->palID, x, y);
	NF_Show3dSprite(sprite->spriteID, true);

	// sort them
	NF_Sort3dSprites();

	// get pixel format for screen
	const SDL_PixelFormat& fmt = *(r_window->format);

	// create surface for current graphic
	graphic->surf = SDL_CreateRGBSurface(
		SDL_HWSURFACE,
		material->image->size.x,
		material->image->size.y,
		fmt.BitsPerPixel,
		fmt.Rmask, fmt.Gmask, fmt.Bmask, fmt.Amask
	);

	// set color key
	SDL_SetColorKey(graphic->surf, SDL_SRCCOLORKEY, SDL_MapRGB(&fmt, 0xFF, 0x00, 0xFF));

	// clone rect
	graphic->rect = graphic->material->rect;

	// copy surface to "vram" backbuffer
	SDL_BlitSurface(graphic->material->surf, &graphic->material->rect, graphic->surf, &graphic->rect);

	return sprites[sprID] = sprite;
}

void R_UpdateMaterial(sprite_t* sprite, uint32_t x, uint32_t y)
{
	Com_Assert(sprite == NULL, "R_UpdateMaterial: sprite is NULL\n");
	Com_Assert(sprite->graphic == NULL, "R_UpdateMaterial: sprite graphic is NULL\n");
	Com_Assert(sprite->graphic->material == NULL, "R_UpdateMaterial: sprite graphic material is NULL\n");
	Com_Assert(sprite->graphic->material->image == NULL, "R_UpdateMaterial: sprite graphic material image is NULL\n");

	if(sprite->graphic->surf != NULL)
	{
		if(sprite->flipped)
		{
			sprite->graphic->rect.x = x - sprite->graphic->rect.w;
		}
		else
		{
			sprite->graphic->rect.x = x;
		}

		sprite->graphic->rect.y = y;

		SDL_BlitSurface(sprite->graphic->surf, NULL, r_sprites[0][0], &sprite->graphic->rect);
	}
}

void R_UpdateMaterialPriority(sprite_t* sprite, uint32_t priority)
{
	Com_Assert(sprite == NULL, "R_FlipMaterial: sprite is NULL\n");
	Com_Assert(sprite->graphic == NULL, "R_FlipMaterial: sprite graphic is NULL\n");
	Com_Assert(sprite->graphic->material == NULL, "R_FlipMaterial: sprite graphic material is NULL\n");
	Com_Assert(sprite->graphic->material->image == NULL, "R_FlipMaterial: sprite graphic material image is NULL\n");

	NF_Set3dSpritePriority(sprite->spriteID, priority);
}

void R_FlipMaterial(sprite_t* sprite, bool flip)
{
	Com_Assert(sprite == NULL, "R_FlipMaterial: sprite is NULL\n");
	Com_Assert(sprite->graphic == NULL, "R_FlipMaterial: sprite graphic is NULL\n");
	Com_Assert(sprite->graphic->material == NULL, "R_FlipMaterial: sprite graphic material is NULL\n");
	Com_Assert(sprite->graphic->material->image == NULL, "R_FlipMaterial: sprite graphic material image is NULL\n");

	if(sprite->flipped == flip) return;

	sprite->flipped = flip;

	SDL_Surface* newSurface = flip_surface(sprite->graphic->surf, 0);
	SDL_FreeSurface(sprite->graphic->surf);
	sprite->graphic->surf = newSurface;
}

bool R_MaterialFlipped(sprite_t* sprite)
{
	Com_Assert(sprite == NULL, "R_FlipMaterial: sprite is NULL\n");
	Com_Assert(sprite->graphic == NULL, "R_FlipMaterial: sprite graphic is NULL\n");
	Com_Assert(sprite->graphic->material == NULL, "R_FlipMaterial: sprite graphic material is NULL\n");
	Com_Assert(sprite->graphic->material->image == NULL, "R_FlipMaterial: sprite graphic material image is NULL\n");

	return sprite->flipped;
}

void R_HideMaterial(sprite_t* sprite)
{
	Com_Assert(sprite == NULL, "R_FlipMaterial: sprite is NULL\n");
	Com_Assert(sprite->graphic == NULL, "R_FlipMaterial: sprite graphic is NULL\n");
	Com_Assert(sprite->graphic->material == NULL, "R_FlipMaterial: sprite graphic material is NULL\n");
	Com_Assert(sprite->graphic->material->image == NULL, "R_FlipMaterial: sprite graphic material image is NULL\n");

	NF_Delete3dSprite(sprite->spriteID);

	sprites[sprite->spriteID] = NULL;

	// decrease reference of graphic info (vram)
	sprite->graphic->refs --;

	// check vram graphic info (we could need to unload data not in use in vram)
	if(sprite->graphic->refs == 0)
	{
		// remove from the list
		graphics[sprite->graphic->vramID] = NULL;

		// decrease references and/or unload material
		R_UnloadMaterial(sprite->graphic->material);

		// unload data from vram
		NF_Free3dSpriteGfx(sprite->graphic->vramID);

		// free gfx data
		Memory_Free(sprite->graphic);
	}

	// free sprite memory
	Memory_Free(sprite);
}

void R_UnloadMaterial(material_t* material)
{
	Com_Assert(material == NULL, "R_UnloadMaterial: material is NULL\n");

	material->refs --;

	if(material->refs > 0) return;

	// remove the material from the list
	materials[material->ramID] = NULL;

	// decrease assets references
	material->asset->ref --;

	// unload sprite gfx
	NF_UnloadSpriteGfx(material->ramID);

	// free material info
	Memory_Free(material);
}
   
void R_Frame()
{
	// update every background
	for(uint8_t slot = 0; slot < r_maxbackgrounds; slot ++)
	{
		// update the background if loaded
		if(backgrounds[slot] != NULL && backgrounds[slot]->hidden == false) R_UpdateBackground(backgrounds[slot]);
	}

	// update every sprite
	for(uint8_t slot = 0; slot < r_maxsprites; slot ++)
	{
		if(sprites[slot] != NULL && sprites[slot]->graphic->refs > 0)
		{
// 			SDL_BlitSurface(sprites[slot]->graphic->surf, NULL, r_sprites[0][0], &sprites[slot]->graphic->rect);
		}
	}

	// vertical sync
	swiWaitForVBlank();
}


// Background functions
u32 R_GetTileMapAddress(uint16_t width, uint16_t height, int16_t tile_x, int16_t tile_y) // RIP OFF NFlib!
{
	// basic map info
	uint16_t size_x = R_TILES(width);					// width tiles
	uint16_t size_y = R_TILES(height);					// height tiles

	// fix values to stay in range
	if(tile_x < 0) tile_x = size_x + tile_x; else if(tile_x >= size_x) tile_x %= size_x;
	if(tile_y < 0) tile_y = size_y + tile_y; else if(tile_y >= size_y) tile_y %= size_y;

	uint16_t block_x = tile_x >> 5;						// n� de pantalla (X)
	uint16_t block_y = tile_y >> 5;						// n� de pantalla (Y)
	uint32_t row_size = (size_x >> 5) << 11;			// bytes size of a tiles row

	// make sure we stay in range
	// Com_Assert(tile_x > size_x, "R_GetTileMapAddress: horizontal tile selected out of range\n");
	// Com_Assert(tile_y > size_y, "R_GetTileMapAddress: vertical tile selected out of range\n");

	// finally generate tile address (32x32 tiles per block, by rows)
	uint32_t scr_y = (block_y * row_size);
	uint32_t scr_x = (block_x << 11);
	uint32_t tls_y = ((tile_y - (block_y << 5)) << 5);
	uint32_t tls_x = (tile_x - (block_x << 5));

	// this is what we are looking for, return the value
	return scr_y + scr_x + ((tls_y + tls_x) << 1);
}

uint16_t R_GetTileOfMap(background_t* background, int16_t tile_x, int16_t tile_y)
{
	// get tile address in map
	//uint32_t address = R_GetTileMapAddress(background->image->size.x, background->image->size.y, tile_x, tile_y);

	uint16_t size_x = R_TILES(background->image->size.x);
	uint16_t size_y = R_TILES(background->image->size.y);

	if(tile_x < 0) tile_x += size_x;
	if(tile_y < 0) tile_y += size_y;

	uint32_t address = tile_y * size_x + tile_x;

	Com_Assert(
		address > background->ram.map.length, "Buffer overflow: %dx%d@0x%x of %dx%d@0x%x\n",
		tile_x,
		tile_y,
		address,
		size_x,
		size_y,
		background->ram.map.length
	);

	// current tile
	return background->ram.map.data[address];
}

uint16_t R_GetTileOfVramMap(background_t* background, int16_t tile_x, int16_t tile_y)
{
	FUNC_STUB_PRINT
	return 0;
}

void R_SetTileOfMap(background_t* background, int16_t tile_x, int16_t tile_y, uint16_t tile)
{
	FUNC_STUB_PRINT
}

void R_SetTileOfVramMap(background_t* background, int16_t tile_x, int16_t tile_y, uint16_t tile)
{
	FUNC_STUB_PRINT
}

background_t* R_PrecacheBackground(assetHeader_t* asset, uint8_t screen, uint8_t layer, bool repeat)
{
	// get the array position
	uint8_t slot = (screen == 0) ? layer : (4 + layer);

	// check if there is already a background and stop the program
	Com_Assert(backgrounds[slot] != NULL, "R_PrecacheBackgroundTileset: layer %d in screen %d already in use\n", layer, screen);

	// increase asset references
	asset->ref ++;

	// initialize data structure
	background_t* background = (background_t*)Memory_Alloc(sizeof(background_t));
	Com_Memset(background, 0, sizeof(background_t));

	// store needed data
	background->asset = asset;
	background->screen = screen;
	background->layer = layer;
	background->image = ((map_t*)asset->data)->image;
	background->hidden = true;
	background->repeat = repeat;

	// vram data
	background->vram.map.base = 0;
	background->vram.map.length = R_VRAM_MAP_BLOCK_LENGTH * R_VRAM_MAP_BLOCKS;

	background->vram.tileset.base = 0;
	background->vram.tileset.length = R_VRAM_TILESET_LENGTH;

	// allocate ram for vram buffers
	background->vram.map.data = (uint16_t*)Memory_Alloc(sizeof(uint8_t) * background->vram.map.length);

	Com_Memset(background->vram.map.data, 0xFF, sizeof(uint8_t) * background->vram.map.length);

	// scroll info
	background->scroll.x = 0;
	background->scroll.y = 0;
	background->scrolled = true;
	SDL_Rect rect = {0, 0, 256, 192};
	background->rect = rect;

	// make sure the background is not visible yet
	R_SetBackgroundVisibility(background->screen, background->layer, false);

	// build filenames
	char* sbifile = String_Create(strlen(asset->path) + 4);
	strcpy(sbifile, asset->path);
	strcat(sbifile, ".sbi");

	// open the img file
	file_t* fp = FileSystem_Open(sbifile, FileSystemMode_Read);

	char head[4];
	FileSystem_Read(fp, sizeof(char), 4, &head);

	Com_Assert(head[0] != 'S' || head[1] != 'S' || head[2] != 'B' || head[3] != 'I', "R_PrecacheBackground: file's %s header incorrect", sbifile);

	// get palette size
	FileSystem_Read(fp, sizeof(uint16_t), 1, &background->pal.length);

	// allocate space for palette data
	background->pal.data = (uint16_t*)Memory_Alloc(R_VRAM_PALETTE_LENGTH);
	if(background->pal.length < R_VRAM_PALETTE_LENGTH) Com_Memset(background->pal.data, 0, R_VRAM_PALETTE_LENGTH);

	// read palette
	FileSystem_Read(fp, sizeof(uint8_t), background->pal.length, background->pal.data);

	// get the tileset size
	FileSystem_Read(fp, sizeof(uint32_t), 1, &background->ram.tileset.length);

	// set tile limits for vram an ram
	background->ram.tiles.tiles = background->ram.tileset.length >> 6;
	background->vram.tiles.tiles = R_VRAM_TILESET_TILES;

	// allocate space for tileset buffer
	background->ram.tileset.data = (r_tile_t*)Memory_Alloc(background->ram.tileset.length);

	// read the whole tileset to RAM
	FileSystem_Read(fp, sizeof(uint8_t), background->ram.tileset.length, background->ram.tileset.data);

	// tileset status indicator
	background->ram.tiles.status = (r_ram_tile_t*)Memory_Alloc(sizeof(r_ram_tile_t) * background->ram.tiles.tiles);
	background->vram.tiles.status = (r_vram_tile_t*)Memory_Alloc(sizeof(r_vram_tile_t) * background->vram.tiles.tiles);

	// initialize every tile data
	for(uint32_t i = 0; i < background->ram.tiles.tiles; i ++)
	{
		background->ram.tiles.status[i].vramTile = R_VRAM_TILE_NOT_LOADED;
	}

	for(uint32_t i = 0; i < background->vram.tiles.tiles; i ++)
	{
		background->vram.tiles.status[i].mapRefs = 0;
		background->vram.tiles.status[i].ramTile = R_VRAM_TILE_NOT_LOADED;
	}

	// go to the end and get the size
	FileSystem_Read(fp, sizeof(uint16_t), 1, &background->ram.map.length);

	// allocate space for map buffer
	background->ram.map.data = (uint16_t*)Memory_Alloc(background->ram.map.length);

	// read the whole map to RAM
	FileSystem_Read(fp, sizeof(uint8_t), background->ram.map.length, background->ram.map.data);

	// finally close the file
	FileSystem_Close(fp);

	// we do not the filename anymore
	Memory_Free(sbifile);

	// reconstruct the image on a custom surface
	const SDL_PixelFormat& fmt = *(r_window->format);

	// first create the bg surface
	background->surf = SDL_CreateRGBSurface(SDL_HWSURFACE, background->image->size.x, background->image->size.y, fmt.BitsPerPixel, 31, 31 << 5, 31 << 10, 0);

	for(int16_t y = 0; y < R_TILES(background->image->size.y); y ++)
	{
		for(int16_t x = 0; x < R_TILES(background->image->size.x); x ++)
		{
			uint16_t tile = R_GetTileOfMap(background, x, y);

			Com_Assert(tile >= background->ram.tiles.tiles, "R_PrecacheBackground: tile %d out of range (%d) - this suggests memory corruption if .sbi file wasn't manually edited", tile, background->ram.tiles.tiles);

			// get the tile data
			uint16_t data[R_VRAM_TILE_LENGTH];

			for(int i = 0; i < R_VRAM_TILE_LENGTH; i += 1)
			{
				uint8_t id = background->ram.tileset.data[tile].info[i];
				data[i] = background->pal.data[id];
			}

			SDL_Rect rect = {(int16_t)(x * R_TILE_WIDTH), (int16_t)(y * R_TILE_HEIGHT), R_TILE_WIDTH, R_TILE_HEIGHT};
			SDL_Surface* t = SDL_CreateRGBSurfaceFrom(&data, R_TILE_WIDTH, R_TILE_HEIGHT, fmt.BitsPerPixel, R_TILE_WIDTH * 2, 31, 31 << 5, 31 << 10, 0);
			SDL_BlitSurface(t, NULL, background->surf, &rect);
		}
	}

	// add the background to the list and return the pointer
	return backgrounds[slot] = background;
}

void R_DisplayBackground(background_t* background)
{
	if(background->hidden == true)
	{
		R_SetBackgroundVisibility(background->screen, background->layer, true);
		background->hidden = false;
	}
}

void R_HideBackground(background_t* background)
{
	if(background->hidden == false)
	{
		R_SetBackgroundVisibility(background->screen, background->layer, false);
		background->hidden = true;
	}
}

// scroll background
void R_ScrollBackground(background_t* background, int32_t x, int32_t y)
{
	// just update scroll position and mark it for update
	background->scroll.x = x;
	background->scroll.y = y;
	background->scrolled = true;

	// small checks to not overflow the map
	if(background->repeat == false)
	{
		// make sure the scroll stays in place if needed
		if(background->scroll.x > background->image->size.x - camera.size.x) background->scroll.x = background->image->size.x - camera.size.x;
		if(background->scroll.y > background->image->size.y - camera.size.y) background->scroll.y = background->image->size.y - camera.size.y;
	}

	background->rect.x = background->scroll.x;
	background->rect.y = background->scroll.y;
}

// updates vram tiles if needed
void R_UpdateBackground(background_t* background)
{
	// we do not need to update the bg if its not scrolled
	if(background->scrolled == false) return;

	// we can rely on this function to do the work
	SDL_BlitSurface(background->surf, &background->rect, r_backgrounds[background->screen][background->layer], &r_backgrounds_size[background->screen][background->layer]);
}

void R_UnloadBackground(background_t* background)
{
	FUNC_STUB_PRINT
	// remove the background from the list
	uint8_t slot = (background->screen == 0) ? background->layer : (4 + background->layer);

	// first of all remove from the slot
	backgrounds[slot] = NULL;

	// hide bg first
	R_SetBackgroundVisibility(background->screen, background->layer, false);

	// decrease asset ref
	background->asset->ref --;

	// free vram map data
	Memory_Free(background->vram.map.data);

	// free the struct data
	Memory_Free(background);
}

void R_SetBackgroundVisibility(uint8_t screen, uint8_t layer, bool show)
{
	switch(show)
	{
		case false:
			NF_HideBg(screen, layer);
			SDL_FillRect(r_backgrounds[screen][layer], NULL, r_transparentColor);
			break;

		case true:
			NF_ShowBg(screen, layer);
			break;
	}
}

void R_SetBackgroundOffsets(uint8_t screen, uint8_t layer, uint32_t x, uint32_t y)
{
	FUNC_STUB_PRINT
}
