#ifndef __GRAPHICS_H__
#define __GRAPHICS_H__

#include <nds.h>
#include <common.h>
#include <assets.h>

// screen and engine data
#define R_SCREEN_WIDTH				(256)
#define R_SCREEN_HEIGHT				(192)

#define R_VRAM_BG_WIDTH				(512)
#define R_VRAM_BG_HEIGHT			(256)

// tiles-to-pixels and pixels-to-tiles conversions
#define R_TILES(x)					((x) >> 3)
#define R_PIXELS(x)					((x) << 3)

// default parameters for 512x256 backgrounds
#define R_VRAM_MAP_TILE_LENGTH		(2)
#define R_VRAM_MAP_BLOCK_LENGTH		((R_TILES(R_VRAM_BG_WIDTH) * R_TILES(R_VRAM_BG_HEIGHT)) << 1)
#define R_VRAM_MAP_BLOCKS			(((R_VRAM_MAP_BLOCK_LENGTH - 1) >> 11) + 1)

// tileset data
#define R_TILE_WIDTH				(8)
#define R_TILE_HEIGHT				(8)

#define R_VRAM_TILE_LENGTH			(R_TILE_WIDTH * R_TILE_HEIGHT)
#define R_VRAM_TILE_NOT_LOADED		(65535)
#define R_IS_TILE_LOADED(x)			(x != R_VRAM_TILE_NOT_LOADED)

#define R_VRAM_TILESET_WIDTH		R_TILES(R_SCREEN_WIDTH)
#define R_VRAM_TILESET_HEIGHT		R_TILES(R_SCREEN_HEIGHT)

#define R_VRAM_TILESET_TILES		((R_VRAM_TILESET_WIDTH + 3) * (R_VRAM_TILESET_HEIGHT + 3))
#define R_VRAM_TILESET_LENGTH		(R_VRAM_TILESET_TILES << 6)
#define R_VRAM_TILESET_BLOCKS		(((R_VRAM_TILESET_LENGTH - 1) >> 14) + 1)

// palette data
#define R_VRAM_PALETTE_COLORS		(256)
#define R_VRAM_PALETTE_LENGTH		(R_VRAM_PALETTE_COLORS << 1)

// needed to reference between 'em
struct material_t;
struct graphic_t;
struct sprite_t;

// base struct type for graphics list
struct material_t
{
   assetHeader_t* asset;
   
   uint8_t ramID;
   uint8_t palID;
   uint8_t refs;
   
   image_t* image;

#ifndef __NDSBUILD
	SDL_Surface* surf;
	SDL_Rect rect;
#endif /* !__NDSBUILD */

   // graphic_t* graphic; // used to know if the material is already in vram or not
};

// base struct for vram-ed sprites data
struct graphic_t
{
	material_t* material; // we need to keep this

	uint8_t vramID;
	uint8_t palID;
	uint8_t refs;

#ifndef __NDSBUILD
	SDL_Surface* surf;
	SDL_Rect rect;
#endif /* __NDSBUILD */
};

// base struct for sprites info
struct sprite_t
{
	graphic_t* graphic; // we need to keep this

	uint8_t spriteID;

	uint32_t priority;
	bool flipped;
};

struct r_vram_tile_t
{
	uint16_t mapRefs;
	uint16_t ramTile;
};

struct r_ram_tile_t
{
	uint16_t vramTile;
};

struct r_vram_tileset_t
{
	r_vram_tile_t* status;
	uint32_t tiles;
};

struct r_ram_tileset_t
{
	r_ram_tile_t* status;
	uint32_t tiles;
};

struct r_map_t
{
	uint16_t* data;
	uint16_t base;
	uint32_t length;
#ifndef __NDSBUILD
	SDL_Surface* surf;
	SDL_Rect rect;
#endif /* !__NDSBUILD */
};

struct r_tile_t
{
	uint8_t info[R_VRAM_TILE_LENGTH];
};

struct r_tileset_t
{
	r_tile_t* data;
	uint16_t base;
	uint32_t length;

#ifndef __NDSBUILD
	SDL_Surface* surf;
	SDL_Rect rect;
#endif /* !__NDSBUILD */
};

struct r_vram_t
{
	r_map_t map;
	r_tileset_t tileset;
	r_vram_tileset_t tiles;
};

struct r_ram_t
{
	r_map_t map;
	r_tileset_t tileset;
	r_ram_tileset_t tiles;
};

struct r_pal_t
{
	uint16_t* data;
	uint16_t length;
};

// base struct for backgrounds
struct background_t
{
	assetHeader_t* asset;
	image_t* image;
	
	coord_t scroll;
	bool scrolled;

	bool repeat;

	r_vram_t vram;
	r_ram_t ram;

	// palete info
	r_pal_t pal;

	uint8_t screen;
	uint8_t layer;
	bool hidden;

#ifndef __NDSBUILD
	SDL_Surface* surf;
	SDL_Rect rect;
#endif /* !__NDSBUILD */
};

struct camera_t
{
	uint8_t focus;
	coord_t position;
	coord_t size;
};

void R_Init(uint8_t maxMaterials, uint8_t maxSprites, uint8_t maxGraphics);
material_t* R_LoadMaterial(assetHeader_t* asset, image_t* image, bool transparent);
sprite_t* R_DisplayMaterial(material_t* graphic, uint32_t x, uint32_t y);
void R_UpdateMaterial(sprite_t* graphic, uint32_t x, uint32_t y);
void R_UpdateMaterialPriority(sprite_t* graphic, uint32_t priority);
void R_FlipMaterial(sprite_t* graphic, bool flip);
bool R_MaterialFlipped(sprite_t* graphic);
void R_HideMaterial(sprite_t* graphic);
void R_UnloadMaterial(material_t* graphic);
void R_Frame();

// background functions
background_t* R_PrecacheBackground(assetHeader_t* asset, uint8_t screen, uint8_t layer, bool repeat);
void R_DisplayBackground(background_t* background);
void R_HideBackground(background_t* background);
void R_ScrollBackground(background_t* background, int32_t x, int32_t y);

void R_End();

#endif /* __GRAPHICS_H__ */

