﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace mapViewer.Viewer
{
    public static class Viewer
    {
        private static SpriteBatch spriteBatch = null;
        private static Texture2D texture = null;
        private static Texture2D image = null;

        public static void Initialize(SpriteBatch spr)
        {
            spriteBatch = spr; 
        }

        public static void Load(string map, Texture2D tex)
        {
            texture = tex;

            // open map file and load the tileset information
            FileStream stream = new FileStream(map, FileMode.Open);
            BinaryReader reader = new BinaryReader(stream);

            // skip map size info
            int width = reader.ReadInt32();
            int height = reader.ReadInt32();

            // prepare image
            image = new Texture2D(texture.GraphicsDevice, width, height);

            // skip spawns data by the moment
            byte spawns = reader.ReadByte();

            for (int i = 0; i < spawns; i++)
            {
                // skip coords for spawn entry
                reader.ReadInt32();
                reader.ReadInt32();
            }

            // get tiles count
            short tiles = reader.ReadInt16();

            // read every tile in the map and copy the tiles to final image
            for (int y = 0; y < height / 8; y++)
            {
                for (int x = 0; x < width / 8; x++)
                {
                    short tilex = reader.ReadInt16();
                    short tiley = 0;

                    while (tilex >= (512 / 8))
                    {
                        tilex -= (512 / 8);
                        tiley++;
                    }

                    // copy tile to image
                    Color[] src = new Color[8 * 8];
                    
                    texture.GetData<Color>(0, new Rectangle(tilex * 8, tiley * 8, 8, 8), src, 0, 8 * 8);
                    image.SetData<Color>(0, new Rectangle(x * 8, y * 8, 8, 8), src, 0, 8*8);
                }
            }
        }

        public static void Draw()
        {
            spriteBatch.Begin();

            // draw texture
            spriteBatch.Draw(image, new Rectangle(0, 0, image.Width, image.Height), Color.White);

            spriteBatch.End();
        }
    }
}
