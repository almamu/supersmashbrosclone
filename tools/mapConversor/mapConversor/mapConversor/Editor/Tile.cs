﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace mapConversor.Editor
{
    class Tile
    {
        private int x = 0;
        private int y = 0;

        public Tile(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Rectangle GetTile()
        {
            return new Rectangle(this.x * 8, this.y * 8, 8, 8);
        }
    }
}
