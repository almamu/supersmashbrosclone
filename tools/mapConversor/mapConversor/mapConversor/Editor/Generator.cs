﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace mapConversor.Editor
{
    public static class Generator
    {
        private static SpriteBatch spriteBatch = null;
        private static Tile[,] map = null;
        private static Texture2D texture = null;
        private static Tile[,] optimized = null;
        private static Texture2D result = null;
        private static bool converted = false;

        public static void Initialize(SpriteBatch spr, Texture2D texture)
        {
            Generator.spriteBatch = spr;
            Generator.texture = texture;

            // prepare output texture
            result = new Texture2D(texture.GraphicsDevice, 512, 512);

            // initialize map data
            map = new Tile[Generator.texture.Width / 8, Generator.texture.Height / 8];
            optimized = new Tile[Generator.result.Width / 8, Generator.result.Height / 8];

            // initialize arrays
            for (int x = 0; x < Generator.texture.Width / 8; x++)
            {
                for (int y = 0; y < Generator.texture.Height / 8; y++)
                {
                    map[x, y] = new Tile(x, y);
                }
            }

            for (int x = 0; x < result.Width / 8; x++)
            {
                for (int y = 0; y < result.Height / 8; y++)
                {
                    optimized[x, y] = null;
                }
            }
        }

        private static short TileExists(Rectangle source)
        {
            short tile = -1;

            for (int y = 0; y < result.Height / 8; y++)
            {
                for (int x = 0; x < result.Width / 8; x++)
                {
                    tile++;

                    if (optimized[x, y] == null) continue;

                    Color[] srcColor = new Color[8 * 8];
                    Color[] dstColor = new Color[8 * 8];

                    texture.GetData<Color>(0, source, srcColor, 0, 8 * 8);
                    texture.GetData<Color>(0, optimized[x, y].GetTile(), dstColor, 0, 8 * 8);

                    int equal = 0;

                    for (int i = 0; i < dstColor.Length; i++)
                    {
                        if (srcColor[i].R == dstColor[i].R &&
                            srcColor[i].G == dstColor[i].G &&
                            srcColor[i].B == dstColor[i].B)
                            equal++;
                    }

                    if (equal == dstColor.Length) return tile;
                }
            }

            return -1;
        }

        public static void OptimizeTileset()
        {
            if (converted == true) return;

            int resx = 0;
            int resy = 0;

            /*for (int y = 0; y < Generator.texture.Height / 8; y++)
            {
                for (int x = 0; x < Generator.texture.Width / 8; x++)
                {
                    if (TileExists(map[x, y].GetTile()) == -1)
                    {
                        optimized[resx, resy] = map[x, y];

                        resx++;

                        if (resx >= result.Width / 8)
                        {
                            resx = 0;
                            resy++;
                        }
                    }
                }
            }

            for (resy = 0; resy < result.Height / 8; resy++)
            {
                for (resx = 0; resx < result.Width / 8; resx++)
                {
                    if (optimized[resx, resy] == null) continue;

                    Color[] color = new Color[8*8];
                    texture.GetData<Color>(0, optimized[resx, resy].GetTile(), color, 0, 8 * 8);

                    result.SetData<Color>(0, new Rectangle(resx * 8, resy * 8, 8, 8), color, 0, 8 * 8);
                }
            }*/

            FileStream stream = new FileStream("test.png", FileMode.Create);

            result.SaveAsPng(stream, result.Width, result.Height);

            stream.Close();

            converted = true;

            // generate map file
            FileStream output = new FileStream("output.map", FileMode.Create);
            BinaryWriter writer = new BinaryWriter(output);

            // write map size
            writer.Write(texture.Width);
            writer.Write(texture.Height);

            // add spawns info
            writer.Write((byte)2);

            // add spawns data
            writer.Write(130);
            writer.Write(250);

            writer.Write(330);
            writer.Write(250);

            // write tiles count
            /*int tiles = 0;

            for (int y = 0; y < (result.Height / 8); y++)
            {
                for (int x = 0; x < (result.Width / 8); x++)
                {
                    if (optimized[x, y] != null) tiles++;
                }
            }

            writer.Write(tiles);

            for (int y = 0; y < result.Height / 8; y++)
            {
                for (int x = 0; x < result.Width / 8; x++)
                {
                    short tile = TileExists(new Rectangle(x * 8, y * 8, 8, 8));

                    if (tile == -1) writer.Write((short)0);
                    else writer.Write((short)tile);
                }
            }*/

            output.Close();
        }

        public static void Draw()
        {
            spriteBatch.Begin();

            spriteBatch.Draw(texture, new Rectangle(0, 0, texture.Width, texture.Height), Color.White);
            if(converted == true) spriteBatch.Draw(result, new Rectangle(texture.Width, 0, result.Width, result.Height), Color.White);

            spriteBatch.End();
        }
    }
}
