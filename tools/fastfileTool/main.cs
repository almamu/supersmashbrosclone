﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using fastfileTool.CVS;
using fastfileTool.Fastfile;

namespace fastfileTool
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();

            // setup openFileDialog data
            openFileDialog1.DefaultExt = "CVS file (*.cvs)|*.cvs";
            openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialog = openFileDialog1.ShowDialog();

            if (dialog == DialogResult.OK)
            {
                string output = openFileDialog1.FileName;
                List<FFItem> items = Parser.ParseFile(output);

                if (items != null)
                {
                    // change extension
                    output = output.Replace(".cvs", ".ff");
                    
                    Generator.Generate(output, items);
                }
            }
        }
    }
}
