﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace fastfileTool.CVS
{
    public class FFItem
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Type { get; set; }
        public string[] ExtraData { get; set; }
    }

    public class Parser
    {
        private static FFItem ParseLine(string line)
        {
            // first split the line by commas
            string[] data = line.Split(',');

            if (data.Length < 3)
            {
                return null;
            }

            // build item
            FFItem item = new FFItem();

            // store item data
            item.Name = data[0];
            item.Path = data[1];
            item.Type = data[2];

            // copy extra info (needed for image width, for example)
            if (data.Length > 3)
            {
                item.ExtraData = new string[data.Length - 3];

                Array.Copy(data, 3, item.ExtraData, 0, data.Length - 3);
            }

            return item;
        }

        public static List<FFItem> ParseFile(string cvs)
        {
            string[] data = File.ReadAllLines(cvs);
            List<FFItem> result = new List<FFItem>();

            foreach (string line in data)
            {
                // ignore comments and empty lines
                if (line.StartsWith("#") == true || string.IsNullOrEmpty(line) == true || string.IsNullOrWhiteSpace(line) == true) continue;

                FFItem item = ParseLine(line);

                if (item == null) return null;

                result.Add(item);
            }

            return result;
        }
    }
}
