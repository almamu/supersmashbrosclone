﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using fastfileTool.CVS;

namespace fastfileTool.Fastfile
{
    enum assetType_t
    {
        ASSET_MAP = 0, // game maps
        ASSET_CHARACTER = 1, // characters
        ASSET_ICON = 2, // game icons
        ASSET_GFX = 3, // attacks fx
        ASSET_SOUND = 4, // game sound
        ASSET_MENU = 5, // game menu
        ASSET_IMAGE = 6, // general image
        ASSET_COLMAP = 7,
        ASSET_MAX,
    };

    class Generator
    {
        private static void WriteString(BinaryWriter writer, string value)
        {
            int length = value.Length;

            writer.Write(length);
            writer.Write(Encoding.ASCII.GetBytes(value));
        }

        private static bool GenerateImage(BinaryWriter writer, FFItem item)
        {
            if (item.ExtraData.Length < 3)
            {
                Log.Error("Image needs 3 data fields, " + item.ExtraData.Length + " got");
                return false;
            }

            // check data first
            int width = 0, height = 0;
            byte frames = 0;

            try
            {
                int.TryParse(item.ExtraData[0], out width);
                int.TryParse(item.ExtraData[1], out height);
                byte.TryParse(item.ExtraData[2], out frames);
            }
            catch (Exception)
            {
                return false;
            }

            // write asset type
            writer.Write((int)(assetType_t.ASSET_IMAGE));

            // write image info
            WriteString(writer, item.Name); // asset name
            WriteString(writer, item.Path); // asset path
            writer.Write(width); // image width
            writer.Write(height); // image height
            writer.Write(frames); // image frames (important, ds may go crazy if we dont keep 'em)

            return true;
        }

        private static bool GenerateCollisionMap(BinaryWriter writer, FFItem item)
        {
            if (item.ExtraData.Length < 2)
            {
                Log.Error("CollisionMap needs 3 data fields, " + item.ExtraData.Length + " got");
                return false;
            }

            // check data first
            int width = 0, height = 0;

            try
            {
                int.TryParse(item.ExtraData[0], out width);
                int.TryParse(item.ExtraData[1], out height);
            }
            catch (Exception)
            {
                return false;
            }

            // write asset type
            writer.Write((int)(assetType_t.ASSET_COLMAP));

            // write image info
            WriteString(writer, item.Name); // asset name
            WriteString(writer, item.Path); // asset path
            writer.Write(width); // collision map width
            writer.Write(height); // collision map height

            return true;
        }

        private static bool GenerateMap(BinaryWriter writer, FFItem item)
        {
            // just write type and name
            writer.Write((int)(assetType_t.ASSET_MAP));

            // write asset name
            WriteString(writer, item.Name);
            WriteString(writer, item.Path);

            int width, height, spawns;

            try
            {
                int.TryParse(item.ExtraData[0], out width);
                int.TryParse(item.ExtraData[1], out height);
                int.TryParse(item.ExtraData[2], out spawns);
            }
            catch
            {
                Log.Error("Cannot parse extra data");
                return false;
            }

            if (spawns > 255)
            {
                Log.Error("A map cannot have more than 255 spawns!");
                return false;
            }

            // just throw everything into the file
            writer.Write(width);
            writer.Write(height);
            writer.Write((char)spawns);

            for (int i = 0; i < spawns; i++)
            {
                int x, y;

                try
                {
                    int.TryParse(item.ExtraData[2 + (i * 2) + 1], out x);
                    int.TryParse(item.ExtraData[2 + (i * 2) + 2], out y);
                }
                catch
                {
                    Log.Error("Cannot parse spawn position " + i);
                    return false;
                }

                // spawn position
                writer.Write(x);
                writer.Write(y);
            }

            return true;
        }

        private static bool GenerateCharacter(BinaryWriter writer, FFItem item)
        {
            // character struct is not fully defined yet, so this is majorly just a stub
            if (item.ExtraData.Length < 3)
            {
                Log.Error("Character needs 3 data fields, got " + item.ExtraData.Length);
                return false;
            }

            // write asset type
            writer.Write((int)(assetType_t.ASSET_CHARACTER));

            // write asset name and path
            WriteString(writer, item.Name);
            WriteString(writer, item.Path);

            int width, height;

            try
            {
                int.TryParse(item.ExtraData[0], out width);
                int.TryParse(item.ExtraData[1], out height);
            }
            catch
            {
                Log.Error("Cannot parse extra data");
                return false;
            }

            // just throw everything into the file
            writer.Write(width);
            writer.Write(height);
            WriteString(writer, item.ExtraData[2]);

            // finally return!
            return true;
        }

        public static bool Generate(string output, List<FFItem> items)
        {
            FileStream stream = new FileStream(output, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(stream);

            // write assets count
            writer.Write(items.Count);

            // loop the items list
            foreach (FFItem item in items)
            {
                bool result = false;

                // add item to fastfile if possible
                switch (item.Type.ToLower())
                {
                    case "image":
                        result = GenerateImage(writer, item);
                        break;

                    case "collisionmap":
                        result = GenerateCollisionMap(writer, item);
                        break;

                    case "map":
                        result = GenerateMap(writer, item);
                        break;

                    case "character":
                        result = GenerateCharacter(writer, item);
                        break;
                }

                if (result == false)
                {
                    Log.Error("Cannot generate fastfile. " + Environment.NewLine + " file: " + item.Path + " of type " + item.Type.ToLower());
                    return false;
                }

                // update file data
                stream.Flush();
            }

            // close writer
            writer.Close();

            // close the file
            stream.Close();

            Log.Debug("Fastfile generation completed on " + output);

            return true;
        }
    }
}
