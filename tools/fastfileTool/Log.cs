﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fastfileTool
{
    public static class Log
    {
        public static void Debug(string message)
        {
            Console.Write("DEBUG: ");
            Console.WriteLine(message);
        }

        public static void Error(string message)
        {
            Console.Write("ERROR: ");
            Console.WriteLine(message);
        }

        public static void Warning(string message)
        {
            Console.Write("WARNING: ");
            Console.WriteLine(message);
        }
    }
}
