﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSBGraphicUtil
{
    public class MapEntry
    {
        public int X { get; set; }
        public int Y { get; set; }
        public ushort Tile { get; set; }
    }
}
