﻿using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SSBGraphicUtil
{
    public partial class ResultControler : GraphicsDeviceControl
    {
        public Texture2D Texture { get; private set; }
        private SpriteBatch batch;
        private int basex = 0;
        private int basey = 0;
        private int oldmousex = 0;
        private int oldmousey = 0;

        /// <summary>
        /// Initializes the control.
        /// </summary>
        protected override void Initialize()
        {
            // Hook the idle event to constantly redraw our animation.
            Application.Idle += delegate { Invalidate(); };

            batch = new SpriteBatch(GraphicsDevice);
        }

        public bool LoadImage(List<MapEntry> map, List<Color[]> tileset, string filename)
        {
            // cargamos la textura desde el fichero
            FileStream fs = File.Open(filename, FileMode.Open);
            Texture2D tex = Texture2D.FromStream(GraphicsDevice, fs);
            fs.Close();

            Texture2D bg = new Texture2D(GraphicsDevice, tex.Width, tex.Height);

            foreach (MapEntry entry in map)
            {
                Rectangle dest = new Rectangle(entry.X * TilesetControler.TileWidth, entry.Y * TilesetControler.TileHeight, TilesetControler.TileWidth, TilesetControler.TileHeight);

                try
                {
                    bg.SetData<Color>(0, dest, tileset[entry.Tile], 0, tileset[entry.Tile].Length);
                }
                catch
                {
                    return false;
                }
            }

            Texture = bg;

            return true;
        }

        /// <summary>
        /// Draws the control.
        /// </summary>
        protected override void Draw()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            batch.Begin();

            if (Texture != null)
            {
                MouseState state = Mouse.GetState();

                if (state.LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed)
                {
                    if (oldmousex == 0 && oldmousey == 0)
                    {
                        oldmousex = state.X;
                        oldmousey = state.Y;
                    }

                    basex -= oldmousex - state.X;
                    basey -= oldmousey - state.Y;
                }

                oldmousex = state.X;
                oldmousey = state.Y;

                batch.Draw(Texture, new Vector2(basex, basey), Color.White);
            }

            batch.End();
        }
    }
}
