﻿using System.Diagnostics;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SSBGraphicUtil
{
    public partial class PaleteControl : GraphicsDeviceControl
    {
        private Texture2D paleteTexture;
        public List<Color> Palete { get; private set; }
        private SpriteBatch batch;
        private const int PaleteWidth = 15; // 285
        private const int PaleteHeight = 18;
        private const int PaleteTileWidth = 16;
        private const int PaleteTileHeight = 24;

        /// <summary>
        /// Initializes the control.
        /// </summary>
        protected override void Initialize()
        {
            // Hook the idle event to constantly redraw our animation.
            Application.Idle += delegate { Invalidate(); };
            batch = new SpriteBatch(GraphicsDevice);
        }

        public bool LoadImage(string filename)
        {
            List<Color> colors = new List<Color>(255);

            // cargamos la textura desde el fichero
            FileStream fs = File.Open(filename, FileMode.Open);
            Texture2D tex = Texture2D.FromStream(GraphicsDevice, fs);
            fs.Close();

            // obtenemos todos los colores
            Color[] colours = new Color[tex.Width * tex.Height];
            tex.GetData<Color>(colours);

            // vamos añadiendo colores a la paleta
            foreach (var color in colours)
            {
                if (colors.Contains(color) == false)
                {
                    if (colors.Count < 256) colors.Add(color);
                    else return false;
                }
            }

            // construimos ahora la imagen de textura con la paleta :D
            paleteTexture = new Texture2D(GraphicsDevice, PaleteTileWidth * PaleteWidth, PaleteTileHeight * PaleteHeight);
            Color[] data = new Color[PaleteTileWidth * PaleteWidth * PaleteTileHeight * PaleteHeight];
            for (int j = 0; j < data.Length; j++) data[j] = Color.Transparent;
            paleteTexture.SetData<Color>(data);

            int x = 0, y = 0;

            foreach (var color in colors)
            {
                data = new Color[PaleteTileWidth * PaleteTileHeight];
                for (int j = 0; j < data.Length; j++) data[j] = color;

                paleteTexture.SetData<Color>(0, new Rectangle(x, y, PaleteTileWidth, PaleteTileHeight), data, 0, data.Length);
                x += PaleteTileWidth;

                if (x > ((PaleteWidth - 1) * PaleteTileWidth))
                {
                    x = 0; y += PaleteTileHeight;
                }
            }

            Palete = colors;

            return true;
        }

        /// <summary>
        /// Draws the control.
        /// </summary>
        protected override void Draw()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            batch.Begin();

            if (paleteTexture != null)
            {
                batch.Draw(
                    paleteTexture,
                    new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height),
                    null,
                    Color.White
                );
            }

            batch.End();
        }
    }
}
