﻿using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SSBGraphicUtil
{
    public partial class TilesetControler : GraphicsDeviceControl
    {
        public Texture2D Tileset { get; private set; }
        public List<Color[]> TilesetData { get; private set; }
        public List<MapEntry> MapData { get; private set; }
        private SpriteBatch batch;
        public const int TileWidth = 8;
        public const int TileHeight = 8;
        public const int TilesetWidth = 34;

        private int basex = 0;
        private int basey = 0;
        private int oldmousex = 0;
        private int oldmousey = 0;

        /// <summary>
        /// Initializes the control.
        /// </summary>
        protected override void Initialize()
        {
            // Hook the idle event to constantly redraw our animation.
            Application.Idle += delegate { Invalidate(); };
            batch = new SpriteBatch(GraphicsDevice);
        }

        public bool LoadImage(string filename)
        {
            List<Color[]> tiles = new List<Color[]>();
            List<MapEntry> map = new List<MapEntry>();

            // cargamos la textura desde el fichero
            FileStream fs = File.Open(filename, FileMode.Open);
            Texture2D tex = Texture2D.FromStream(GraphicsDevice, fs);
            fs.Close();

            // nos aseguramos de que la textura es multiplo de 8
            if (tex.Width % TileWidth > 0 || tex.Height % TileHeight > 0)
            {
                return false;
            }

            int maxTileX = tex.Width / TileWidth;
            int maxTileY = tex.Height / TileHeight;

            // primero buscamos todas las tiles que sean unicas y las añadimos al buffer
            for (int x = 0; x < maxTileX; x++)
            {
                for (int y = 0; y < maxTileY; y++)
                {
                    ushort mapEntryPlace = 0;

                    Rectangle rect = new Rectangle(x * TileWidth, y * TileHeight, TileWidth, TileHeight);

                    Color[] colors = new Color[TileWidth * TileHeight];
                    tex.GetData<Color>(0, rect, colors, 0, TileWidth * TileHeight);

                    bool add = true;

                    foreach(var tile in tiles)
                    {
                        if(tile.Length != colors.Length)
                        {
                            mapEntryPlace++;
                            continue;
                        }

                        bool found = true;

                        for (int i = 0; i < tile.Length; i++)
                        {
                            if (tile[i] != colors[i])
                            {
                                mapEntryPlace++;
                                found = false;
                                break;
                            }
                        }

                        if (found)
                        {
                            add = false;
                            break;
                        }
                    }

                    if (add)
                    {
                        tiles.Add(colors);
                        mapEntryPlace = (ushort)(tiles.Count - 1);
                    }

                    MapEntry entry = new MapEntry();

                    entry.X = x;
                    entry.Y = y;
                    entry.Tile = mapEntryPlace;

                    map.Add(entry);
                }
            }

            // la informacion del mapa ya la podemos guardar
            MapData = map;

            // una vez esta todo filtrado podemos empezar a añadir a la textura
            Texture2D tileset = new Texture2D(GraphicsDevice, TilesetWidth * TileWidth, TileHeight * ((tiles.Count / TilesetWidth) + 1));

            int tilex = 0, tiley = 0;

            foreach (var tile in tiles)
            {
                tileset.SetData<Color>(0, new Rectangle(tilex * TileWidth, tiley * TileHeight, TileWidth, TileHeight), tile, 0, tile.Length);

                tilex++;

                if (tilex >= TilesetWidth)
                {
                    tilex = 0; tiley++;
                }
            }

            Tileset = tileset;
            TilesetData = tiles;

            return true;
        }

        /// <summary>
        /// Draws the control.
        /// </summary>
        protected override void Draw()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            batch.Begin();

            if (Tileset != null)
            {
                MouseState state = Mouse.GetState();

                if (state.RightButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed)
                {
                    if (oldmousex == 0 && oldmousey == 0)
                    {
                        oldmousex = state.X;
                        oldmousey = state.Y;
                    }

                    basex -= oldmousex - state.X;
                    basey -= oldmousey - state.Y;
                }

                oldmousex = state.X;
                oldmousey = state.Y;

                batch.Draw(Tileset, new Vector2(basex, basey), Color.White);
            }

            batch.End();
        }
    }
}
