#region File Description
//-----------------------------------------------------------------------------
// MainForm.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System.IO;
using System.Text;
using System.Windows.Forms;
#endregion

namespace SSBGraphicUtil
{
    // System.Drawing and the XNA Framework both define Color types.
    // To avoid conflicts, we define shortcut names for them both.
    using GdiColor = System.Drawing.Color;
    using XnaColor = Microsoft.Xna.Framework.Color;

    
    /// <summary>
    /// Custom form provides the main user interface for the program.
    /// In this sample we used the designer to add a splitter pane to the form,
    /// which contains a SpriteFontControl and a SpinningTriangleControl.
    /// </summary>
    public partial class MainForm : Form
    {
        private bool somethingLoaded = false;

        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.DefaultExt = ".png";
            openFileDialog1.Filter = "Imagenes PNG|*.png|Imagenes JPEG|*.jpg";

            if(openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            textBox1.Text = openFileDialog1.FileName;

            if (palete1.LoadImage(textBox1.Text) == false)
            {
                MessageBox.Show("La paleta tiene mas de 256 colores, por favor optimiza la imagen", "Error en la paleta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                somethingLoaded = false;
                return;
            }

            if (tileset1.LoadImage(textBox1.Text) == false)
            {
                MessageBox.Show("No se pudo generar el tileset, comprueba las dimensiones de la imagen, han de ser multiplos de 8", "Error en el tileset", MessageBoxButtons.OK, MessageBoxIcon.Error);
                somethingLoaded = false;
                return;
            }

            if (result1.LoadImage(tileset1.MapData, tileset1.TilesetData, textBox1.Text) == false)
            {
                MessageBox.Show("No se pudo generar la imagen final a traves del tileset", "Error en la paleta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                somethingLoaded = false;
                return;
            }

            MessageBox.Show("Imagen cargada con exito", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            somethingLoaded = true;
        }

        private bool ExportData(string filename)
        {
            FileStream stream = new FileStream(filename, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(stream);

            // escribimos la cabecera de la imagen
            writer.Write(Encoding.ASCII.GetBytes("SSBI"));

            // escribimos el tama�o en bytes de la paleta (2 bytes por entrada)
            writer.Write((ushort)(palete1.Palete.Count * 2));

            // escribimos los datos de la paleta
            foreach (var color in palete1.Palete)
            {
                byte r = (byte)(color.R * 31 / 255);
                byte g = (byte)(color.G * 31 / 255);
                byte b = (byte)(color.B * 31 / 255);
                ushort rgb = (ushort)((r) | ((g) << 5) | ((b) << 10));

                writer.Write(rgb);
            }

            // una vez acabamos con la paleta necesitamos generar el tileset
            // la forma mas facil es escribir en trozos de 8x8 en el fichero
            writer.Write((tileset1.TilesetData.Count * (TilesetControler.TileWidth * TilesetControler.TileHeight)));

            foreach (var tile in tileset1.TilesetData)
            {
                foreach (var color in tile)
                {
                    int pal = 0;
                    bool found = false;

                    for (pal = 0; pal < palete1.Palete.Count; pal++)
                    {
                        if (color == palete1.Palete[pal])
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        return false;
                    }

                    // escribimos el color del pixel del tileset
                    writer.Write((byte)(pal));
                }
            }

            // ahora nos quedan los datos del mapa
            // son esenciales para mostrar nuestro fondo mas adelante
            writer.Write((ushort)(tileset1.MapData.Count * 2));

            ushort[,] map = new ushort[result1.Texture.Width / TilesetControler.TileWidth, result1.Texture.Height / TilesetControler.TileHeight];

            for (int x = 0; x < result1.Texture.Width / TilesetControler.TileWidth; x++)
            {
                for (int y = 0; y < result1.Texture.Height / TilesetControler.TileHeight; y++)
                {
                    map[x, y] = 0;
                }
            }

            foreach (var place in tileset1.MapData)
            {
                map[place.X, place.Y] = place.Tile;
            }

            for (int y = 0; y < result1.Texture.Height / TilesetControler.TileHeight; y++)
            {
                for (int x = 0; x < result1.Texture.Width / TilesetControler.TileWidth; x++)
                {
                    writer.Write((ushort)(map[x, y]));
                }
            }

            // por ultimo cerramos el escritor ya que hemos acabado de generar el fichero
            writer.Close();

            return true;
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            if (somethingLoaded == false)
            {
                MessageBox.Show("Tienes que cargar una imagen antes de poder exportarla", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            saveFileDialog1.FileName = "";
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.DefaultExt = "sbi";
            saveFileDialog1.Filter = "SuperSmashBros Image|*.sbi";

            if (saveFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            if (ExportData(saveFileDialog1.FileName) == false)
            {
                MessageBox.Show("No se pudo guardar la imagen SBI en " + saveFileDialog1.FileName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Imagen SBI generada con exito", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
