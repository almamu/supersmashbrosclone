﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using fastfileTool.CVS;
using characterTool.Characters;

namespace characterTool
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            // setup openFileDialog data
            openFileDialog1.Filter = "CHS file (*.chs)|*.chs";
            openFileDialog1.DefaultExt = "*.chs";
            openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialog = openFileDialog1.ShowDialog();

            if (dialog == DialogResult.OK)
            {
                string output = openFileDialog1.FileName;

                Generator.Generate(output);
            }
        }
    }
}
