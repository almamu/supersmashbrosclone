﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fastfileTool;

namespace characterTool
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Log.Debug("CharacterTool v1.0 started");

            new Main().ShowDialog();
        }
    }
}
