﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using fastfileTool;
using fastfileTool.CVS;

namespace characterTool.Characters
{
    class AnimData_t
    {
        public byte frames, speed;
        public string[] files;
    };

    class Generator
    {
        public enum AnimType_t
        {
            None = 0,
            Stand = 1,
            Walk = 2,
            Punch = 3,
            LowPunch = 4,
            Kick = 5,
            LowKick = 6,
            Crouch = 7,
            Jump = 8,
        };

        private static Dictionary<AnimType_t, AnimData_t> data = new Dictionary<AnimType_t, AnimData_t>();

        public static void Generate(string file)
        {
            Log.Debug("Parsing character file \"" + file + "\"");

            // initialize data
            data = new Dictionary<AnimType_t, AnimData_t>();

            // add every entry to have dummy data
            data.Add(AnimType_t.Stand, new AnimData_t());
            data.Add(AnimType_t.Walk, new AnimData_t());
            data.Add(AnimType_t.Punch, new AnimData_t());
            data.Add(AnimType_t.LowPunch, new AnimData_t());
            data.Add(AnimType_t.Kick, new AnimData_t());
            data.Add(AnimType_t.LowKick, new AnimData_t());
            data.Add(AnimType_t.Crouch, new AnimData_t());
            data.Add(AnimType_t.Jump, new AnimData_t());

            // parse everything
            string[] lines = File.ReadAllLines(file);

            foreach(string line in lines)
            {
                if (line.StartsWith("#") == true || string.IsNullOrWhiteSpace(line) == true) continue;

                if (ParseLine(line) == false)
                {
                    Log.Error("Cannot parse line: " + line);
                    continue;
                }
            }

            GenerateFile(file);
        }

        public static bool ParseLine(string line)
        {
            if (line.StartsWith("animation ") == false)
            {
                Log.Error("Non-animation info in line: " + line);
                return false;
            }

            string[] info = line.Split(',');

            if (info.Length == 0)
            {
                Log.Error("Unknown animation type: " + line);
                return false;
            }

            string[] type = info[0].Split(' ');
            AnimType_t animType = AnimType_t.None;

            switch (type[1])
            {
                case "stand":
                    animType = AnimType_t.Stand;
                    break;

                case "walk":
                    animType = AnimType_t.Walk;
                    break;

                case "kick":
                    animType = AnimType_t.Kick;
                    break;

                case "lowkick":
                    animType = AnimType_t.LowKick;
                    break;

                case "punch":
                    animType = AnimType_t.Punch;
                    break;

                case "lowpunch":
                    animType = AnimType_t.LowPunch;
                    break;

                case "crouch":
                    animType = AnimType_t.Crouch;
                    break;

                case "jump":
                    animType = AnimType_t.Jump;
                    break;

                default:
                    Log.Error("Unknown anim type " + type[1]);
                    return false;
            }
            
            // get the data entry from the dict
            if (data.ContainsKey(animType) == false)
            {
                Log.Error("Unknown animation type " + type[1]);
                return false;
            }

            try
            {
                AnimData_t anim = data[animType];

                byte.TryParse(info[1], out anim.frames);
                byte.TryParse(info[2], out anim.speed);

                if (anim.frames != (info.Length - 3))
                {
                    Log.Error("Frames do not match");
                    return false;
                }

                if (anim.frames > 0)
                {
                    anim.files = new string[anim.frames];

                    Log.Debug("Found animation node of type " + type[1] + " of " + anim.frames + " frames");

                    // copy over file names
                    for (int i = 0; i < anim.files.Count() || i < (info.Length - 3); i++)
                    {
                        anim.files[i] = info[i + 3].Trim();
                    }
                }
                else
                {
                    anim.files = null; // make sure it stays as null
                    Log.Warning("Found animation (" + type[1] + ") without frames");
                }
            }
            catch (Exception ex)
            {
                Log.Error("Cannot parse line: " + ex.ToString());
                return false;
            }

            return true;
        }

        private static void WriteString(string value, BinaryWriter writer)
        {
            writer.Write(value.Length);
            writer.Write(Encoding.ASCII.GetBytes(value));
        }

        private static void SerializeAnim(AnimData_t anim, BinaryWriter writer, string nodeName)
        {
            Log.Debug("Serializing animation data for node " + nodeName + " (frames: " + anim.frames + "; speed: " + anim.speed + ")");

            if (anim.files != null && anim.files.Count() != anim.frames)
            {
                Log.Warning("Frames count and files stored mismatch. Relying on anim.files.Count()");

                if (anim.files.Count() > byte.MaxValue)
                {
                    Log.Warning("Frames are more than " + byte.MaxValue + ". Limiting to " + byte.MaxValue);
                }

                anim.frames = (byte)(anim.files.Count());
            }

            writer.Write(anim.frames);
            writer.Write(anim.speed);

            for(int i = 0; i < anim.frames; i ++ )
            {
                WriteString(anim.files[i], writer);
            }
        }

        public static void GenerateFile(string output)
        {
            FileStream stream = new FileStream(output.Remove(output.Length - 4), FileMode.Create); // we do not want the extension
            BinaryWriter writer = new BinaryWriter(stream);

            // serialize animations
            SerializeAnim(data[AnimType_t.Stand], writer, "stand");
            SerializeAnim(data[AnimType_t.Walk], writer, "walk");
            SerializeAnim(data[AnimType_t.Kick], writer, "kick");
            SerializeAnim(data[AnimType_t.LowKick], writer, "lowkick");
            SerializeAnim(data[AnimType_t.Punch], writer, "punch");
            SerializeAnim(data[AnimType_t.LowPunch], writer, "lowpunch");
            SerializeAnim(data[AnimType_t.Crouch], writer, "crouch");
            SerializeAnim(data[AnimType_t.Jump], writer, "jump");

            Log.Debug("Character generation completed successfully");

            writer.Close();
            stream.Close();
        }
    }
}
