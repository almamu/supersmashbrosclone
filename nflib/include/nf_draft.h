#ifndef __NFDRAFT_H__
#define __NFDRAFT_H__

#ifndef WIN32
# if __STDC_VERSION__ < 199901L
#  ifndef __cplusplus
#   if __GNUC__ >= 2
#    define __func__ __FUNCTION__
#   else
#    define __func__ "<unknown>"
#   endif
#  endif
# endif
#else
# define __func__ __FUNCTION__
#endif

extern void Com_Printf(int level, const char* str, ...);
#define FUNC_STUB_PRINT Com_Printf(2, "Called %s stub\n", __func__);

#endif /* !__NFDRAFT_H__ */