
// NightFox LIB - Funciones 2D comunes
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409



// Includes devKitPro
#include <nds.h>

// Includes C
#include <stdio.h>
#include <string.h>

// Includes propios
#include "nf_basic.h"
#include "nf_3d.h"
#include "nf_sprite3d.h"
#include "nf_sprite256.h"





//////////////////////////////////
// Defines y variables globales //
//////////////////////////////////

// Estructura de control de los sprites 3d
NF_TYPE_3DSPRITE_INFO NF_3DSPRITE[NF_3DSPRITES];

// Estructura de control de las texturas en VRAM
NF_TYPE_TEX256VRAM_INFO NF_TEX256VRAM[NF_3DSPRITES];

// Estructura de control de las paletas en VRAM
NF_TYPE_3DSPRPALSLOT_INFO NF_TEXPALSLOT[32];

// Estructura de control de la VRAM de texturas
NF_TYPE_TEXVRAM_INFO NF_TEXVRAM;

// Define la estructura de control de los sprites 3d creados
NF_TYPE_CREATED_3DSPRITE_INFO NF_CREATED_3DSPRITE;





// Funcion NF_Init3dSpriteSys();
void NF_Init3dSpriteSys(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_Vram3dSpriteGfx();
void NF_Vram3dSpriteGfx(u16 ram, u16 vram, bool keepframes) {
	FUNC_STUB_PRINT
}



// Funcion NF_Free3dSpriteGfx();
void NF_Free3dSpriteGfx(u16 id) {
	FUNC_STUB_PRINT
}



// Funcion NF_Vram3dSpriteGfxDefrag();
void NF_Vram3dSpriteGfxDefrag(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_Vram3dSpritePal();
void NF_Vram3dSpritePal(u8 id, u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_Create3dSprite();
void NF_Create3dSprite(u16 id, u16 gfx, u16 pal, s16 x, s16 y) {
	FUNC_STUB_PRINT
}



// Funcion NF_Delete3dSprite();
void NF_Delete3dSprite(u16 id) {
	FUNC_STUB_PRINT
}



// Funcion NF_Sort3dSprites();
void NF_Sort3dSprites(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_Set3dSpritePriority();
void NF_Set3dSpritePriority(u16 id, u16 prio) {
	FUNC_STUB_PRINT
}



// Funcion NF_Swap3dSpritePriority();
void NF_Swap3dSpritePriority(u16 id_a, u16 id_b) {
	FUNC_STUB_PRINT
}



// Funcion NF_Move3dSprite();
void NF_Move3dSprite(u16 id, s16 x, s16 y) {
	FUNC_STUB_PRINT
}



// Funcion NF_Show3dSprite();
void NF_Show3dSprite(u16 id, bool show) {
	FUNC_STUB_PRINT
}



// Funcion NF_Set3dSpriteFrame();
void NF_Set3dSpriteFrame(u16 id, u16 frame) {
	FUNC_STUB_PRINT
}



// Funcion NF_Draw3dSprites();
void NF_Draw3dSprites(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_Update3dSpritesGfx();
void NF_Update3dSpritesGfx(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_Rotate3dSprite();
void NF_Rotate3dSprite(u16 id, s16 x, s16 y, s16 z) {
	FUNC_STUB_PRINT
}



// Funcion NF_Scale3dSprite();
void NF_Scale3dSprite(u16 id, u16 x, u16 y) {
	FUNC_STUB_PRINT
}



// Funcion NF_Blend3dSprite();
void NF_Blend3dSprite(u8 sprite, u8 poly_id, u8 alpha) {
	FUNC_STUB_PRINT
}



// Funcion NF_3dSpritesLayer();
void NF_3dSpritesLayer(u8 layer) {
	FUNC_STUB_PRINT
}



// Funcion NF_3dSpriteEditPalColor();
void NF_3dSpriteEditPalColor(u8 pal, u8 number, u8 r, u8 g, u8 b) {
	FUNC_STUB_PRINT
}



// Funcion 	NF_3dSpriteUpdatePalette();
void NF_3dSpriteUpdatePalette(u8 pal) {
	FUNC_STUB_PRINT
}



// Funcion NF_3dSpriteGetPalColor();
void NF_3dSpriteGetPalColor(u8 pal, u8 number, u8* r, u8* g, u8* b) {
	FUNC_STUB_PRINT
}


// Funcion NF_3dSpriteSetDeep();
void NF_3dSpriteSetDeep(u8 id, s16 z) {
	FUNC_STUB_PRINT
}
