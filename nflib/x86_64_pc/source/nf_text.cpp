
// NightFox LIB - Funciones de Textos
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409



// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// Includes propios
#include "nf_basic.h"
#include "nf_2d.h"
#include "nf_tiledbg.h"
#include "nf_text.h"




// Define los buffers para almacenar las capas de texto
NF_TYPE_TEXT_INFO NF_TEXT[2][4];



// Funcion NF_InitTextSys();
void NF_InitTextSys(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_LoadTextFont();
void NF_LoadTextFont(const char* file, const char* name, u16 width, u16 height, u8 rotation) {
	FUNC_STUB_PRINT
}



// Funcion NF_UnloadTestFont();
void NF_UnloadTextFont(const char* name) {
	NF_UnloadTiledBg(name);
}



// Funcion NF_CreateTextLayer();
void NF_CreateTextLayer(u8 screen, u8 layer, u8 rotation, const char* name) {
	FUNC_STUB_PRINT
}



// Funcion NF_DeleteTextLayer();
void NF_DeleteTextLayer(u8 screen, u8 layer) {
	FUNC_STUB_PRINT
}



// Funcion NF_WriteText();
void NF_WriteText(u8 screen, u8 layer, u16 x, u16 y, const char* text) {
	FUNC_STUB_PRINT
}



// Funcion NF_UpdateTextLayers();
void NF_UpdateTextLayers(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_ClearTextLayer();
void NF_ClearTextLayer(u8 screen, u8 layer) {
	FUNC_STUB_PRINT
}



// Funcion NF_DefineTextColor();
void NF_DefineTextColor(u8 screen, u8 layer, u8 color, u8 r, u8 g, u8 b) {
	FUNC_STUB_PRINT
}



// Function NF_SetTextColor();
void NF_SetTextColor(u8 screen, u8 layer, u8 color) {
	FUNC_STUB_PRINT
}
