
// NightFox LIB - Funciones de Sprites a 256 colores
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409





// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>

#include <common.h>

// Includes propios
#include "nf_basic.h"
#include "nf_2d.h"
#include "nf_sprite256.h"

extern SDL_Surface* r_window;

// Define los Buffers para almacenar los Sprites
char* NF_BUFFER_SPR256GFX[NF_SLOTS_SPR256GFX];
char* NF_BUFFER_SPR256PAL[NF_SLOTS_SPR256PAL];

// Define la estructura de datos de los Graficos de los Sprites
NF_TYPE_SPR256GFX_INFO NF_SPR256GFX[NF_SLOTS_SPR256GFX];
// Define la estructura de datos de las Paletas de los Sprites
NF_TYPE_SPR256PAL_INFO NF_SPR256PAL[NF_SLOTS_SPR256PAL];

// Define la estructura de Gfx en VRAM
NF_TYPE_SPR256VRAM_INFO NF_SPR256VRAM[2][128];
// Datos de paletas de Sprites en VRAM (en uso, slot en ram, etc)
NF_TYPE_SPRPALSLOT_INFO NF_SPRPALSLOT[2][16];

// Define la estructura de datos del OAM (Sprites)
NF_TYPE_SPRITEOAM_INFO NF_SPRITEOAM[2][128];		// 2 pantallas, 128 sprites

// Define la esturctura de control de la VRAM para Sprites
NF_TYPE_SPRVRAM_INFO NF_SPRVRAM[2];		// Informacion VRAM de Sprites en ambas pantallas





// Funcion NF_InitSpriteBuffers()
void NF_InitSpriteBuffers(void) {

	u16 n = 0;	// Variable comun

	// Inicializa Buffers de GFX
	for (n = 0; n < NF_SLOTS_SPR256GFX; n ++) {
		NF_BUFFER_SPR256GFX[n] = NULL;			// Inicializa puntero
		NF_SPR256GFX[n].size = 0;				// Tama�o (en bytes) del grafico (GFX)
		NF_SPR256GFX[n].width = 0;				// Ancho del Gfx
		NF_SPR256GFX[n].height = 0;				// Altura del Gfx
		NF_SPR256GFX[n].available = true;		// Disponibilidat del Slot
	}

	// Inicializa Buffers de PAL
	for (n = 0; n < NF_SLOTS_SPR256PAL; n ++) {
		NF_BUFFER_SPR256PAL[n] = NULL;		// Inicializa puntero
		NF_SPR256PAL[n].size = 0;			// Tama�o (en bytes) de la paleta (PAL)
		NF_SPR256PAL[n].available = true;	// Disponibilidat del Slot
	}

}



// Funcion NF_ResetSpriteBuffers()
void NF_ResetSpriteBuffers(void) {
	u16 n = 0;	// Variable comun

	// Borra los Buffers de GFX
	for (n = 0; n < NF_SLOTS_SPR256GFX; n ++) {
		free(NF_BUFFER_SPR256GFX[n]);
	}

	// Borra los Buffers de PAL
	for (n = 0; n < NF_SLOTS_SPR256PAL; n ++) {
		free(NF_BUFFER_SPR256PAL[n]);
	}

	// Reinicia el sistema de Sprites
	NF_InitSpriteBuffers();
}



// Funcion NF_InitSpriteSys();
void NF_InitSpriteSys(int screen, ...) {

	// Analiza los parametros variables de la funcion
	va_list options;
	va_start(options, screen);
	u8 mode = va_arg(options, int);
	va_end(options);


	// Variables
	u8 n = 0;	// Uso comun

	// Inicializa la estructura de Gfx en VRAM
	// y la estructura de datos del OAM (Sprites)
	for (n = 0; n < 128; n ++) {	// 128 sprites
		// Gfx en la VRAM (128 Gfx x pantalla)
		NF_SPR256VRAM[screen][n].size = 0;				// Tama�o (en bytes) del Gfx
		NF_SPR256VRAM[screen][n].width = 0;				// Ancho del Gfx
		NF_SPR256VRAM[screen][n].height = 0;			// Altura del Gfx
		NF_SPR256VRAM[screen][n].address = 0;			// Posicion en la VRAM
		NF_SPR256VRAM[screen][n].ramid = 0;				// Numero de Slot en RAM del que provienes
		NF_SPR256VRAM[screen][n].framesize = 0;			// Tama�o del frame (en bytes)
		NF_SPR256VRAM[screen][n].lastframe = 0;			// Ultimo frame
		NF_SPR256VRAM[screen][n].keepframes = false;	// Si es un Sprite animado, debes de mantener los frames en RAM ?
		NF_SPR256VRAM[screen][n].inuse = false;			// Esta en uso ?
		// OAM (128 Sprites x pantalla)
		NF_SPRITEOAM[screen][n].index = n;			// Numero de Sprite (Index = N)
		NF_SPRITEOAM[screen][n].x = 0;				// Coordenada X del Sprite (0 por defecto)
		NF_SPRITEOAM[screen][n].y = 0;				// Coordenada Y del Sprite (0 por defecto)
		NF_SPRITEOAM[screen][n].layer = 0;			// Prioridad en las capas (0 por defecto)
		NF_SPRITEOAM[screen][n].pal = 0;			// Paleta que usaras (0 por defecto)
		NF_SPRITEOAM[screen][n].size = SpriteSize_8x8;	// Tama�o del Sprite (macro) (8x8 por defecto)
		NF_SPRITEOAM[screen][n].color = SpriteColorFormat_16Color;	// Modo de color (macro) (256 colores)
		NF_SPRITEOAM[screen][n].gfx = NULL;			// Puntero al grafico usado
		NF_SPRITEOAM[screen][n].rot = -1;			// Id de rotacion (-1 por defecto) (0 - 31 Id de rotacion)
		NF_SPRITEOAM[screen][n].doublesize = false;	// Usar el "double size" al rotar ? ("NO" por defecto)
		NF_SPRITEOAM[screen][n].hide = true;		// Ocultar el Sprite ("SI" por defecto)
		NF_SPRITEOAM[screen][n].hflip = false;		// Volteado Horizontal ("NO" por defecto)
		NF_SPRITEOAM[screen][n].vflip = false;		// Volteado Vertical ("NO" por defecto)
		NF_SPRITEOAM[screen][n].mosaic = false;		// Mosaico ("NO" por defecto)
		NF_SPRITEOAM[screen][n].gfxid = 0;			// Numero de Gfx usado
		NF_SPRITEOAM[screen][n].frame = 0;			// Frame actual
		NF_SPRITEOAM[screen][n].framesize = 0;		// Tama�o del frame (en bytes)
		NF_SPRITEOAM[screen][n].lastframe = 0;		// Ultimo frame
		NF_SPRITEOAM[screen][n].created = false;	// Esta creado este sprite ?
		NF_SPRITEOAM[screen][n].surf = NULL;		// Surface para dibujar el sprite
	}

	// Inicializa la estructura de datos de la VRAM de Sprites
	if (mode == 128) {
		NF_SPRVRAM[screen].max = 131072;
	} else {
		NF_SPRVRAM[screen].max = 65536;
	}
	NF_SPRVRAM[screen].free = NF_SPRVRAM[screen].max;		// Memoria VRAM libre (64kb/128kb)
	NF_SPRVRAM[screen].last = 0;							// Ultima posicion usada
	NF_SPRVRAM[screen].deleted = 0;							// Ningun Gfx borrado
	NF_SPRVRAM[screen].fragmented = 0;						// Memoria VRAM fragmentada
	NF_SPRVRAM[screen].inarow = NF_SPRVRAM[screen].max;		// Memoria VRAM contigua
	for (n = 0; n < 128; n ++) {
		NF_SPRVRAM[screen].pos[n] = 0;		// Posicion en VRAM para reusar despues de un borrado
		NF_SPRVRAM[screen].size[n] = 0;		// Tama�o del bloque libre para reusar
	}

	// Inicializa los datos de las paletas
	for (n = 0; n < 16; n ++) {
		NF_SPRPALSLOT[screen][n].inuse = false;
		NF_SPRPALSLOT[screen][n].ramslot = 0;
	}

}



// Funcion NF_LoadSpriteGfx();
void NF_LoadSpriteGfx(const char* file, u16 id,  u16 width, u16 height)
{

}



// Funcion NF_UnloadSpriteGfx();
void NF_UnloadSpriteGfx(u16 id) {
	FUNC_STUB_PRINT
}



// Funcion NF_LoadSpritePal();
void NF_LoadSpritePal(const char* file, u8 id) {
	FUNC_STUB_PRINT
}



// Funcion NF_UnloadSpritePal();
void NF_UnloadSpritePal(u8 id) {
	FUNC_STUB_PRINT
}



// Funcion NF_VramSpriteGfx();
void NF_VramSpriteGfx(u8 screen, u16 ram, u16 vram, bool keepframes) {
	FUNC_STUB_PRINT
}



// Funcion NF_FreeSpriteGfx();
void NF_FreeSpriteGfx(u8 screen, u16 id) {
	FUNC_STUB_PRINT
}



// Funcion NF_VramSpriteGfxDefrag();
void NF_VramSpriteGfxDefrag(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_VramSpritePal();
void NF_VramSpritePal(u8 screen, u8 id, u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_CreateSprite();
void NF_CreateSprite(u8 screen, u8 id, u16 gfx, u8 pal, s16 x, s16 y) {
	FUNC_STUB_PRINT
}



// Funcion NF_DeleteSprite();
void NF_DeleteSprite(u8 screen, u8 id) {
	FUNC_STUB_PRINT
}



// Funcion NF_SpriteOamSet();
void NF_SpriteOamSet(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_SpriteSetPalColor();
void NF_SpriteSetPalColor(u8 screen, u8 pal, u8 number, u8 r, u8 g, u8 b) {
	FUNC_STUB_PRINT
}



// Funcion NF_SpriteEditPalColor();
void NF_SpriteEditPalColor(u8 screen, u8 pal, u8 number, u8 r, u8 g, u8 b) {
	FUNC_STUB_PRINT
}



// Funcion 	NF_SpriteUpdatePalette();
void NF_SpriteUpdatePalette(u8 screen, u8 pal) {
	FUNC_STUB_PRINT
}



// Funcion NF_SpriteGetPalColor();
void NF_SpriteGetPalColor(u8 screen, u8 pal, u8 number, u8* r, u8* g, u8* b) {
	FUNC_STUB_PRINT
}
