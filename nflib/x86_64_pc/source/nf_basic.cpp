
// NightFox LIB - Funciones basicas y de Debug
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409



// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// Includes propios
#include "nf_basic.h"
#include "nf_defines.h"



// Define la variable global NF_ROOTFOLDER
char NF_ROOTFOLDER[32];

// this should be set by default
nf_errorhandler_t NF_ERRORHANDLER = (nf_errorhandler_t)NF_ShowError;
nf_messagehandler_t NF_MESSAGEHANDLER = (nf_messagehandler_t)NF_ShowMessage;

// default message handlers! (iprintf is no longer compatible)
void NF_ShowMessage(int level, const char* str, ...)
{
	char line[2048];
	
	va_list args;
	va_start(args, str);
	
	vsnprintf(line, 2048, str, args);
	
	va_end(args);
	
	iprintf("%s", line);
}

void NF_ShowError(const char* str, ...)
{
	char line[2048];
	
	va_list args;
	va_start(args, str);
	
	vsnprintf(line, 2048, str, args);
	
	va_end(args);
	
	iprintf("%s", line);
}

void NF_SetErrorHandler(nf_errorhandler_t handler)
{
	NF_ERRORHANDLER = handler;
}

void NF_SetMessageHandler(nf_messagehandler_t handler)
{
	NF_MESSAGEHANDLER = handler;
}

// Funcion NF_Error();
void NF_Error(u16 code, const char* text, u32 value) {

	/*consoleDemoInit();		// Inicializa la consola de texto
	consoleClear();			// Borra la pantalla
	setBrightness(3, 0);	// Restaura el brillo
*/
	u32 n = 0;	// Variables de uso general

	// Captura el codigo de error
	switch (code) {

		case 101:	// Fichero no encontrado
			NF_MESSAGEHANDLER(32, "File %s not found.\n", text);
			break;

		case 102:	// Memoria insuficiente
			NF_MESSAGEHANDLER(32, "Out of memory.\n%d bytes\ncan't be allocated.\n", value);
			break;

		case 103:	// No quedan Slots libres
			NF_MESSAGEHANDLER(32, "Out of %s slots.\nAll %d slots are in use.\n", text);
			break;

		case 104:	// Fondo no encontrado
			NF_MESSAGEHANDLER(32, "Tiled Bg %s\nnot found\n", text);
			break;

		case 105:	// Fondo no creado
			NF_MESSAGEHANDLER(32, "Background number %d\non screen %s is\nnot created.\n", value, text);
			break;

		case 106:	// Fuera de rango
			NF_MESSAGEHANDLER(32, "%s Id out\nof range (%d max).\n", text, value);
			break;

		case 107:	// Insuficientes bloques contiguos en VRAM (Tiles)
			n = (int)((value * 16384) / 1024);
			NF_MESSAGEHANDLER(32, "Can't allocate %d\nblocks of tiles for\n%s background\nFree %dkb of VRAM or try to\nreload all Bg's again\n", value, text, n);
			break;

		case 108:	// Insuficientes bloques contiguos en VRAM (Maps)
			n = (int)((value * 2048) / 1024);
			NF_MESSAGEHANDLER(32, "Can't allocate %d\nblocks of maps for\n%s background\nFree %dkb of VRAM or try to\nreload all Bg's again\n", value, text, n);
			break;

		case 109:	// Id ocupada
			NF_MESSAGEHANDLER(32, "%s Id.%d\nis already in use.\n", text, value);
			break;

		case 110:	// Id no cargada
			NF_MESSAGEHANDLER(32, "%s\n%d not loaded.\n", text, value);
			break;

		case 111:	// Id no en VRAM
			NF_MESSAGEHANDLER(32, "%s\n%d not in VRAM.\n", text, value);
			break;

		case 112:	// Sprite no creado
			NF_MESSAGEHANDLER(32, "Sprite number %d\non screen %s is\nnot created.\n", value, text);
			break;

		case 113:	// Memoria VRAM insuficiente
			NF_MESSAGEHANDLER(32, "Out of VRAM.\n%d bytes for %s\ncan't be allocated.\n", value, text);
			break;

		case 114:	// La capa de Texto no existe
			NF_MESSAGEHANDLER(32, "Text layer on screen\nn� %d don't exist.\n", value);
			break;

		case 115:	// Medidas del fondo no compatibles (no son multiplos de 256)
			NF_MESSAGEHANDLER(32, "Tiled Bg %s\nhas wrong size.\nYour bg sizes must be\ndividable by 256 pixels.\n", text);
			break;

		case 116:	// Archivo demasiado grande
			NF_MESSAGEHANDLER(32, "File %s\nis too big.\nMax size for\nfile is %dkb.\n", text, value >> 10);
			break;

		case 117:	// Medidas del fondo affine no compatibles (Solo se admiten 256x256 y 512x512)
			NF_MESSAGEHANDLER(32, "Affine Bg %s\nhas wrong size.\nYour bg sizes must be\n256x256 or 512x512 and\nwith 256 tiles or less.\n", text);
			break;

		case 118:	// Medidas del fondo affine no compatibles (Solo se admiten 256x256 y 512x512)
			NF_MESSAGEHANDLER(32, "Affine Bg %s\nonly can be created\non layers 2 or 3.\n", text);
			break;

		case 119:	// Tama�o de la textura ilegal.
			NF_MESSAGEHANDLER(32, "Texture id.%d illegal size.\nOnly power of 2 sizes can\nbe used (8 to 1024).\n", value);
			break;

		case 120:	// Tama�o de la Sprite ilegal.
			NF_MESSAGEHANDLER(32, "Sprite id.%d illegal size.\n8x8 Sprites can't be used\nin 1D_128 mode.\n", value);
			break;

	}

	NF_ERRORHANDLER("Error code %d.\n", code);		// Imprime el codigo de error

	// Deten la ejecucion del programa
	while (1) {
		swiWaitForVBlank();
	}

}



// Funcion NF_SetRootFolder();
void NF_SetRootFolder(const char* folder, char **output) 
{
}




// Funcion NF_DmaMemCopy();
void NF_DmaMemCopy(void* destination, const void* source, u32 size)
{
	memcpy(destination, source, size);
}



// Funcion NF_GetLanguage();
u8 NF_GetLanguage(void) {
	FUNC_STUB_PRINT
	return 0;
}
