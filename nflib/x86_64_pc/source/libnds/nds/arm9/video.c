#include <nds/ndstypes.h>
#include <nds/arm9/video.h>
#include <nds/arm9/input.h>

#include <SDL.h>

// local define, just for testing
#define NDS_FPS_LIMIT (60)

// render surfaces
SDL_Surface* r_window = NULL;
SDL_Surface* r_screens[2] = { NULL, NULL };
SDL_Surface* r_backgrounds[2][4] =
{
	{NULL, NULL, NULL, NULL},
	{NULL, NULL, NULL, NULL}
};

SDL_Surface* r_sprites[2][4] =
{
	{NULL, NULL, NULL, NULL},
	{NULL, NULL, NULL, NULL}
};

SDL_Rect r_sprites_size[2][4] =
{
	{
		{0, 0, 256, 192},
		{0, 0, 256, 192},
		{0, 0, 256, 192},
		{0, 0, 256, 192}
	},
	{
		{0, 0, 256, 192},
		{0, 0, 256, 192},
		{0, 0, 256, 192},
		{0, 0, 256, 192},
	}
};

// surfaces data
SDL_Rect r_screens_position[2] =
{
	{0, 0},
	{0, 192}
};

SDL_Rect r_screens_size[2] =
{
	{0, 0, 256, 192},
	{0, 0, 256, 192}
};

SDL_Rect r_backgrounds_size[2][4] =
{
	{
		{0, 0, 256, 192},
		{0, 0, 256, 192},
		{0, 0, 256, 192},
		{0, 0, 256, 192}
	},
	{
		{0, 0, 256, 192},
		{0, 0, 256, 192},
		{0, 0, 256, 192},
		{0, 0, 256, 192},
	}
};

// framelimit data
uint32_t r_maxTicks = 0;
uint32_t r_fps = 0;
uint32_t r_startTicks = 0;
uint32_t r_transparentColor = 0;

bool r_initialized = false;

// used to initialize the video systems
// THIS IS NOT REALLY AN INITIALIZATION ROUTINE
// BUT FOR SMASH THIS IS THE FIRST FUNCTION CALLED
void defaultExceptionHandler()
{
	int screen = 0;
	SDL_PixelFormat* fmt = NULL;
	int layer = 0;
	FILE* fp = NULL;
	bool loaded = false;

	if(r_initialized == true)
	{
		return;
	}

	// max ticks for the frame limit system
	r_maxTicks = 1000 / NDS_FPS_LIMIT;

	// sdl initialization
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		exit(-1);
	}

	// window video mode
	if( (r_window = SDL_SetVideoMode(256, 192 * 2, 16, 0)) == NULL)
	{
		exit(-1);
	}

	// set window data like title
	SDL_EnableUNICODE(1);
	SDL_WM_SetCaption("SuperSmashBros v1.0", NULL);

	// set transparent color to magenta (0xFF00FF)
	// and store it for further(?) use
	fmt = r_window->format;
	
	r_transparentColor = SDL_MapRGB(fmt, 0xFF, 0x00, 0xFF);
	SDL_SetColorKey(r_window, SDL_SRCCOLORKEY, r_transparentColor);

	// setup screens and backgrounds
	for(screen = 0; screen < 2; screen ++)
	{
		r_screens[screen] = SDL_CreateRGBSurface(SDL_HWSURFACE, 256, 192, fmt->BitsPerPixel, fmt->Rmask, fmt->Gmask, fmt->Bmask, fmt->Amask);
		SDL_SetColorKey(r_screens[screen], SDL_SRCCOLORKEY, r_transparentColor);

		// make sure every layer is transparent and usable by the render system
		for(layer = 0; layer < 4; layer ++)
		{
			r_backgrounds[screen][layer] = SDL_CreateRGBSurface(SDL_HWSURFACE, 256, 192, fmt->BitsPerPixel, fmt->Rmask, fmt->Gmask, fmt->Bmask, fmt->Amask);
			r_sprites[screen][layer] = SDL_CreateRGBSurface(SDL_HWSURFACE, 256, 192, fmt->BitsPerPixel, fmt->Rmask, fmt->Gmask, fmt->Bmask, fmt->Amask);

			// set background color
			SDL_FillRect(r_backgrounds[screen][layer], NULL, r_transparentColor);
			SDL_SetColorKey(r_backgrounds[screen][layer], SDL_SRCCOLORKEY, r_transparentColor);

			// set sprites color
			SDL_FillRect(r_sprites[screen][layer], NULL, r_transparentColor);
			SDL_SetColorKey(r_sprites[screen][layer], SDL_SRCCOLORKEY, r_transparentColor);
		}
	}

	// some small checks to make sure we can create screen surfaces
	if(r_screens[0] == NULL || r_screens[1] == NULL)
	{
		exit(-1);
	}

	// finally mark video system as initialized and return
	r_initialized = true;

	// now we might want to initialize basic game configuration facilities like key-map
	fp = fopen("gameconfig.dat", "rb+");

	// TODO: Add code to load game config

	// we want to load the game configuration if the file exists
	if(fp)
	{
		fclose(fp);
	}
	
	// the gameconfig file may be corrupt
	// this way we ensure to have a default config
	// as last resort
	if(loaded == false)
	{
		// main pad controls
		input_lookup.a = SDLK_d;
		input_lookup.b = SDLK_s;
		input_lookup.x = SDLK_a;
		input_lookup.y = SDLK_w;

		// shoulder keys
		input_lookup.l = SDLK_q;
		input_lookup.r = SDLK_r;

		// directional keys
		input_lookup.left = SDLK_LEFT;
		input_lookup.right = SDLK_RIGHT;
		input_lookup.up = SDLK_UP;
		input_lookup.down = SDLK_DOWN;

		// control keys
		input_lookup.start = SDLK_RETURN;
		input_lookup.select = SDLK_SPACE;
	}
}

void setBrightness(int screen, int level)
{

}

void swiWaitForVBlank(void)
{
	int screen = 0;
	int bg = 0;
	int spr = 0;
	uint32_t currentTick = 0;

	// update timer status
	r_startTicks = SDL_GetTicks();

	// make sure we do not crash because of a wrong call to this
	// as this is not like the DS function which just triggers an interrupt
	if(r_initialized == false)
	{
		return;
	}

	// make sure we render every screen
	for(screen = 0; screen < 2; screen ++)
	{
		// first blit backgrounds to simulate the layer system for backgrounds
		for(bg = 3; bg >= 0; bg --)
		{
			SDL_BlitSurface(r_backgrounds[screen][bg], &r_backgrounds_size[screen][bg], r_screens[screen], &r_screens_size[screen]);

			// for each background layer we want to blit the sprites too
			SDL_BlitSurface(r_sprites[screen][bg], &r_sprites_size[screen][bg], r_screens[screen], &r_screens_size[screen]);
		}

		// finally blit screens to window
		SDL_BlitSurface(r_screens[screen], &r_screens_size[screen], r_window, &r_screens_position[screen]);
	}

	// flip buffers to show current backbuffer
	SDL_Flip(r_window);

	// the next frame will need the screens to be clean
	// cleaning just the surface for virtual screens
	// is enough, this gives the desired effect
	for(screen = 0; screen < 1; screen ++)
	{
		SDL_FillRect(r_screens[screen], NULL, r_transparentColor);

		for(bg = 3; bg >= 0; bg --)
		{
			SDL_FillRect(r_sprites[screen][bg], NULL, r_transparentColor);
			SDL_FillRect(r_backgrounds[screen][bg], NULL, r_transparentColor);
		}
	}

	// frametime checking
	// limits the FPS to 60 just as the DS does
	// we might want to lower this limit to 30
	// in the future
	r_fps ++;

	currentTick = SDL_GetTicks() - r_startTicks;

	if(currentTick < r_maxTicks)
	{
		// ok, we have spare time, sleep
		SDL_Delay(r_maxTicks - currentTick);
	}
}