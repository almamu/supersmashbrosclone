#include <stdlib.h>

#include <nds/arm9/input.h>

#include <SDL.h>

ds_inputcfg_t input_lookup;
uint32_t down = 0;
uint32_t held = 0;

void scanKeys()
{
	// this is the best place to handle these events
	// even when not every event is input
	SDL_Event ev;

	// the down keys flag should be cleared
	// this helps to differentiate from a key press
	// and a key held
	down = 0;

	while(SDL_PollEvent(&ev) != false)
	{
		switch(ev.type)
		{
			// we need to take care of both helds and downs
			// this event is only fired when the key first gets pressed
			// and never fired again until a keyup event is fired for
			// the same key, so we can rely on this code
			case SDL_KEYDOWN: // a key got pressed
				if(ev.key.keysym.sym == input_lookup.a)
				{
					down |= KEY_A;
					held |= KEY_A;
				}
				else if(ev.key.keysym.sym == input_lookup.b)
				{
					down |= KEY_B;
					held |= KEY_B;
				}
				else if(ev.key.keysym.sym == input_lookup.x)
				{
					down |= KEY_X;
					held |= KEY_X;
				}
				else if(ev.key.keysym.sym == input_lookup.y)
				{
					down |= KEY_Y;
					held |= KEY_Y;
				}
				else if(ev.key.keysym.sym == input_lookup.l)
				{
					down |= KEY_L;
					held |= KEY_L;
				}
				else if(ev.key.keysym.sym == input_lookup.r)
				{
					down |= KEY_R;
					held |= KEY_R;
				}
				else if(ev.key.keysym.sym == input_lookup.start)
				{
					down |= KEY_START;
					held |= KEY_START;
				}
				else if(ev.key.keysym.sym == input_lookup.select)
				{
					down |= KEY_SELECT;
					held |= KEY_SELECT;
				}
				else if(ev.key.keysym.sym == input_lookup.up)
				{
					down |= KEY_UP;
					held |= KEY_UP;
				}
				else if(ev.key.keysym.sym == input_lookup.left)
				{
					down |= KEY_LEFT;
					held |= KEY_LEFT;
				}
				else if(ev.key.keysym.sym == input_lookup.down)
				{
					down |= KEY_DOWN;
					held |= KEY_DOWN;
				}
				else if(ev.key.keysym.sym == input_lookup.right)
				{
					down |= KEY_RIGHT;
					held |= KEY_RIGHT;
				}
				break;

			// this one only needs to take care about
			// held buttons as the down ones are already cleared
			case SDL_KEYUP: // a key got released
				if(ev.key.keysym.sym == input_lookup.a)
				{
					held &= ~KEY_A;
				}
				else if(ev.key.keysym.sym == input_lookup.b)
				{
					held &= ~KEY_B;
				}
				else if(ev.key.keysym.sym == input_lookup.x)
				{
					held &= ~KEY_X;
				}
				else if(ev.key.keysym.sym == input_lookup.y)
				{
					held &= ~KEY_Y;
				}
				else if(ev.key.keysym.sym == input_lookup.l)
				{
					held &= ~KEY_L;
				}
				else if(ev.key.keysym.sym == input_lookup.r)
				{
					held &= ~KEY_R;
				}
				else if(ev.key.keysym.sym == input_lookup.start)
				{
					held &= ~KEY_START;
				}
				else if(ev.key.keysym.sym == input_lookup.select)
				{
					held &= ~KEY_SELECT;
				}
				else if(ev.key.keysym.sym == input_lookup.up)
				{
					held &= ~KEY_UP;
				}
				else if(ev.key.keysym.sym == input_lookup.left)
				{
					held &= ~KEY_LEFT;
				}
				else if(ev.key.keysym.sym == input_lookup.down)
				{
					held &= ~KEY_DOWN;
				}
				else if(ev.key.keysym.sym == input_lookup.right)
				{
					held &= ~KEY_RIGHT;
				}
				break;

			// we should take care of mouse movement and presses here too
			// but as the game doesn't use it yet we can skip that part as of now

			// special case, this shutsdown the game
			// maybe not the best way
			// but should be enough for now
			case SDL_QUIT:
				SDL_Quit();
				exit(0);
				break;
		}
	}
}

uint32_t keysCurrent()
{
	return keysDown();
}

uint32_t keysDown()
{
	return down;
}

uint32_t keysHeld()
{
	return held;
}