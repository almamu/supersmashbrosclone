#include <nds/ndstypes.h>
#include <nds/arm9/console.h>

#include <stdio.h>
#include <stdarg.h>

PrintConsole defaultConsole =
{
	true
};

PrintConsole* consoleDemoInit()
{
	return &defaultConsole;
}

void consoleClear()
{

}