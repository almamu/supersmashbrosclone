#include <nds.h>
#include <string.h>
#include "dswifi9.h"

struct in_addr Wifi_GetIPInfo(struct in_addr * pGateway,struct in_addr * pSnmask,struct in_addr * pDns1,struct in_addr * pDns2)
{
	struct in_addr ret;

	memset(&ret, 0, sizeof(struct in_addr));

	return ret;
}

bool Wifi_InitDefault(bool useFirmwareSettings)
{
#ifdef _WIN32
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* Winsock DLL.                                  */
        printf("WSAStartup failed with error: %d\n", err);
        return 1;
    }
#endif /* _WIN32 */
	return true;
}

#ifdef _WIN32
int ioctl(SOCKET sock, long cmd, unsigned long* argp)
{
	return ioctlsocket(sock, cmd, argp);
}

#endif /* WIN32 */
