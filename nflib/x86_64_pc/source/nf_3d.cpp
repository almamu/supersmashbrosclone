
// NightFox LIB - Funciones 2D comunes
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409



// Includes devKitPro
#include <nds.h>

// Includes C
#include <stdio.h>
#include <string.h>

// Includes propios
#include "nf_basic.h"
#include "nf_3d.h"
#include "nf_2d.h"





// Funcion NF_Set3D();
void NF_Set3D(u8 screen, u8 mode) {
	FUNC_STUB_PRINT
}



// Funcion NF_InitOpenGL();
void NF_InitOpenGL(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_GetTextureSize();
u16 NF_GetTextureSize(u16 textel) {
	FUNC_STUB_PRINT
	return 256;
}
