
// NightFox LIB - Funciones de Fondos Affine
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409



// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// Includes propios
#include "nf_basic.h"
#include "nf_2d.h"
#include "nf_tiledbg.h"
#include "nf_affinebg.h"





// Estructura para almacenar los parametros de los fondos Affine
NF_TYPE_AFFINE_BG NF_AFFINE_BG[2][4];





// Funcion NF_InitTiledBgSys();
void NF_InitAffineBgSys(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_LoadAffineBg();
void NF_LoadAffineBg(const char* file, const char* name, u16 width, u16 height) {
	FUNC_STUB_PRINT
}



// Funcion NF_UnloadAffineBg();
void NF_UnloadAffineBg(const char* name) {
	NF_UnloadTiledBg(name);
}





// Funcion NF_CreateAffineBg();
void NF_CreateAffineBg(u8 screen, u8 layer, const char* name, u8 wrap) {
	FUNC_STUB_PRINT
}



// Funcion NF_DeleteAffineBg();
void NF_DeleteAffineBg(u8 screen, u8 layer) {
	FUNC_STUB_PRINT
}



// Funcion NF_AffineBgTransform();
void NF_AffineBgTransform(u8 screen, u8 layer, s32 x_scale, s32 y_scale, s32 x_tilt, s32 y_tilt) {
	FUNC_STUB_PRINT
}





// Funcion NF_AffineBgMove();
void NF_AffineBgMove(u8 screen, u8 layer, s32 x, s32 y, s32 angle) {
	FUNC_STUB_PRINT
}



// Funcion NF_AffineBgCenter();
void NF_AffineBgCenter(u8 screen, u8 layer, s32 x, s32 y) {
	FUNC_STUB_PRINT
}

