
// NightFox LIB - Include de Fondos mixtos (Tiled / Bitmap 8 bits)
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409





// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// Includes propios
#include "nf_basic.h"
#include "nf_2d.h"
#include "nf_tiledbg.h"
#include "nf_bitmapbg.h"
#include "nf_mixedbg.h"





// Funcion NF_InitMixedBgSys();
void NF_InitMixedBgSys(u8 screen) {
	FUNC_STUB_PRINT
}
