
// NightFox LIB - Funciones 2D comunes
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409



// Includes devKitPro
#include <nds.h>

// Includes C
#include <stdio.h>
#include <string.h>

// Includes propios
#include "nf_basic.h"
#include "nf_2d.h"
#include "nf_tiledbg.h"
#include "nf_sprite256.h"


extern SDL_Surface *r_window, *r_screens[2];

// Funcion NF_Set2D();
void NF_Set2D(u8 screen, u8 mode)
{
	SDL_Rect rect = {0, 0, 256, 192};

	SDL_FillRect(r_screens[screen], &rect, SDL_MapRGB(r_window->format, 0, 0, 0));
}



// Funcion NF_ShowBg();
void NF_ShowBg(u8 screen, u8 layer)
{
	NF_TILEDBG_LAYERS[screen][layer].visible = true;
}



// Funcion NF_HideBg();
void NF_HideBg(u8 screen, u8 layer)
{
	NF_TILEDBG_LAYERS[screen][layer].visible = false;
}



// Funcion NF_ScrollBg();
void NF_ScrollBg(u8 screen, u8 layer, s16 x, s16 y)
{
	NF_TILEDBG_LAYERS[screen][layer].rect.x = x;
	NF_TILEDBG_LAYERS[screen][layer].rect.y = y;
}



// Funcion NF_MoveSprite();
void NF_MoveSprite(u8 screen, u8 id, s16 x, s16 y)
{
	FUNC_STUB_PRINT
}



// Funcion NF_SpriteLayer();
void NF_SpriteLayer(u8 screen, u8 id, u8 layer)
{
	FUNC_STUB_PRINT
}



// Funcion NF_ShowSprite();
void NF_ShowSprite(u8 screen, u8 id, bool show)
{
	FUNC_STUB_PRINT
}



// Funcion NF_HflipSprite();
void NF_HflipSprite(u8 screen, u8 id, bool hflip)
{
	FUNC_STUB_PRINT
}



// Funcion NF_GetSpriteHflip();
bool NF_GetSpriteHflip(u8 screen, u8 id)
{
	FUNC_STUB_PRINT
	return true;
}



// Funcion NF_VflipSprite();
void NF_VflipSprite(u8 screen, u8 id, bool vflip)
{
	FUNC_STUB_PRINT
}



// Funcion NF_GetSpriteVflip();
bool NF_GetSpriteVflip(u8 screen, u8 id)
{
	FUNC_STUB_PRINT
	return false;
}



// Funcion NF_SpriteFrame();
void NF_SpriteFrame(u8 screen, u8 id, u16 frame)
{
	FUNC_STUB_PRINT
}



// Funcion NF_EnableSpriteRotScale();
void NF_EnableSpriteRotScale(u8 screen, u8 sprite, u8 id, bool doublesize)
{
	FUNC_STUB_PRINT
}



// Funcion NF_DisableSpriteRotScale();
void NF_DisableSpriteRotScale(u8 screen, u8 sprite)
{
	FUNC_STUB_PRINT
}



// Funcion NF_SpriteRotScale();
void NF_SpriteRotScale(u8 screen, u8 id, s16 angle, u16 sx, u16 sy)
{
	FUNC_STUB_PRINT
}
