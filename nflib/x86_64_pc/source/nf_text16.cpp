
// NightFox LIB - Funciones de Textos de 16 pixeles
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409



// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// Includes propios
#include "nf_basic.h"
#include "nf_2d.h"
#include "nf_tiledbg.h"
#include "nf_text.h"
#include "nf_text16.h"





// Funcion NF_LoadTextFont16();
void NF_LoadTextFont16(const char* file, const char* name, u16 width, u16 height, u8 rotation) {
	FUNC_STUB_PRINT
}





// Funcion NF_CreateTextLayer16();
void NF_CreateTextLayer16(u8 screen, u8 layer, u8 rotation, const char* name) {
	FUNC_STUB_PRINT
}





// Funcion NF_WriteText16();
void NF_WriteText16(u8 screen, u8 layer, u16 x, u16 y, const char* text) {
	FUNC_STUB_PRINT
}





// Funcion NF_ClearTextLayer16();
void NF_ClearTextLayer16(u8 screen, u8 layer) {
	FUNC_STUB_PRINT
}
