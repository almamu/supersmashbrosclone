
// NightFox LIB - Funciones de Fondos con tiles
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409





// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// Includes propios
#include "nf_basic.h"
#include "nf_2d.h"
#include "nf_tiledbg.h"



// Define los bancos de Mapas y Tiles
u8 NF_BANKS_TILES[2];	// (1 banks = 16kb)	Cada banco de tiles puede alvergar 8 bancos de Mapas
u8 NF_BANKS_MAPS[2];	// (1 bank = 2kb)	Usar multiplos de 8. Cada set de 8 bancos consume 1 banco de tiles

// Define los Buffers para almacenar los fondos
char* NF_BUFFER_BGTILES[NF_SLOTS_TBG];
char* NF_BUFFER_BGMAP[NF_SLOTS_TBG];
char* NF_BUFFER_BGPAL[NF_SLOTS_TBG];

// Define la estructura de propiedades de los fondos
NF_TYPE_TBG_INFO NF_TILEDBG[NF_SLOTS_TBG];			// Info de los fondos cargados en RAM
NF_TYPE_TBGLAYERS_INFO NF_TILEDBG_LAYERS[2][4];		// Info de los fondos en pantalla

// Define la estructura para las paletas extendidas
NF_TYPE_EXBGPAL_INFO NF_EXBGPAL[NF_SLOTS_EXBGPAL];	// Datos de las paletas extendidas

// Define el array de bloques libres
u8 NF_TILEBLOCKS[2][NF_MAX_BANKS_TILES];
u8 NF_MAPBLOCKS[2][NF_MAX_BANKS_MAPS];



// Funcion NF_InitTiledBgBuffers();
void NF_InitTiledBgBuffers(void)
{
	u8 n = 0;
	// Buffers de fondos tileados
	for (n = 0; n < NF_SLOTS_TBG; n ++) {	// Inicializa todos los slots
		NF_BUFFER_BGTILES[n] = NULL;		// Buffer para los tiles
		NF_BUFFER_BGMAP[n] = NULL;			// Buffer para el map
		NF_BUFFER_BGPAL[n] = NULL;			// Buffer para la paleta
		sprintf(NF_TILEDBG[n].name, "xxxNONAMExxx");	// Nombre del Tileset
		NF_TILEDBG[n].tilesize = 0;			// Tama�o del Tileset
		NF_TILEDBG[n].mapsize = 0;			// Tama�o del Mapa
		NF_TILEDBG[n].palsize = 0;			// Tama�o de la Paleta
		NF_TILEDBG[n].width = 0;			// Ancho del Mapa
		NF_TILEDBG[n].height = 0;			// Alto del Mapa
		NF_TILEDBG[n].available = true;		// Disponibilidad
	}
	// Buffers de paletas extendidas
	for (n = 0; n < NF_SLOTS_EXBGPAL; n ++) {
		NF_EXBGPAL[n].buffer = NULL;
		NF_EXBGPAL[n].palsize = 0;
		NF_EXBGPAL[n].inuse = false;
	}

}



// Funcion NF_ResetTiledBgBuffers();
void NF_ResetTiledBgBuffers(void) {
	u8 n = 0, i = 0;
	for (n = 0; n < NF_SLOTS_TBG; n ++) {	// Inicializa todos los slots
		free(NF_BUFFER_BGTILES[n]);			// Vacia el Buffer para los tiles
		free(NF_BUFFER_BGMAP[n]);			// Vacia Buffer para el map
		free(NF_BUFFER_BGPAL[n]);			// Vacia Buffer para la paleta			
	}
	for (n = 0; n < NF_SLOTS_EXBGPAL; n ++) {
		NF_EXBGPAL[n].buffer = NULL;
	}

	// free surfaces
	for (n = 0; n < 1; n ++)
	{
		for(i = 0; i < 4; i ++)
		{
			if(NF_TILEDBG_LAYERS[n][i].surf != NULL)
			{
				SDL_FreeSurface(NF_TILEDBG_LAYERS[n][i].surf);
				NF_TILEDBG_LAYERS[n][i].surf = NULL;
			}
		}
	}

	NF_InitTiledBgBuffers();				// Reinicia el resto de variables
}



// Funcion NF_InitTiledBgSys();
void NF_InitTiledBgSys(u8 screen)
{
	// Variables
	u8 n = 0;

	// Define el numero de bancos de Mapas y Tiles
	NF_BANKS_TILES[screen] = 8;		// (1 banks = 16kb)	Cada banco de tiles puede alvergar 8 bancos de Mapas
	NF_BANKS_MAPS[screen] = 16;		// (1 bank = 2kb)	Usar multiplos de 8. Cada set de 8 bancos consume 1 banco de tiles
	// Por defecto Tiles = 8, Mapas = 16
	// Esto nos deja 6 bancos de 16kb para tiles
	// y 16 bancos de 2kb para mapas

	// Inicializa el array de bloques libres de Tiles
	for (n = 0; n < NF_BANKS_TILES[screen]; n ++) {
		NF_TILEBLOCKS[screen][n] = 0;
	}

	// Inicializa el array de bloques libres de Mapas
	for (n = 0; n < NF_BANKS_MAPS[screen]; n ++) {
		NF_MAPBLOCKS[screen][n] = 0;
	}

	SDL_Rect rect = {0, 0, 256, 192};

	// Inicializa el array de informacion de fondos en pantalla
	for (n = 0; n < 4; n ++)
	{
		NF_TILEDBG_LAYERS[screen][n].tilebase = 0;		// Base del Tileset
		NF_TILEDBG_LAYERS[screen][n].tileblocks = 0;	// Bloques usados por el Tileset
		NF_TILEDBG_LAYERS[screen][n].mapbase = 0;		// Base del Map
		NF_TILEDBG_LAYERS[screen][n].mapblocks = 0;		// Bloques usados por el Map
		NF_TILEDBG_LAYERS[screen][n].bgwidth = 0;		// Ancho del fondo
		NF_TILEDBG_LAYERS[screen][n].bgheight = 0;		// Altura del fondo
		NF_TILEDBG_LAYERS[screen][n].mapwidth = 0;		// Ancho del mapa
		NF_TILEDBG_LAYERS[screen][n].mapheight = 0;		// Altura del mapa
		NF_TILEDBG_LAYERS[screen][n].bgtype = 0;		// Tipo de mapa
		NF_TILEDBG_LAYERS[screen][n].bgslot = 0;		// Buffer de graficos usado
		NF_TILEDBG_LAYERS[screen][n].blockx = 0;		// Bloque de mapa actual (horizontal)
		NF_TILEDBG_LAYERS[screen][n].blocky = 0;		// Bloque de mapa actual (vertical)
		NF_TILEDBG_LAYERS[screen][n].created = false;	// Esta creado ?
		NF_TILEDBG_LAYERS[screen][n].visible = false;	// Esta visible ?
		NF_TILEDBG_LAYERS[screen][n].rect = rect;
		NF_TILEDBG_LAYERS[screen][n].surf = NULL;
	}

	// Ahora reserva los bancos necesarios de VRAM para mapas
	// Cada bloque de 16kb (1 banco de tiles) permite 8 bancos de mapas (de 2kb cada uno)
	u8 r_banks;
	r_banks = ((NF_BANKS_MAPS[screen] - 1) >> 3) + 1;		// Calcula los bancos de Tiles a reservar para Maps
	for (n = 0; n < r_banks; n ++) {
		NF_TILEBLOCKS[screen][n] = 128;				// Marca que bancos de VRAM son para MAPS
	}

	for (n = 0; n < 4; n ++) {					// Oculta todas las 4 capas
		NF_HideBg(screen, n);
	}
}



// Funcion NF_LoadTiledBg();
void NF_LoadTiledBg(const char* file, const char* name, u16 width, u16 height) {
	FUNC_STUB_PRINT
}



// Funcion NF_LoadTilesForBg();
void NF_LoadTilesForBg(const char* file, const char* name, u16 width, u16 height, u16 tile_start, u16 tile_end) {
	FUNC_STUB_PRINT
}



// Funcion NF_UnloadTiledBg();
void NF_UnloadTiledBg(const char* name) {
	FUNC_STUB_PRINT
}



// Funcion NF_CreateTiledBg();
void NF_CreateTiledBg(u8 screen, u8 layer, const char* name) {
	FUNC_STUB_PRINT
}



// Funcion NF_DeleteTiledBg();
void NF_DeleteTiledBg(u8 screen, u8 layer) {
	FUNC_STUB_PRINT
}



// Funcion NF_GetTileMapAddress();
u32 NF_GetTileMapAddress(u8 screen, u8 layer, u16 tile_x, u16 tile_y) {
	FUNC_STUB_PRINT
	return 0;
}



// Funcion NF_GetTileOfMap();
u16 NF_GetTileOfMap(u8 screen, u8 layer, u16 tile_x, u16 tile_y) {
	FUNC_STUB_PRINT
	return 0;
}



// Funcion NF_SetTileOfMap();
void NF_SetTileOfMap(u8 screen, u8 layer, u16 tile_x, u16 tile_y, u16 tile) {
	FUNC_STUB_PRINT
}



// Funcion NF_UpdateVramMap();
void NF_UpdateVramMap(u8 screen, u8 layer) {
	FUNC_STUB_PRINT
}




// Funcion NF_BgSetPalColor();
void NF_BgSetPalColor(u8 screen, u8 layer, u8 number, u8 r, u8 g, u8 b) {
	FUNC_STUB_PRINT
}



// Funcion NF_BgEditPalColor();
void NF_BgEditPalColor(u8 screen, u8 layer, u8 number, u8 r, u8 g, u8 b) {
	FUNC_STUB_PRINT
}



// Funcion 	NF_BgUpdatePalette();
void NF_BgUpdatePalette(u8 screen, u8 layer) {
	FUNC_STUB_PRINT
}



// Funcion NF_BgGetPalColor();
void NF_BgGetPalColor(u8 screen, u8 layer, u8 number, u8* r, u8* g, u8* b) {
	FUNC_STUB_PRINT
}



// Funcion NF_GetTilePal();
u8 NF_GetTilePal(u8 screen, u8 layer, u16 tile_x, u16 tile_y) {
	FUNC_STUB_PRINT
	return 0;
}



// Funcion NF_SetTilePal();
void NF_SetTilePal(u8 screen, u8 layer, u16 tile_x, u16 tile_y, u8 pal) {
	FUNC_STUB_PRINT
}



// Funcion NF_LoadExBgPal();
void NF_LoadExBgPal(const char* file, u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_UnloadExBgPal();
void NF_UnloadExBgPal(u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_VramExBgPal();
void NF_VramExBgPal(u8 screen, u8 layer, u8 id, u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_SetExBgPal();
void NF_SetExBgPal(u8 screen, u8 layer, u8 pal) {
	FUNC_STUB_PRINT
}



// Funcion NF_SetTileHflip();
void NF_SetTileHflip(u8 screen, u8 layer, u16 tile_x, u16 tile_y) {
	FUNC_STUB_PRINT
}



// Funcion NF_SetTileVflip();
void NF_SetTileVflip(u8 screen, u8 layer, u16 tile_x, u16 tile_y) {
	FUNC_STUB_PRINT
}





// Funcion NF_RotateTileGfx();
void NF_RotateTileGfx(u8 slot, u16 tile, u8 rotation) {
	FUNC_STUB_PRINT
}
