
// NightFox LIB - Include de funciones de fondos en modo Bitmap
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409



// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// Includes propios
#include "nf_bitmapbg.h"
#include "nf_basic.h"
#include "nf_2d.h"



// Define los Buffers para almacenar datos de 16 bits
NF_TYPE_BG16B_INFO NF_BG16B[NF_SLOTS_BG16B];		// Fondos RAW de 16 bits

// Backbuffer de 16 bits de cada pantalla
u16* NF_16BITS_BACKBUFFER[2];

// Define los Buffers para almacenar datos de 8 bits
NF_TYPE_BG8B_INFO NF_BG8B[NF_SLOTS_BG8B];	// Fondos indexados de 8 bits

// Backbuffer de 8 bits de cada pantalla
NF_TYPE_BB8B_INFO NF_8BITS_BACKBUFFER[2];





// Funcion NF_Init16bitsBgBuffers();
void NF_Init16bitsBgBuffers(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_Reset16bitsBgBuffers();
void NF_Reset16bitsBgBuffers(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_Init16bitsBackBuffer();
void NF_Init16bitsBackBuffer(u8 screen) {
	FUNC_STUB_PRINT
}


// Funcion NF_Enable16bitsBackBuffer();
void NF_Enable16bitsBackBuffer(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_Disble16bitsBackBuffer();
void NF_Disble16bitsBackBuffer(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_Flip16bitsBackBuffer();
void NF_Flip16bitsBackBuffer(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_InitBitmapBgSys();
void NF_InitBitmapBgSys(u8 screen, u8 mode) {
	FUNC_STUB_PRINT
}



// Funcion NF_Load16bitsBg();
void NF_Load16bitsBg(const char* file, u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_Load16bitsImage();
void NF_Load16bitsImage(const char* file, u8 slot, u16 size_x, u16 size_y) {
	FUNC_STUB_PRINT
}



// Funcion NF_Load16bImgData();
void NF_Load16bImgData(const char* file, u8 slot, u16 x, u16 y, u8 type) {
	FUNC_STUB_PRINT
}



// Funcion NF_Unload16bitsBg();
void NF_Unload16bitsBg(u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_Copy16bitsBuffer();
void NF_Copy16bitsBuffer(u8 screen, u8 destination, u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_Draw16bitsImage();
void NF_Draw16bitsImage(u8 screen, u8 slot, s16 x, s16 y, bool alpha) {
	FUNC_STUB_PRINT
}



// Funcion NF_Init8bitsBgBuffers();
void NF_Init8bitsBgBuffers(void) {
	FUNC_STUB_PRINT
}



// Funcion NF_Reset8bitsBgBuffers();
void NF_Reset8bitsBgBuffers(void) {
	FUNC_STUB_PRINT
}


// Funcion NF_Load8bitsBg();
void NF_Load8bitsBg(const char* file, u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_Unload8bitsBg();
void NF_Unload8bitsBg(u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_Copy8bitsBuffer();
void NF_Copy8bitsBuffer(u8 screen, u8 destination, u8 slot) {
	FUNC_STUB_PRINT
}



// Funcion NF_Init8bitsBackBuffer();
void NF_Init8bitsBackBuffer(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_Enable8bitsBackBuffer();
void NF_Enable8bitsBackBuffer(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_Disble8bitsBackBuffer();
void NF_Disble8bitsBackBuffer(u8 screen) {
	FUNC_STUB_PRINT
}



// Funcion NF_Flip8bitsBackBuffer();
void NF_Flip8bitsBackBuffer(u8 screen, u8 destination) {
	FUNC_STUB_PRINT
}
