
// NightFox LIB - Funciones de de funciones de sonido
// Requiere DevkitARM
// Codigo por Cesar Rincon "NightFox"
// http://www.nightfoxandco.com/
// Version 20130409





// Includes devKitPro
#include <nds.h>
#include <filesystem.h>
#include <fat.h>

// Includes C
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// Includes propios
#include "nf_basic.h"
#include "nf_sound.h"





// Define los Buffers para almacenar los archivos de audio
char* NF_BUFFER_RAWSOUND[NF_SLOTS_RAWSOUND];

// Datos de los sonidos cargado
NF_TYPE_RAWSOUND_INFO NF_RAWSOUND[NF_SLOTS_RAWSOUND];





// Funcion NF_InitRawSoundBuffers();
void NF_InitRawSoundBuffers(void) {
	FUNC_STUB_PRINT
}





// Funcion NF_ResetRawSoundBuffers();
void NF_ResetRawSoundBuffers(void) {
	FUNC_STUB_PRINT
}





// Funcion NF_LoadRawSound();
void NF_LoadRawSound(const char* file, u16 id,  u16 freq, u8 format) {
	FUNC_STUB_PRINT
}





// Funcion UnloadRawSound();
void NF_UnloadRawSound(u8 id) {
	FUNC_STUB_PRINT
}





// Funcion NF_PlayRawSound();
u8 NF_PlayRawSound(u8 id, u8 volume, u8 pan, bool loop, u16 loopfrom) {
	FUNC_STUB_PRINT
	return 0;
}
