#ifndef _filesystem_h_
#define _filesystem_h_

#ifdef __cplusplus
extern "C" {
#endif

bool nitroFSInit(char **basepath);

#ifdef __cplusplus
}
#endif

#endif /* _filesystem_h_ */