#ifndef DSWIFI9_H
#define DSWIFI9_H

#include "dswifi_version.h"

/*
 * Unix-like systems compatibility fixes
 */

#if defined(linux) || defined(__linux__) || defined(__linux)
#include <nds/sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#endif /* linux */

/*
 * Windows system compatibility fixes
 */
#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <nds/sys/socket.h>
#endif /* _WIN32 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define WFC_CONNECT	true
#define INIT_ONLY	false

/*
 * Windows system compatibility fix (ioctl to ioctlsocket)
 */
#ifdef _WIN32
extern int ioctl(SOCKET socket, long cmd, void * arg);
#endif /* _WIN32 */

// Wifi_GetIPInfo: (values may not be valid before connecting to an AP, or setting the IP manually.)
//  struct in_addr * pGateway:	pointer to receive the currently configured gateway IP
//  struct in_addr * pSnmask:	pointer to receive the currently configured subnet mask
//  struct in_addr * pDns1:		pointer to receive the currently configured primary DNS server IP
//  struct in_addr * pDns2:		pointer to receive the currently configured secondary DNS server IP
//  Returns:					The current IP address of the DS 
extern struct in_addr Wifi_GetIPInfo(struct in_addr * pGateway,struct in_addr * pSnmask,struct in_addr * pDns1,struct in_addr * pDns2);

extern bool Wifi_InitDefault(bool useFirmwareSettings);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* DSWIFI9_H */
