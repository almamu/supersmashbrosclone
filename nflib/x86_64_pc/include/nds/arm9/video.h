#ifndef VIDEO_ARM9_INCLUDE
#define VIDEO_ARM9_INCLUDE

#include <nds/ndstypes.h>
#include <SDL.h>

/*
 * @brief Render window
 */
extern SDL_Surface* r_window;

/*
 * @brief Simulated DS view
 */
extern SDL_Surface* r_screens[2];

/*
 * @brief Simulated backgrounds system ds-like
 */
extern SDL_Surface* r_backgrounds[2][4];

/*
 * @brief Simulated sprites system
 */
extern SDL_Surface* r_sprites[2][4];

/*
 * @brief Position of every simulated screen
 */
extern SDL_Rect r_screens_position[2];

/*
 * @brief Size of every simulated screen
 */
extern SDL_Rect r_screens_size[2];

/*
 * @brief Size of every simulated background layer
 */
extern SDL_Rect r_backgrounds_size[2][4];

/*
 * @brief Size of every simulated sprite layer
 */
extern SDL_Rect r_sprites_size[2][4];

/*
 * @brief Max frames to render by second
 */
extern uint32_t r_maxTicks;

/*
 * @brief Frames per second
 */
extern uint32_t r_fps;

/*
 * @brief Tick at the frame rendering started
 */
extern uint32_t r_startTicks;

/*
 * @brief Alpha color used by the engine (usually the first palete color, aka magenta)
 */
extern uint32_t r_transparentColor;

/*
 * @brief Indicates if the video system has been initialized
 */
extern bool r_initialized;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define DISPLAY_SPR_1D_LAYOUT		(1 << 4)

#define DISPLAY_SPR_1D				(1 << 4)
#define DISPLAY_SPR_2D				(0 << 4)
#define DISPLAY_SPR_1D_BMP			(4 << 4)
#define DISPLAY_SPR_2D_BMP_128		(0 << 4)
#define DISPLAY_SPR_2D_BMP_256		(2 << 4)


#define DISPLAY_SPR_1D_SIZE_32		(0 << 20)
#define DISPLAY_SPR_1D_SIZE_64		(1 << 20)
#define DISPLAY_SPR_1D_SIZE_128		(2 << 20)
#define DISPLAY_SPR_1D_SIZE_256		(3 << 20)
#define DISPLAY_SPR_1D_BMP_SIZE_128	(0 << 22)
#define DISPLAY_SPR_1D_BMP_SIZE_256	(1 << 22)

/** \brief sets the screens brightness.
	\param screen 1 = main screen, 2 = subscreen, 3 = both
	\param level -16 = black, 0 = full brightness, 16 = white
*/
void setBrightness(int screen, int level);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* !VIDEO_ARM9_INCLUDE */
