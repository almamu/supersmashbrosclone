#ifndef	INPUT_HEADER_INCLUDE
#define	INPUT_HEADER_INCLUDE
//---------------------------------------------------------------------------------
//#include <nds/touch.h>
#include <nds/input.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*!	\brief Obtains the current keypad state.
	Call this function once per main loop in order to use the keypad functions.
*/
void scanKeys(void);

/*!	\brief Obtains the current keypad state.
	Call this function to get keypad state without affecting state of other key functions (keysUp keysHeld etc...)
*/
uint32 keysCurrent(void);

//!	Obtains the current keypad held state.
uint32 keysHeld(void);

//!	Obtains the current keypad pressed state.
uint32 keysDown(void);

#ifdef __cplusplus
}
#endif

#endif /* !INPUT_HEADER_INCLUDE */