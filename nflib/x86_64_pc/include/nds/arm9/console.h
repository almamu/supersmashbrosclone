#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdio.h>
#include <nds/ndstypes.h>

#define iprintf printf

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct PrintConsole
{
	bool consoleInitialised;
}PrintConsole;

/*!	\brief Initialize the console to a default state for prototyping.
	This function sets the console to use sub display, VRAM_C, and BG0 and enables MODE_0_2D on the
	sub display.  It is intended for use in prototyping applications which need print ability and not actual
	game use.  Print functionality can be utilized with just this call.
	\return A pointer to the current PrintConsole.
*/
PrintConsole* consoleDemoInit(void);

//! Clears the screan by using iprintf("\x1b[2J");
void consoleClear(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !CONSOLE_H */