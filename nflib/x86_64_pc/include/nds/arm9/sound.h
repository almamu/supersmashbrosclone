#ifndef _sound_h_
#define _sound_h_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <nds/ndstypes.h>


/*! \brief Sound formats used by the DS */
typedef enum {
	SoundFormat_16Bit = 1, /*!<  16-bit PCM */
	SoundFormat_8Bit = 0,  /*!<  8-bit PCM */
	SoundFormat_PSG = 3,   /*!<  PSG (programmable sound generator?) */
	SoundFormat_ADPCM = 2  /*!<  IMA ADPCM compressed audio  */
}SoundFormat;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _sound_h_ */
