#ifndef _exceptions_h_
#define _exceptions_h_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

//! sets the default hardware exception handler.
void defaultExceptionHandler();

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !_exceptions_h_ */