#ifndef __DS_SYS_SOCKET_H__
#define __DS_SYS_SOCKET_H__

#ifndef WIN32
/*
 * Original Linux includes needed
 */
#include <sys/socket.h>

/**
 * @def SOCKET int
 */
typedef int SOCKET;

/**
 * @def closesocket socket
 */
#define closesocket close

/**
 * @def SOCKET_ERROR
 */
#define SOCKET_ERROR (-1)
#endif /* !WIN32 */

/**
 * @def setopt setsockopt
 */
#define setopt setsockopt

#endif /* !__DS_SYS_SOCKET_H__ */
