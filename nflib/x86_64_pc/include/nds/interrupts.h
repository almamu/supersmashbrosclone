#ifndef NDS_INTERRUPTS_INCLUDE
#define NDS_INTERRUPTS_INCLUDE

/*! \fn  swiWaitForVBlank()
	\brief Wait for vblank interrupt

	Waits for a vertical blank interrupt

	\note Identical to calling swiIntrWait(1, 1)
*/
void swiWaitForVBlank(void);

#endif /* !NDS_INTERRUPTS_INCLUDE */
