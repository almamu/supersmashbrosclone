#ifndef NDS_DEBUG_INCLUDE
#define NDS_DEBUG_INCLUDE

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void nocashWrite(const char *message, int len);
/*! \brief Send a message to the no$gba debug window 
\param message The message to send
*/
void nocashMessage(const char *message);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* NDS_DEBUG_INCLUDE */
