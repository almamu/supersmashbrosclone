#ifndef _INPUT_H_
#define _INPUT_H_

#include <nds/ndstypes.h>
#include <SDL.h>

/*! \file
	\brief common values for keypad input.

	common values that can be used on both the arm9 and arm7.
*/

//! enum values for the keypad buttons.
typedef enum KEYPAD_BITS {
  KEY_A      = BIT(0),  //!< Keypad A button.
  KEY_B      = BIT(1),  //!< Keypad B button.
  KEY_SELECT = BIT(2),  //!< Keypad SELECT button.
  KEY_START  = BIT(3),  //!< Keypad START button.
  KEY_RIGHT  = BIT(4),  //!< Keypad RIGHT button.
  KEY_LEFT   = BIT(5),  //!< Keypad LEFT button.
  KEY_UP     = BIT(6),  //!< Keypad UP button.
  KEY_DOWN   = BIT(7),  //!< Keypad DOWN button.
  KEY_R      = BIT(8),  //!< Right shoulder button.
  KEY_L      = BIT(9),  //!< Left shoulder button.
  KEY_X      = BIT(10), //!< Keypad X button.
  KEY_Y      = BIT(11), //!< Keypad Y button.
  KEY_TOUCH  = BIT(12), //!< Touchscreen pendown.
  KEY_LID    = BIT(13)  //!< Lid state.
} KEYPAD_BITS;

typedef struct ds_inputcfg_t
{
  SDLKey a, b, select, start, right, left, up, down, r, l, x, y;
}ds_inputcfg_t;

/*
 * @brief Lookup table to transform keyboard press to DS key-presses
 */
extern ds_inputcfg_t input_lookup;

#endif /* !_INPUT_H_ */
