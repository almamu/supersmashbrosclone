#ifndef NDS_INCLUDE
#define NDS_INCLUDE

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 * Windows compatibility fixes
 */
#ifdef _WIN32
/*
 * Compatibility for Windows 2000 an later
 */
#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0500

/*
 * Base includes for every windows program
 */
#include <windows.h>
#endif /* _WIN32 */

/*
 * Base NDS abstraction layer
 */
#include "nds/ndstypes.h"
#include "nds/interrupts.h"
#include "nds/input.h"
#include "nds/debug.h"

/*
 * NDS' ARM9 abstraction layer
 */

#include "nds/arm9/console.h"
#include "nds/arm9/exceptions.h"
#include "nds/arm9/input.h"
#include "nds/arm9/sound.h"
#include "nds/arm9/sprite.h"
#include "nds/arm9/video.h"

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !NDS_INCLUDE */